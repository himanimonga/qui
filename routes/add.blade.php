@extends('admin.layouts.app')
@section('breadcrumbs')
@include('includes.breadcrumb', ['breadcrumbs' => [
'Listing' => route('admin.blog'),
'Add New'
]])
@endsection
@section('content')
<section class="page-content container">
	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<form method="post" action="{{ route('admin.storeblog') }}" enctype="multipart/form-data" autocomplete="off">
						@csrf
						<div class="row">
							
							<div class="col-md-6">
								<div class="form-group">
									<label>Category</label>
									<select class="form-control" name="category" id="category" >
										@foreach($category as $key=>$value)
										<option value="{{$value['id']}}" {{ old('category') == $value['id'] ? 'selected' : '' }}>{{$value['name']}}</option>
										@endforeach	
									</select>
									
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Title*</label>
									<input type="text" name="title" class="form-control" placeholder="Title Name" value="{{ old('title') }}">
									
									@error('title')
									<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Author name*</label>
									<input type="text" name="author_name" class="form-control" placeholder="Author name" value="{{ old('author_name') }}">
									
									@error('author_name')
									<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Date*</label>
									<div class="input-group date dp-years">
										<input type="text" name="date" class="form-control" placeholder="Date" value="{{ old('date') }}">
										<span class="input-group-addon action">
										</span>
									</div>
									
									
									@error('date')
									<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
								
								
								
								
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Blog image*</label>
									<input type="file" name="image" class="form-control"><b>Upload minimum width 874px and hight 512px image.</b>
									
									@error('image')
									<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Meta Title*</label>
									<input type="text" name="meta_title" class="form-control" placeholder="Meta Title" value="{{ old('meta_title') }}">
									
									@error('meta_title')
									<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Meta Description*</label>
									<textarea type="text" name="meta_description" class="form-control" placeholder="Meta Description" rows="5" cols="10">{{ old('meta_description') }}</textarea>
									
									@error('meta_description')
									<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Short Description*</label>
									<textarea type="text" name="short_description" class="form-control" placeholder="Short Description" rows="5" cols="10">{{ old('short_description') }}</textarea>
									
									@error('short_description')
									<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Description*</label>
									<textarea type="text" name="description" class="form-control" placeholder="Description" rows="5" cols="10">{{ old('description') }}</textarea>
									
									@error('description')
									<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Status</label>
									<select class="form-control" name="status">
										<option value="Enable" {{ old('status') == "Enable" ? 'selected' : '' }}>Enable</option>
										<option value="Disable" {{ old('status') == "Disable" ? 'selected' : '' }}>Disable</option>
									</select>
								</div>
							</div>
						</div>
						<div class="card-footer row">
							<div class="input submit">
								<div class="submit">
									<input class="btn btn-primary" type="submit" value="Save">
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
	
	CKEDITOR.replace('description', {
        filebrowserUploadUrl: "{{route('admin.ckeditor.upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
    });
</script>
@endsection