<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
   // return $request->user();
//});
//Route::post('forgot_password', 'Auth\PasswordResetController@create');

Route::group([
    'prefix' => 'auth'
], function () {
 
  Route::post('login', 'ApiController@login');
  Route::post('signup', 'ApiController@signup');
  Route::post('create_account', 'ApiController@create_account');

  Route::post('/socialLogin', 'ApiController@socialLogin');
  Route::post('forgotPassword','ApiController@forgotPassword');
  // Route::group(['middleware' => ['web','auth:api']], function(){ 

  Route::get('pages', 'ApiController@pages');
  Route::get('category', 'ApiController@category');
  //news
  Route::post('news', 'NewsController@news');
  Route::post('storeNews', 'NewsController@storeNews');
  Route::post('updateNews', 'NewsController@updateNews');
  Route::post('deleteNews', 'NewsController@deleteNews');
 
  //Route::post('storeNeededHelp', 'ApiController@storeNeededHelp');
  //needed
  Route::post('storeNeeded', 'NeededController@storeNeeded');
  Route::post('needed', 'NeededController@needed');
  Route::post('updateNeeded', 'NeededController@updateNeeded');
  Route::post('deleteNeeded', 'NeededController@deleteNeeded');
  //offered
  Route::post('updateOffered', 'OfferedController@updateOffered');
  Route::post('deleteOffered', 'OfferedController@deleteOffered');
  Route::post('storeOffered', 'OfferedController@storeOffered');
  Route::post('offered', 'OfferedController@offered');
  //Route::post('storeNews', 'ApiController@storeNews');
  Route::post('notificationCount', 'NotificationController@notificationCount');
  Route::post('notification', 'NotificationController@notification');
  Route::post('notificationStatus', 'NotificationController@notificationStatus');
  Route::post('get_notificationStatus', 'NotificationController@get_notificationStatus');
  Route::post('notifydelete', 'NotificationController@notifydelete');
 
  Route::post('change_reaction', 'ReactionController@change_reaction');
  Route::post('get_reaction', 'ReactionController@get_reaction');

  //chat
  Route::post('personalChat', 'ChatController@personalChat');
  Route::post('getChat', 'ChatController@getChat');
  Route::post('support_chat', 'ChatController@support_chat');
  Route::post('unreadmsg', 'ChatController@unreadmsg');
  Route::post('chatJoin', 'ChatController@chatJoin');
  Route::post('chatleave', 'ChatController@chatleave');
  Route::post('check_onlineCount', 'ChatController@onlinecount'); 
  Route::post('favourite_geolocations', 'GeolocationController@favourite_geolocations');
  Route::post('getFavourite_geolocations', 'GeolocationController@getFavourite_geolocations');
  Route::post('removeFavourite_geolocations', 'GeolocationController@removeFavourite_geolocations');
  //Route::post('chatDelete', 'ChatController@chatDelete');

  Route::post('updateProfile', 'ApiController@updateProfile');
  
  // Route::post('getFavourite_geolocations', 'ApiController@getFavourite_geolocations');
  Route::post('share_page', 'ShareController@share_page');
  Route::post('post_share', 'ShareController@post_share');
 
 
  Route::post('getFeedback', 'ApiController@getFeedback');
  
  //puclic private channel notification
 // Route::post('publicprivateNotification','ApiController@publicprivateNotification');
  //  Route::get('category', 'ApiController@category');
  //alert
  Route::post('getLast_helpPost', 'ApiController@getLast_helpPost');
  Route::post('map_trackStore', 'ApiController@map_trackStore');
  Route::post('getLatLong', 'ApiController@getLatLong');
  
  Route::post('joinLocation', 'ApiController@joinLocation');
  Route::post('getjoinLocation', 'ApiController@getjoinLocation');
  Route::post('report', 'ApiController@report');
  

  Route::post('rating', 'ApiController@rating');
  Route::get('contactUs', 'ApiController@contactUs');
  Route::post('fileShare', 'ApiController@fileShare');
  //channel
  Route::post('get_publicChannel', 'ChannelController@get_publicChannel');
  Route::post('blockUnblock', 'ChannelController@blockUnblock');
  Route::post('deleteChannel', 'ChannelController@deleteChannel');
  Route::post('muteUnmute', 'ChannelController@muteUnmute');
  Route::post('storechannelPost', 'ChannelController@storechannelPost');
  
  //Route::post('notifydelete', 'ApiController@notifydelete');
  Route::get('logout', 'ApiController@logout');   
  Route::post('alluserdata', 'ApiController@alluserdata'); 
  //Route::post('location_search', 'ApiController@location_search'); 

           // });       


      
  
   /* Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'ApiController@logout');
      // Route::get('user', 'ApiController@user');
    });*/
});
