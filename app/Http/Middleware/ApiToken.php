<?php

namespace App\Http\Middleware;

use Closure;

class ApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
   public function handle($request, Closure $next)
{
     
	
    if ( $request->headers->get('apitoken') != Config('app.key')) {

        return response()->json('Unauthorized', 401);
    } 

    return $next($request);
}
}
