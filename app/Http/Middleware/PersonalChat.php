<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class PersonalChat extends Model
{
	  use Sortable;
	
	public $sortable = ['tittle'];
    protected $fillable = [

       'chat_token','client_id','uuid','user_name','user_image','channel','message','status','created_at','post_id','start_timetoken','end_timetoken','type',
    ];



    public function Postdata(){
		return $this->belongsTo('App\Model\Post','post_id');
	}
}
