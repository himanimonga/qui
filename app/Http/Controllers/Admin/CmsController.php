<?php



namespace App\Http\Controllers\Admin;



use Illuminate\Http\Request;

use Illuminate\Http\UploadedFile;

use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\Controller;

use App\Model\Cms;

use Illuminate\Pagination\Paginator;



class CmsController extends Controller

{

	public function __construct(){

		$this->cms = new Cms();

		//$this->middleware('auth:admin')->except('logout');

	}

	

	public function index(Request $request){

	$records = Cms::query();

			if($request->query('search')){

				$records->whereLike(['page_uses','title'],$request->input('search'));

			}

			

			$users = $records->sortable()->paginate(env('PAGINATION_LIMIT'));

	

		$pageTitle = 'CMS';

		//$pages = Cms::all()->sortByDesc("id")->toArray();

		return view('admin.cms.index', compact('users', 'pageTitle'));

	}

	

	public function add(){

		$pageTitle = 'Add Page';

		return view('admin.cms.add', compact('pageTitle'));

	}

	

	public function store(Request $request){

		

		if($request->isMethod('post')){

			$validator = Validator::make($request->all(), [

				'page_uses' => 'required|max:100',

				'title' => 'required|max:100',

				'meta_title' => 'required|max:250',

				'meta_description' => 'required|max:400',

				'content' => 'required',

				'image' => 'nullable|image|mimes:jpeg,png,jpg|max:2048',

			]);

			

			$msg = [
				'message' => 'Something wents wrong!!!',
				'alert-type' => 'error'
			];
			if($validator->fails()){
				 return redirect()->intended(route('admin.addCms'))
                        ->withErrors($validator)
                        ->withInput()
						->with($msg);
						
			}

			

			$pageData = $request->post();

			

			/*$imageName = time().'.'.request()->featureImage->getClientOriginalExtension();

			request()->featureImage->move(public_path('uploads/cms'), $imageName); */

			

			$pageData['slug'] = $this->cms->createSlug($request->title);

			//$pageData['featureImage'] = $imageName;

			

			$data = Cms::create($pageData);

			$notification = [

				'message' => 'Page created successfully!!!',

				'alert-type' => 'success'

			];

			return redirect()->intended(route('admin.cms'))->with($notification);

			

		}

	}

	

	public function edit($id){

		$pageTitle = 'Edit Page';

        $page = Cms::find($id);

        return view('admin.cms.edit',compact('page','pageTitle'));

	}

	

	public function update(Request $request, $id){

		if($request->isMethod('PUT')){

			$page = Cms::find($id);

			$page_name = $request->page_name;

			$validator = Validator::make($request->all(), [

				'page_uses' => 'required|max:100',

				'title' => 'required|max:100',

				'meta_title' => 'nullable|max:250',

				'meta_description' => 'nullable|max:400',

				'content' => 'required',

				

			]);

			
			$msg = [
				'message' => 'Something wents wrong!!!',
				'alert-type' => 'error'
			];
			if($validator->fails()){
				 return redirect(route('admin.editCms', $page->id))
                        ->withErrors($validator)
                        ->withInput()
						->with($msg);
			} 
			 if($request->hasFile('image')){

				

				$validator = Validator::make($request->all(), [

					'image' => 'required|image|mimes:jpeg,png,jpg|max:2048||dimensions:min_width=1349,min_height=235'

				

				]);

				

				if($validator->fails()){
					 return redirect(route('admin.editCms', $page->id))
							->withErrors($validator)
							->withInput();
				} 

				

				//unlink(public_path().'/uploads/cms/'.$page->featureImage);

				

				$imageName = time().'.'.request()->image->getClientOriginalExtension();

				request()->image->move(public_path('uploads/cms'), $imageName);

				

				$page->featureImage = $imageName;

			

			 }

			

			

			

			$page->update($request->post());
           $notification = [
				'message' => 'Page updated successfully!!!',
				'alert-type' => 'success'
			];
			return redirect()->intended(route('admin.cms'))->with($notification);

		}

	}

	

	public function destroy($id)

    {

		$page = Cms::find($id);

		unlink(public_path().'/uploads/cms/'.$page->featureImage);

		

        Cms::destroy($id);

		$msg = [

				'message' => 'Page has been deleted!!!',

				'alert-type' => 'success'

			];

        return redirect()->intended(route('admin.cms'))->with($msg);

    }

}

