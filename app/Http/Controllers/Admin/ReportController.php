<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Report;
use App\Model\Post;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\Paginator;


class ReportController extends Controller
{
    
	public function report(Request $request){
		$page = 'Report Post';
		$records = Report::query();
			if($request->query('search')){
				$records->where('user_name', 'LIKE', "%{$request->input('search')}%")->where('type','inappropriate')->where('status','!=',3);
				$records->orWhere('user_email', 'LIKE', "%{$request->input('search')}%")->where('type','inappropriate')->where('status','!=',3);
				$records->orWhere('post_username', 'LIKE', "%{$request->input('search')}%")->where('type','inappropriate')->where('status','!=',3);
				$records->orWhere('post_useremail', 'LIKE', "%{$request->input('search')}%")->where('type','inappropriate')->where('status','!=',3);
				$records->orWhere('post_title', 'LIKE', "%{$request->input('search')}%")->where('type','inappropriate')->where('status','!=',3);
				$records->orWhere('title', 'LIKE', "%{$request->input('search')}%")->where('type','inappropriate')->where('status','!=',3);
				$records->orWhere('msg', 'LIKE', "%{$request->input('search')}%")->where('type','inappropriate')->where('status','!=',3);
			}
			$users = $records->where('type','inappropriate')->where('status','!=',3)->sortable("updated_at")->paginate(env('PAGINATION_LIMIT'));

		return view('admin.report.report',compact('users'));
	}

	public function spam(Request $request){
		$page = 'Reports';
		$records = Report::query();
			if($request->query('search')){
				$records->where('user_name', 'LIKE', "%{$request->input('search')}%")->where('type','spam')->where('status','!=',3);
				$records->orWhere('user_email', 'LIKE', "%{$request->input('search')}%")->where('type','spam')->where('status','!=',3);
				$records->orWhere('post_username', 'LIKE', "%{$request->input('search')}%")->where('type','spam')->where('status','!=',3);
				$records->orWhere('post_useremail', 'LIKE', "%{$request->input('search')}%")->where('type','spam')->where('status','!=',3);
				$records->orWhere('post_title', 'LIKE', "%{$request->input('search')}%")->where('type','spam')->where('status','!=',3);
				$records->orWhere('title', 'LIKE', "%{$request->input('search')}%")->where('type','spam')->where('status','!=',3);
				$records->orWhere('msg', 'LIKE', "%{$request->input('search')}%")->where('type','spam')->where('status','!=',3);
			}
			$users = $records->where('type','spam')->where('status','!=',3)->sortable("updated_at")->paginate(env('PAGINATION_LIMIT'));
		return view('admin.report.spam',compact('users'));
	}

	public function reportChange_status(Request $request){

		if($request->ajax()){

			//$user = User::find($request->seg);

			try{
						//User::where('id', $user->id)->update([]);$data=[AssignMentor];

						$assindata=Report::where('id',$request->id)->get()->toArray();
						$post=Post::where('id',$assindata[0]['post_id'])->get()->toArray();

						if(!empty($assindata) && $assindata[0]['status'] == 0){
                         Post::where('id', $assindata[0]['post_id'])->update(['status'=>1]);
						 Report::where('id', $request->id)->update(['status'=>'1']);
						 $response = array('status'=>1,'msg'=>'Post deactivated successfully.');

						}if(!empty($assindata) && $assindata[0]['status'] == 1){
                             Post::where('id', $assindata[0]['post_id'])->update(['status'=>0]);
                          Report::where('id', $request->id)->update(['status'=>'0']);
                          $response = array('status'=>1,'msg'=>'Post activated successfully.');
						}


					}catch(\Exception $e){
						$response = array('status'=>0,'msg'=>'something went wrong');

					}

		}
	 	return response()->json($response);
	}
    
    public function reportDelete(Request $request){

		if($request->ajax()){
             try{


                         
                         $assindata=Report::where('id',$request->id)->get()->toArray();
                         
                         $post=Post::where('id',$assindata[0]['post_id'])->update(['status'=>3]);
                         Report::where('id',$request->id)->update(['status'=>3]);
                          $response = array('status'=>1,'msg'=>'Post has been deleted successfully.');



					}catch(\Exception $e){
						$response = array('status'=>0,'msg'=>'something went wrong');

					}

		}
	 	return response()->json($response);
	}

	

   
	
}
