<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Category;
use App\Model\Post;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\Paginator;
use App\Model\Chat;
use App\Model\SupportChat;
use PubNub\PubNub;
use PubNub\Enums\PNStatusCategory;
use PubNub\Callbacks\SubscribeCallback;
use PubNub\PNConfiguration;
use PubNub\Exceptions\PubNubException;
use Monolog\Handler\ErrorLogHandler;
use DateTime;
use DB;
use Illuminate\Pagination\LengthAwarePaginator;


class ChatController extends Controller
{

  protected $pubnub;
  protected $pnconf;

  public function __construct()
  {
      $this->pnconf = new PNConfiguration();
      $this->pnconf->setSubscribeKey(env('SubscribeKey'));
      $this->pnconf->setPublishKey(env('PublishKey'));
      $this->pnconf->setSecretKey(env('SecretKey'));
      $this->pnconf->setSecure(false);
      $this->pubnub = new PubNub($this->pnconf);
      //$this->pubnub->ssl => true;
  }
  

    public function supportPost(Request $request)
    {

      $validation = Validator::make($request->all(), [
      'id' => 'required',
       'msg' => 'required',

     ]);
     if($validation->passes())
     {
      $d = SupportChat::find($request->id);
      $data=['client_id'=>$d['client_id'],'msg'=>$request->msg,'channel'=>$d['channel'],'client_email'=>$d['client_email']];
       SupportChat::create($data);
      return response()->json([
       'message'   => 'Data insert','status' => 1,
       ]);
     }
     else
     {
      return response()->json([
       'message'   => $validation->errors()->all(),'status' => 0,
      
     
      ]);
     }
    }

  public function index(Request $request)
    {

      $users = DB::table('chats')
        ->join('posts', 'chats.post_id', '=', 'posts.id')
        ->select('chats.*', 'posts.tittle As tittle')   
        ->where('tittle', 'like', '%' . $request->input('search') . '%')
        ->groupBy('post_id')
        ->orderBy('created_at','Desc')
        ->paginate(env('PAGINATION_LIMIT'));
       // print_r($users); die;
    	/*$records = PersonalChat::query()->with([
			       'Postdata',
			       ]);
        if($request->query('search')){
              $records->whereLike(['tittle'],$request->input('search'));
        }
        $users = $records->groupBy('post_id')->sortable()->paginate(env('PAGINATION_LIMIT'));*/
        return view('admin.chat.index',compact('users'));
    }

   public function list(Request $request, $id)
    {
     try {
            $data=Chat::where('id',$id)->pluck('post_id');
            $result = $this->pubnub->history()
						->channel($data[0])
            //->startTimeoken()
           // ->endTimeoken()
            ->includeTimetoken(true)
						->sync();
            
			$dataArray=[];
			$i=1;
     // print_r($result->getMessages()); die;
      $start_timetoken=[];
			foreach($result->getMessages() as $key => $value){
				$msg=$value->getEntry();
				
				$data1= User::where('id',$msg[0]['user']['_id'])->get()->toArray();
				$msg1['_id'] = $i; 
        $msg1['channel'] = $data[0];
				$msg1['text'] = $msg[0]['text'];
				$msg1['createdAt'] = $msg[0]['createdAt'];
        if($key == 0){
           $msg1['start_timeToken']= 0;
        }else{ 
          $k =$key-1;
         $msg1['start_timeToken']=$result->getMessages()[$k]->getTimetoken();
       }
        $msg1['timetoken']=$value->getTimetoken();
				
				$msg1['user'] =[
						'_id' => $data1[0]['id'], 
						'name' => $data1[0]['name'], 
						'avatar'=> $data1[0]['image'],
				];
	            array_push($dataArray,$msg1);
				$i++;
			// }
            }
            if(!empty($request->search)){ 
            $dataReverse1=collect($dataArray)->where('text', $request->search);
           // $dataReverse1=  array_reverse($srch);
           }else{
             $dataReverse1=  array_reverse($dataArray);
            }
              $currentPage = LengthAwarePaginator::resolveCurrentPage();
               $productCollection = collect($dataReverse1);
                $perPage = 10;
                $currentPageproducts = $productCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
                 $dataReverse= new LengthAwarePaginator($currentPageproducts , count($productCollection), $perPage);
                 $dataReverse->setPath($request->url());
          // print_r($dataReverse); die;
	           return view('admin.chat.list',compact('dataReverse'));
        
        }
		catch(\PubNub\Exceptions\PubNubServerException $e) {
		      	return json_encode(array('msg'=>'Data not found!','status'=>false));
		         // echo "Error happened while publishing: " . $e->getMessage();
        }
    }

    public function delete(Request $request){ 
      if($request->ajax()){
         try{
               //print_r($request->all()); die;
               $result = $this->pubnub->deleteMessages()
    					    ->channel($request->channel)
                  ->start($request->start_timeToken)
                  ->end($request->end_timetoken)
    					    ->sync();
                $response = array('status'=>1,'msg'=>'Message has been deleted successfully.');
            }
    		catch(\PubNub\Exceptions\PubNubServerException $e) {
    		      $response = array('status'=>0,'msg'=>'something went wrong');
    		}
    }
    return response()->json($response);
  }

   public function support(Request $request)
    {

      $pageTitle = 'Manage SupportChat';
       //$users = User::sortable()->paginate(5);
      $records = SupportChat::query()->groupBy('client_id')->orderBy('created_at', 'DESC');
      if($request->query('search')){

      $records->where('client_name', 'LIKE', "%{$request->input('search')}%");
      $records->orWhere('client_email', 'LIKE', "%{$request->input('search')}%");
      }

      $users = $records->sortable("created_at")->paginate(env('PAGINATION_LIMIT'));
        return view('admin.chat.support',compact('pageTitle','users'));
    }

    public function support_chat(Request $request, $id)
    {
        $data=SupportChat::where('id',$id)->pluck('channel');
 
     

         return view('admin.chat.chat',compact('data'));
            }  

            public function fileSave(Request $request){

         $validation = Validator::make($request->all(), [
      'file' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
     ]);
     if($validation->passes())
     {
      $image = $request->file('file');
      $new_name = rand() . '.' . $image->getClientOriginalExtension();
      $image->move(public_path('images'), $new_name);
      return response()->json([
       'message'   => 'https://onlineprojectprogress.com/laravel/qui/images/'.$new_name.'',
       
      
      
      ]);
     }
     else
     {
      return response()->json([
       'message'   => $validation->errors()->all(),
      
     
      ]);
     }
    }

  
    
	

   
	
}
