<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Category;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\Paginator;


class CategoryController extends Controller
{
    
	public function edit($id){
		$page = 'Edit Category';
		$post = Category::find($id);
		return view('admin.category.edit',compact('post','page'));
	}


	public function update(Request $request, $id){
		if($request->isMethod('PUT')){
			$Post = Category::find($id);

			$validator = Validator::make($request->all(), [
				'name' => 'required',
				]);

			$msg = [
				'message' => 'Something wents wrong!!!',
				'alert-type' => 'error'
			];
			if($validator->fails()){
				return redirect(route('admin.edit.category', $Post->id))
				->withErrors($validator)
				->withInput()
				->with($msg);
			}else{
                   if($request->hasFile('image')){

						$validator = Validator::make($request->all(), [
							'image' => 'required|image|mimes:jpeg,png,jpg|max:2048'
						]);

						if($validator->fails()){
							return redirect(route('admin.edit.category', $Post->id))
							->withErrors($validator)
							->withInput();
						}

						//unlink(public_path().'/upload/image/'.$Post->image);

						$logo = time().'.'.request()->image->getClientOriginalExtension();
				request()->image->move(public_path('admin/images/add-more-icons/img'), $logo);

					$imageName=  $logo;


						$Post->image = $imageName;


					}

					$Post->update($request->post());

					$notification = [
						'message' => 'Category updated successfully!!!',
						'alert-type' => 'success'
					];

                     return redirect()->intended(route('admin.category'))->with($notification);

				}
			}
			}

			public function categoryDelete(Request $request){

		if($request->ajax()){
             try{


                         Category::where('id', $request->id)->update(['status'=>1]);
                          $response = array('status'=>1,'msg'=>'Post has been deleted successfully.');



					}catch(\Exception $e){
						$response = array('status'=>0,'msg'=>'something went wrong');

					}

		}
	 	return response()->json($response);
	}

   
	
}
