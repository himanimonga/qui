<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Session;
use App\Model\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Validator;
use Hash;

use Maatwebsite\Excel\Facades\Excel;

class SubadminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }
	
	
	public function subadmin_list(Request $request)
    {
	$pageTitle = 'Admin Users';
	$records = Admin::query()->where('type','sub_admin');
			if($request->query('search')){
				$records->whereLike(['name','email'],$request->input('search'));
			}
			
			$users = $records->paginate(env('PAGINATION_LIMIT'));
	//$data['subadmin']=Admin::all()->where('type','sub_admin')->get();
		return view('admin.subadmin.index', compact('users', 'pageTitle'));
    }
	
	public function delete_subadmin(Request $request)
	{
		if($request->ajax()){
				
				$team = Admin::find($request->id);
				
				if(!empty($team)){
					Admin::destroy($request->id);
					$response = array('status'=>1,'msg'=>'Sub Admin has been deleted successfully.');	
					}else{
					$response = array('status'=> 0, 'msg'=>'Sub Admin  does not exists in our records'); 
				}
			}
			return response()->json($response);
	}
	
	public function edit_subadmin($id)
	{
	$pageTitle = 'Edit Subadmin';
			$blog = Admin::find($id);
			
		return view('admin.subadmin.edit',compact('blog','pageTitle'));
	}
	
	public function subadmin_updatemodule(Request $request)
    {
	    $blog = Admin::find($request->id);
		if($request->isMethod('post')){
		$validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'task' => 'required',
            'email' => 'required',
            ]);
			$msg = [
				'message' => 'Something wents wrong!!!',
				'alert-type' => 'error'
				];
				if($validator->fails()){
					return redirect()->back()
					->withErrors($validator)
					->withInput()
					->with($msg);
					
				}
			
	    $tas=$request->task;
		
		$task= implode(",",$tas);
		
		 //$id=$request->id;
		$data=[
			'name'  	=> $request->name,
			'task'  	=> $task,
			'email'     => $request->email,
			
		];	
		//print_R($data); die;
		$subadmin = $blog->update($data);
		//print_r($subadmin); die;
        
		$notification = [
				'message' => 'User has been updated successfully!!!',
				'alert-type' => 'success'
				];
		return redirect()->intended(route('admin.subadmin_list'))->with($notification);
		
      	}		
	}
	
	
	public function updatePassword_subadmin($id){	
		$pageTitle = 'Change Password';
		$user =Admin::find($id)->toArray();
        //return $userfertilityservices;
        return view('admin.subadmin.updatePassword',compact('user','pageTitle'));
	}

    public function update_password(Request $request){
			
			$validator = Validator::make($request->all(), [
				 'password'=> ['required','string','min:8'],
                 'password_confirmation'=> ['required','same:password'],
				]);
				
				$msg = [
				'message' => 'Something wents wrong!!!',
				'alert-type' => 'error'
				];
				if($validator->fails()){
					return redirect(route('admin.updatePassword_subadmin', $request->id))
					->withErrors($validator)
					->withInput()
					->with($msg);
				} 
				
				$newpassword= Hash::make($request->password);
				//print_r($newpassword); die;
					$fighterapprove = Admin::where('id', $request->id)->update(['password' => $newpassword]);
					
				
				$notification = [
				'message' => 'Password updated successfully!!!',
				'alert-type' => 'success'
				];
				return redirect()->back()->with($notification);
			
		}
		
	
	

 
}
