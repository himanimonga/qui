<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\User;
use App\Http\Requests\Admin\UserRequest;
use Hash;
class UsersController extends Controller
{
    public function user(Request $request)
    {
        $pageTitle = 'Manage Users';
        //$users = User::sortable()->paginate(5);
        $records = User::query();
        if($request->query('search')){
            $records->where('name', 'LIKE', "%{$request->input('search')}%");
            $records->orWhere('email', 'LIKE', "%{$request->input('search')}%");
            $records->orWhere('username', 'LIKE', "%{$request->input('search')}%");
        }

        $users = $records->where('deleteStatus',0)->sortable()->paginate(env('PAGINATION_LIMIT'));

        return view('admin.user.index',compact('pageTitle','users'));
    }

     public function active(Request $request)
    {
        $pageTitle = 'Manage Active Users';
        //$users = User::sortable()->paginate(5);
        $records = User::query();
        if($request->query('search')){
            $records->where('name', 'LIKE', "%{$request->input('search')}%")->where('status',0);
            $records->orWhere('email', 'LIKE', "%{$request->input('search')}%")->where('status',0);
            $records->orWhere('username', 'LIKE', "%{$request->input('search')}%")->where('status',0);
        }

        $users = $records->where('deleteStatus',0)->where('status',0)->sortable()->paginate(env('PAGINATION_LIMIT'));

        return view('admin.user.index',compact('pageTitle','users'));
    }

     public function inactive(Request $request)
    {
        $pageTitle = 'Manage Inactive Users';
        //$users = User::sortable()->paginate(5);
        $records = User::query();
        if($request->query('search')){
            $records->where('name', 'LIKE', "%{$request->input('search')}%")->where('status',1);
            $records->orWhere('email', 'LIKE', "%{$request->input('search')}%")->where('status',1);
            $records->orWhere('username', 'LIKE', "%{$request->input('search')}%")->where('status',1);
        }

        $users = $records->where('deleteStatus',0)->where('status',1)->sortable()->paginate(env('PAGINATION_LIMIT'));

        return view('admin.user.index',compact('pageTitle','users'));
    }

    public function create(){
        $record = new User;
        return view('admin.user.create', compact('record'));
    }

    public function store(UserRequest $request) {
        $allData = $request->validated();
        $allData['password'] = Hash::make('password');
        $allData['email_verified_at'] = date('Y-m-d');
        $client = User::create($allData);
        return redirect()->route('admin.user')->with(['success'=>'User added successfully.']);
    }

    public function edit($id)
    {
        $record = User::findOrFail($id);
        $record->password=null;
//        dd($record->video_url_array);
        return view('admin.user.edit')->with(compact('record'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $record = User::findOrFail($id);
        $allData=$request->validated();
        if(!empty($allData['password'])){
            $allData['password'] = Hash::make('password');
        }else{
            $allData['password']=$record->password;
        }
//        dd($allData);
        $record->fill($allData);
        $record->save();
        return redirect()->route('admin.user')->with(['success'=>'User information has been updated successfully.']);
    }


    public function user_status(Request $request){

        if($request->ajax()){
            //return response()->json($request->id); die;

            //$user = User::find($request->seg);

            try{
                //User::where('id', $user->id)->update([]);$data=[AssignMentor];

                $assindata=User::where('id',$request->id)->get()->toArray();

                if(!empty($assindata) && $assindata[0]['deleteStatus'] == 0){

                    User::where('id', $request->id)->update(['deleteStatus'=>'1']);
                    $response = array('status'=>1,'msg'=>'User account temporary deleted successfully.');

                }if(!empty($assindata) && $assindata[0]['deleteStatus'] == 1){

                    User::where('id', $request->id)->update(['deleteStatus'=>'0']);
                    $response = array('status'=>1,'msg'=>'User account restored successfully.');
                }


            }catch(\Exception $e){
                $response = array('status'=>0,'msg'=>'something went wrong');

            }

        }
        return response()->json($response);
    }


    public function userDestroy(Request $request){

        if($request->ajax()){

            //$user = User::find($request->seg);

            try{
                //User::where('id', $user->id)->update([]);$data=[AssignMentor];

                $assindata=User::where('id',$request->id)->get()->toArray();

                if(!empty($assindata) && $assindata[0]['status'] == 0){

                    User::where('id', $request->id)->update(['status'=>'1']);
                    $response = array('status'=>1,'msg'=>'User account deactivated successfully.');

                }if(!empty($assindata) && $assindata[0]['status'] == 1){

                    User::where('id', $request->id)->update(['status'=>'0']);
                    $response = array('status'=>1,'msg'=>'User account activated successfully.');
                }


            }catch(\Exception $e){
                $response = array('status'=>0,'msg'=>'something went wrong');

            }

        }
        return response()->json($response);
    }

    public function userDisable(Request $request){

        if($request->ajax()){

            //$user = User::find($request->seg);

            try{
                //User::where('id', $user->id)->update([]);$data=[AssignMentor];

                $assindata=User::where('id',$request->id)->get()->toArray();

                if(!empty($assindata) && $assindata[0]['deleteStatus'] == 1){

                    User::where('id', $request->id)->delete();
                    $response = array('status'=>1,'msg'=>'User account deleted successfully.');
                }


            }catch(\Exception $e){
                $response = array('status'=>0,'msg'=>'something went wrong');

            }

        }
        return response()->json($response);
    }
    public function disabled(Request $request)
    {
        $pageTitle = 'Manage Disabled';
        //$users = User::sortable()->paginate(5);
        $records = User::query()->where('deleteStatus','1');

        if($request->query('search')){
            // $records->where('deleteStatus','1');
            $records->where('name', 'LIKE', "%{$request->input('search')}%");
            $records->orWhere('email', 'LIKE', "%{$request->input('search')}%");
            $records->orWhere('username', 'LIKE', "%{$request->input('search')}%");
            $records->where('deleteStatus','1');
        }

        $users = $records->where('deleteStatus','1')->sortable()->paginate(env('PAGINATION_LIMIT'));

        return view('admin.user.deleteStatus',compact('pageTitle','users'));
    }
}
