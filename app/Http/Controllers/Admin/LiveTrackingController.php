<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Category;
use App\Model\Post;
use App\Help;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\Paginator;
use App\Model\PersonalChat;
use PubNub\PubNub;
use PubNub\Enums\PNStatusCategory;
use PubNub\Callbacks\SubscribeCallback;
use PubNub\PNConfiguration;
use PubNub\Exceptions\PubNubException;
use Monolog\Handler\ErrorLogHandler;
use DateTime;


class LiveTrackingController extends Controller
{

 public function __construct()
  {
     }

  public function index($id)
    {

    	$users = Post::where('id',$id)->get()->toArray();
      return view('admin.post.help.track',compact('users'));
    }

  
	
}
