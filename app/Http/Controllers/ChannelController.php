<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Notifications\forgotPassword;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Model\passwordReset;
use App\User;
use App\Admin;
use App\Model\UserReaction;
use App\Model\Report;
use App\Model\Notification;
use App\Model\JoinLocation;

use Illuminate\Support\Facades\Validator;
use Session;
use App\Model\Category;
use App\Model\Feedback;
use App\Model\Favourite_geolocation;
use App\Model\Post;
use App\Model\ChatJoin;
use App\Model\SupportChat;
use App\Model\Chat;
use DB;
use App\Model\Help;
use App\Channel\GetPublichannel;
use PubNub\PubNub;
use PubNub\Enums\PNStatusCategory;
use PubNub\Callbacks\SubscribeCallback;
use PubNub\PNConfiguration;
use PubNub\Exceptions\PubNubException;
use Monolog\Handler\ErrorLogHandler;

class ChannelController extends Controller
{

    protected $pubnub;
    protected $pnconf;


    public function __construct()
    {
      //$this->middleware('api_token');
        $this->pnconf = new PNConfiguration();
        $this->pnconf->setSubscribeKey(env('SubscribeKey'));
        $this->pnconf->setPublishKey(env('PublishKey'));
        $this->pnconf->setSecretKey(env('SecretKey'));
        $this->pnconf->setSecure(false);
        $this->pubnub = new PubNub($this->pnconf);
        //$this->pubnub->ssl => true;
    }

     public function get_publicChannel(Request $request){ 
        try {
         $request->validate([
          'user_id' => 'required',
          'search' => 'nullable',
          'latitude' => 'nullable',
          'longitude' => 'nullable',
          'type' => 'required',
        ]);

          $searchTerm = $request->search;
          $latitude = $request->latitude;
          $longitude = $request->longitude;
          if(!empty($latitude) && !empty($longitude)){
            $latitude = $request->latitude;
          $longitude = $request->longitude;
        }else{
          $latitude = 0;
          $longitude = 0;
        }
         
           
 
           $allData1 = Chat::select(['chats.*','posts.latitude','posts.longitude','posts.tittle',
             DB::RAW('CAST( 6371 * acos( cos( radians(' . $latitude . ') ) * cos( radians( posts.latitude ) )
              * cos( radians(  posts.longitude ) - radians(' . $longitude . ') ) 
              + sin( radians(' . $latitude .') ) * sin( radians(posts.latitude) ) )
                AS DECIMAL(10,2)) AS distance')])
            ->join('posts', 'posts.id', '=', 'chats.post_id')->withCount('user')
            ->where('chats.deleteChannel',0)
            //->where('chats.join_status',0)
           ->whereRaw(
              'chats.id in (
                select max(chats.id) from chats where 
                deleteChannel=0 AND 
                (
                  (chats.type = "discover") OR 
                  ((chats.uuid = '.$request->user_id.' OR chats.client_id = '.$request->user_id.' And chats.join_status != 1)) 
                )
               AND (`chats`.`message` LIKE "%'.$searchTerm.'%" OR EXISTS(
                    SELECT
                        *
                    FROM
                        `users`
                    WHERE
                        `chats`.`client_id` = `users`.`id` AND (`name` LIKE "%'.$searchTerm.'%") OR (`post_name` LIKE "%'.$searchTerm.'%")
                ) OR EXISTS(
                    SELECT
                        *
                    FROM
                        `users`
                    WHERE
                        `chats`.`uuid` = `users`.`id` AND (`name` LIKE "%'.$searchTerm.'%") OR (`post_name` LIKE "%'.$searchTerm.'%")
                ))
                group by chats.channel)
              
            ');

        // print_r($allData1); die;
          
           if($request->type == 'Discover'){ 
             if(!empty($request->search)){
               $allData2 = $allData1->with('Postdata.children')->orderByRaw('distance', 'ASC');

             }else{ 
            $allData2 = $allData1->with('Postdata.children')->orderByRaw('distance', 'ASC')->having("distance", "<=",10);
           }
         }else{
           $allData2 = $allData1->with('Postdata.children')->orderBy('created_at','Desc');
         }
         $allData=  $allData2->get()->toArray();
          
           
           $privateChat = Chat::where('deleteChannel',0)->where('post_id',0)
           ->whereRaw(
              'chats.id in (
                select max(chats.id) from chats where 
                deleteChannel=0 AND 
                (
                  (chats.type = "private" AND chats.post_id = "0") AND 
                  ((chats.uuid = '.$request->user_id.' OR chats.client_id = '.$request->user_id.' And chats.join_status != 1)) 
                )
               AND (`chats`.`message` LIKE "%'.$searchTerm.'%" OR `chats`.`post_name` LIKE "%'.$searchTerm.'%"  OR EXISTS(
                    SELECT
                        *
                    FROM
                        `users`
                    WHERE
                        `chats`.`client_id` = `users`.`id` AND (`name` LIKE "%'.$searchTerm.'%" OR `username` LIKE "%'.$searchTerm.'%")
                ) OR EXISTS(
                    SELECT
                        *
                    FROM
                        `users`
                    WHERE
                        `chats`.`uuid` = `users`.`id` AND (`name` LIKE "%'.$searchTerm.'%" OR `username` LIKE "%'.$searchTerm.'%")
                )
            )
                group by chats.channel)
              
            ')
           ->orderBy('id','Desc')
           ->get()->toArray();

            $discover= Chat::select(['chats.*','posts.latitude','posts.longitude','posts.tittle',
             DB::RAW('CAST( 6371 * acos( cos( radians(' . $latitude . ') ) * cos( radians( posts.latitude ) )
              * cos( radians(  posts.longitude ) - radians(' . $longitude . ') ) 
              + sin( radians(' . $latitude .') ) * sin( radians(posts.latitude) ) )
                AS DECIMAL(10,2)) AS distance')])
            ->join('posts', 'posts.id', '=', 'chats.post_id')->withCount('user')
            ->where('chats.deleteChannel',0)
            //->where('chats.join_status',0)
           ->whereRaw(
              'chats.id in (
                select max(chats.id) from chats where 
                deleteChannel=0 AND 
                (
                  (chats.type = "discover")
                )
              
                group by chats.channel)
              
            ');
             if(!empty($request->search)){
               $alldiscover = $discover->whereHas('Userdata', function ($query) use ($request){
                   $query->where('username', 'like', '%'.$request->search.'%');
                    })->orWhere('posts.tittle', 'LIKE', '%' . $request->search . '%')->with('Postdata.children')->orderByRaw('distance', 'ASC')->get()->toArray();


             }else{ 
              $alldiscover = $discover->with('Postdata.children')->orderByRaw('distance', 'ASC')->having("distance", "<=",10)->get()->toArray();
           }
           $data =array_merge($privateChat,$allData);
           
          // $unreadmsg1=[];
                      $channels=[];
          foreach($data as $value){

            $unread = ChatJoin::where(['channel'=>$value['channel'],'join_id'=>$request->user_id,'status'=>0])->pluck('join_id')->toArray();

            if(!empty($unread)){
              //$unreadmsg=Chat::where(['channel'=>$value['channel'],'deleteChannel'=>0,'join_status'=>0])->get()->toArray();
              $channel = $value['channel'];
              array_push($channels,$channel);
             // array_push($unreadmsg1,$unreadmsg);
            }

          }
         
         $unreadmsg=Chat::where(['deleteChannel'=>0,'join_status'=>0])->whereIn('channel',$channels)->get()->toArray();
         //print_r($unreadmsg); die;
        if(!empty($unreadmsg)){
          $sum=[];
          $sum2=[];
          $i=1;
          foreach($unreadmsg as $value){
            if(!empty($value['unreadmsg']) ){
              $array= (explode(",",$value['unreadmsg']) );
              
              if (in_array($request->user_id, $array))
              {
                      
              }
              else
              {

               // $sum1['index'] = $i;
                $sum1 = $value['channel'];
               // $location_id = $value['location_id'];
                array_push($sum,$sum1);
               //  array_push($sum2,$location_id);
              //$sum[] = $value['unreadmsg'];
              }
              //return json_encode(array('msg'=>'Data inserted!','status'=>true));
            }else{

             // $sum1['index'] = $i;
              $sum1 = $value['channel'];
             //  $location_id = 0;
              array_push($sum,$sum1);
              // array_push($sum2,$location_id);
            }
           
              $i++;
          }
         
        }else{
         $datamsg['channel'] = 0;
         $datamsg['unreadmsg_count']=0;
        }
      //print_r($sum2); die;
        $mainData2 =[];
        foreach($data as $value){
          $mainData3 = $value;
          if(!empty($sum)){
           if(in_array($value['channel'],$sum)){
            $d=array_intersect($sum,array($value['channel']));
               $mainData3['channel']=$value['channel'];
          $mainData3['unreadmsg_count']=count($d);
          if(!empty($value['location_id'])){ 
          $mainData3['location_id']=$value['location_id'][0];
        }
         //print_r($value['location_id']); die;
           }else{
            $mainData3['channel']=$value['channel'];
          $mainData3['unreadmsg_count']=0;
           }
          
          

        }else{
         $mainData3['channel'] =$value['channel'];
         $mainData3['unreadmsg_count']=0;
        }
         array_push($mainData2,$mainData3);


        }
         
       // print_r($mainData2);
        $data1 = JoinLocation::with('Postdata.children')->where(['user_id'=>$request->user_id,'status'=>0])->whereHas('Join_Userdata', function ($query) use ($searchTerm){
          $query->orWhere('username', 'like', '%'.$searchTerm.'%');
           })->where('location_name', 'like', '%'.$searchTerm.'%')->get()->toArray();

      // print_r($data1); die;
        $data2=array_merge($data1,$mainData2);
        $this->array_sort_by_column($data2, 'created_at');
        
          if(!empty($searchTerm)){    
            $ids=User::where('username', 'like', '%'.$searchTerm.'%')->pluck('id');
              $joinids  = ChatJoin::where('join_id',$request->user_id)->whereIn('uuid',$ids)->pluck('uuid');
              //print_r($joinids); die;
              $user=User::whereNotIn('id',$joinids)->where('username', 'like', '%'.$searchTerm.'%')->where('id','!=',$request->user_id)->get()->toArray();

              if(!empty($user)){ 
                $userData=[];
              foreach($user as $u){
                $u1 = $u;
                $u1['client_id'] = $u['id'];
                $u1['type'] = 'private';
                $u1['user_name'] = $u['name'];
                $u1['user_image'] = $u['image'];
                array_push($userData,$u1);
              }
               }else{$userData='';}
           }else{$userData='';}

           $support = SupportChat::where('client_id',$request->user_id)->orderBy('id','Desc')->first();
           if(!empty($support)){
           $supportChat = ['msg'=>$support['msg'],'date'=>$support['created_at']];
           }

        if($data2 || $userData || $support || $alldiscover){
        return json_encode(array('msg'=>'Data found!','data'=>(!empty($data2)) ? $data2 : 'Data not found','newuser'=>(!empty($userData)) ? $userData : '','support'=>(!empty($support)) ? $supportChat : '','discover'=>$alldiscover,'status'=>true));
        }else{
        return json_encode(array('msg'=>'Data not found.','status'=>false));
        }
      } catch (\Exception $e) {

        return json_encode(array('msg'=>$e->getMessage(),'status'=>false));
        //return $e->getMessage();
    }
   }

     public function blockUnblock(Request $request)
    {
      
       $request->validate([
              'user_id' => 'required',
              'channel' => 'required',
        ]);
        $data = Chat::where(['uuid'=>$request->user_id,'channel'=>$request->channel])->pluck('status')->take(1)->toArray();
  
       if(!empty($data)){
       if($data[0] == 0){
       // $update= Chat::where(['uuid'=>$request->user_id,'channel'=>$request->channel])->update(['status'=>1]);
        //print_r($update); die;
         return json_encode(array('msg'=>'User blocked!','status'=>true));
       }else{
          //Chat::where(['uuid'=>$request->user_id,'channel'=>$request->channel])->update(['status'=>0]);
      return json_encode(array('msg'=>'User unblocked!','status'=>true));
       }
     }else{
       return json_encode(array('msg'=>'Data not found!','status'=>false));
     }
    }
  
   public function deleteChannel(Request $request){
       // return json_encode(array('msg'=>$request->user_id,'status'=>true)); die;
       $request->validate([
              'user_id' => 'required',
              'channel' => 'required',
              'location_id'=>'nullable',
              'type'=>'nullable',
        ]);
      if(!empty($request->location_id)){
        if(!empty($request->type) && $request->type == 'discover'){
        $userId= joinLocation::where(['post_id'=>$request->location_id])->pluck('user_id')->first();
       
        if($userId == $request->user_id){ 
          $update1= Post::where(['id'=>$request->location_id])->update(['status'=>1]);
        
          $update2= Post::where(['location_id'=>$request->location_id])->update(['status'=>1]);
          $data = Chat::where(['deleteChannel'=>0,'post_id'=>$request->location_id])->update(['deleteChannel'=>1]);
          $updatee= joinLocation::where(['post_id'=>$request->location_id])->update(['status'=>1]);
         
          $update5= Post::where(['location_id'=>$request->location_id])->get()->toArray();
          foreach($update5 as $key =>$value){
           $data = Chat::where(['deleteChannel'=>0,'post_id'=>$value['id']])->update(['deleteChannel'=>1]);
          ChatJoin::where(['status'=>0,'post_id'=>$value['id']])->update(['status'=>1]); 
            $update3= joinLocation::where(['post_id'=>$value['id']])->update(['status'=>1]);
          }
        return json_encode(array('msg'=>'Channel deleted!!','status'=>true));
       }else{
        return json_encode(array('msg'=>'Sorry! You are not creator of channel','status'=>false));
       }
        }else{
          //$updatechat= Chat::where(['post_id'=>$request->location_id])->update(['status'=>1]);
       
          $update= joinLocation::where(['id'=>$request->location_id])->update(['status'=>1]);
        return json_encode(array('msg'=>'Channel deleted!!','status'=>true));
        }
         
       }else{
          $data = Chat::where(['uuid'=>$request->user_id,'channel'=>$request->channel,'deleteChannel'=>0])->count();
          // print_r($data); die;
         if(!empty($data)){
         if($data > 0){
          $update= Chat::where(['uuid'=>$request->user_id,'channel'=>$request->channel])->update(['deleteChannel'=>1]);
          return json_encode(array('msg'=>'Channel deleted!','status'=>true));
         }
        }else{
         return json_encode(array('msg'=>'Data not found!','status'=>false));
       
       }
     }
  }


  public function muteUnmute(Request $request)
    {
       $request->validate([
              'user_id' => 'required',
              'channel' => 'required',
              'mute_unmute' => 'required',
        ]);

       if($request->mute_unmute == true){

         $data = Chat::where(['uuid'=>$request->user_id,'channel'=>$request->channel])->pluck('muteUnmute')->take(1)->toArray();
          $data1 = Chat::where(['uuid'=>$request->user_id])->pluck('status')->take(1)->toArray();
             if(!empty($data)){
       if($data[0] == 0){
          return json_encode(array('msg'=>'Chat umute!','status'=>false,'notification_status'=>$data1[0] ,'chatMute'=>false));
       }else{
          return json_encode(array('msg'=>'Chat mute!!','status'=>true,'notification_status'=>$data1[0],'chatMute'=>true));
       }
     }else{
       return json_encode(array('msg'=>'Data not found!','status'=>false));
     }


       }else{
        $data = Chat::where(['uuid'=>$request->user_id,'channel'=>$request->channel])->pluck('muteUnmute')->take(1)->toArray();
        $data1 = Chat::where(['uuid'=>$request->user_id])->pluck('status')->take(1)->toArray();
       if(!empty($data)){
       if($data[0] == 0){
        $update= Chat::where(['uuid'=>$request->user_id,'channel'=>$request->channel])->update(['muteUnmute'=>1]);
          return json_encode(array('msg'=>'Chat mute!','notification_status'=>$data1[0] ,'status'=>true));
       }else{
          Chat::where(['uuid'=>$request->user_id,'channel'=>$request->channel])->update(['muteUnmute'=>0]);
      return json_encode(array('msg'=>'Chat unmute!!','notification_status'=>$data1[0] ,'status'=>false));
       }
     }else{
       return json_encode(array('msg'=>'Data not found!','status'=>false));
     }
   }
    }

    public function storechannelPost(Request $request)
    {

       $validator = Validator::make($request->all(),[
      'post' => 'required',
      'tittle' => 'required',
      'latitude' => 'required',
      'longitude' => 'required',
      'description' => 'required',
      'post_type'  => 'required',
      //'image' => 'required|mimes:jpeg,jpg,png',
       'image' => 'nullable',
      'user_image' => 'nullable',
      'user_name' => 'nullable',
     // 'user_last_name' => 'required',
      'user_id' => 'required',
      'type' => 'required',
     
      ]);
      if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{ 
        $data=$request->all();
        
       if(!empty($request->image)){ 
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('upload/image'), $imageName);
        }else{
          $imageName='';
        }
        $userData=Post::where('id',$request->user_id)->get()->toArray();
      
            
       $data['image'] = $imageName;
       $data['type'] = 'Discover';
       $getId=Post::create($data);
       $data1=[ 'post_id' => $getId->id,
                'join_id' => $request->user_id,
                'uuid' => $request->user_id,
               'channel' => $getId->id,
         ];
         ChatJoin::create($data1);
         Chat::create(['channel'=>$getId->id,'uuid'=>$request->user_id,'client_id'=>$request->user_id,'post_id'=>$getId->id,'type'=>'Discover','join_status'=>1,'status'=>0]);
         $data1=['user_id'=>$request->user_id,'location_name'=>$request->tittle,'latitude'=>$request->latitude,'longitude'=>$request->longitude,'join'=>0,'image'=>$request->imageName,'type'=>1,'post_id'=>$getId->id,'location_type'=>1];
                      $storeData= JoinLocation::create($data1);
          $userData1 = User::select(['*', DB::RAW('CAST( 6371 * acos( cos( radians(' . $request->latitude . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $request->longitude . ') ) + sin( radians(' . $request->latitude .') ) * sin( radians(latitude) ) )  AS DECIMAL(10,2)) AS distance')])->where('id','!=',$request->user_id)->where(['other_notification'=>0])->having( 'distance', '<=', 10 )->get()->toArray();
           $creator=User::find($request->user_id);
           $ids=[];
        foreach($userData1 as $value){
      
               array_push($ids,$value['id']);
                if($value['id'] != $request->user_id){
              $notifydata=['title'=>$request->tittle,'description'=>$creator->name.' created a channel near you!','user_name'=>$value['name'],'user_email'=>'','user_id'=>$value['id'],'post_id'=>$getId->id,'type'=>'user'];
                Notification::create($notifydata); 
                }
          }
          //print_r($ids); die;

          $notifydata=['pn_gcm'=>['data'=>['text'=>$request->tittle,'message'=>$request->description,'uuids'=>$ids,'popular'=>$request->tittle]]];
           $result = $this->pubnub->publish()
              ->channel('notify')
              ->message($notifydata)
              ->sync();
       //print_r($getId->id); die;
       $data1=['post_id'=>$getId->id,'client_id'=>$request->user_id,'uuid'=>$request->user_id,'post_name'=>$request->tittle,'channel'=>$getId->id,'image'=>$imageName,'type'=>'Discover'];
       Chat::create($data1);
        // $url= str_replace("/api/auth/storeNeeded","/share_page",\Request::fullUrl());
         
           $needyData=[ 'url'=>  asset('/share_page/').$getId->id ];
         
          return json_encode(array('msg'=>'Your post has been created successfully.','status'=>true));
          
     } 
    }

     function array_sort_by_column(&$array, $column, $direction = SORT_DESC) {
      $reference_array = array();
      foreach($array as $key => $row) {
        $reference_array[$key] = $row[$column];
      }
      
      array_multisort($reference_array, $direction, $array);
    }
   
   
}
