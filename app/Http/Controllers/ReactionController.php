<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Notifications\forgotPassword;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Model\passwordReset;
use App\User;
use App\Admin;
use App\Model\UserReaction;
use App\Model\Report;
use App\Model\Notification;
use App\Model\JoinLocation;

use Illuminate\Support\Facades\Validator;
use Session;
use App\Model\Category;
use App\Model\Feedback;
use App\Model\Favourite_geolocation;
use App\Model\Post;
use App\Model\ChatJoin;
use App\Model\SupportChat;
use App\Model\Chat;
use DB;
use App\Model\Help;
use App\Channel\GetPublichannel;
use PubNub\PubNub;
use PubNub\Enums\PNStatusCategory;
use PubNub\Callbacks\SubscribeCallback;
use PubNub\PNConfiguration;
use PubNub\Exceptions\PubNubException;
use Monolog\Handler\ErrorLogHandler;

class ReactionController extends Controller
{

    protected $pubnub;
    protected $pnconf;


    public function __construct()
    {
      //$this->middleware('api_token');
        $this->pnconf = new PNConfiguration();
        $this->pnconf->setSubscribeKey(env('SubscribeKey'));
        $this->pnconf->setPublishKey(env('PublishKey'));
        $this->pnconf->setSecretKey(env('SecretKey'));
        $this->pnconf->setSecure(false);
        $this->pubnub = new PubNub($this->pnconf);
        //$this->pubnub->ssl => true;
    }
   public function get_reaction(Request $request)
    {
       $request->validate([
           'post_id' => 'required',
           'post_type' => 'required',
              
      ]);
       $data = [
        'like' => UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'status'=>1])->count('post_id'),
        'dislike' => UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'status'=>0])->count('post_id'),
        'smile' => UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'status'=>3])->count('post_id'),
        'heart' => UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'status'=>4])->count('post_id')
      ];
     
      if($data){
         
        return json_encode(array('msg'=>'Data found!','data'=>$data,'status'=>true));
      }else{
        
        return json_encode(array('msg'=>'Data not found.','status'=>false));
      }
 
      
    
  }


           public function change_reaction(Request $request)
    {
       $request->validate([
              //'id' => 'nullable',
              'user_id' => 'required',
              'post_id' => 'required',
              'status' => 'required',
              'post_type' => 'required',
      ]);
   
      $category = UserReaction::where(['user_id'=>$request->user_id,'post_id'=>$request->post_id])->get()->toArray();
     // print_r($category); die;
      if(!empty($category)){
         $status = UserReaction::where(['user_id'=>$request->user_id,'post_id'=>$request->post_id,'status'=>$request->status])->get()->toArray();
         if(!empty($status)){ 
        
         $category = UserReaction::where(['user_id'=>$request->user_id,'post_id'=>$request->post_id,'status'=>$request->status])->update(['status'=>0]);
                     
        }else{

           UserReaction::where(['user_id'=>$request->user_id,'post_id'=>$request->post_id])->update(['status'=>$request->status]);
        }
          $data['totalLike'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type])->where('status','!=',0)->count('post_id');
             $data['totalDislike'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type])->where('status',0)->count('post_id');
          $data['userLike'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'user_id'=>$request->user_id])->where('status','!=',0)->count('post_id');
           $data['smile'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'status'=>1])->count('post_id');
        $data['heart'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'status'=>2])->count('post_id');
        $data['Laugh'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'status'=>3])->count('post_id');
        $data['shoke'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'status'=>4])->count('post_id');
        $data['sad'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'status'=>5])->count('post_id');
        $data['angry'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'status'=>6])->count('post_id');
           $data['status'] =$request->status;
           $user=User::where('id',$request->user_id)->get()->toArray();
           $data['user'] =$user[0]; 
        if($request->header()['lang'][0] == true){
         return json_encode(array('msg'=>'User reaction has changed','data'=>$data,'status'=>true,'likeStatus'=>false));
        }else{
          return json_encode(array('msg'=>'Has cambiado la Reacción','data'=>$data,'status'=>true,'likeStatus'=>false));
        }
        
      }else{
        
       $category = UserReaction::create(['user_id'=>$request->user_id,'post_id'=>$request->post_id,'status'=>$request->status,'post_type'=>$request->post_type]);
            $data['totalLike'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type])->where('status','!=',0)->count('post_id');
             $data['totalDislike'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type])->where('status',0)->count('post_id');
          $data['userLike'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'user_id'=>$request->user_id])->where('status','!=',0)->count('post_id');
           $data['smile'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'status'=>1])->count('post_id');
        $data['heart'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'status'=>2])->count('post_id');
        $data['Laugh'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'status'=>3])->count('post_id');
        $data['shoke'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'status'=>4])->count('post_id');
        $data['sad'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'status'=>5])->count('post_id');
        $data['angry'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'status'=>6])->count('post_id');
           $data['status'] =$request->status;
            $user=User::where('id',$request->user_id)->first();
            $postData=Post::where('id',$request->post_id)->first();
            $usernotify=User::where(['id'=>$postData->user_id,'other_notification'=>0])->first();
            if(!empty($usernotify)){
            $notificationData=['title'=>'Reaction on '. $postData->tittle.' post.','description'=>$user->name.' reacted on your post','user_name'=>$user->name,'user_email'=>'','user_id'=>$postData->user_id,'post_id'=>$request->post_id,'type'=>'user'];
            Notification::create($notificationData); 
        /*$notifydata=['pn_gcm'=>['data'=>['text'=>'Hi','message'=>$user->name.' reacred on your post.','uuids'=>[$postData->user_id],'popular'=>$postData->tittle]]];
        $notifydataIos=['pn_apns'=>['data'=>['text'=>'Hi','message'=>$user->name.' reacred on your post.','uuids'=>[$postData->user_id],'popular'=>$postData->tittle]]];
        $result = $this->pubnub->publish()
              ->channel('notify')
              ->message($notifydata)
              ->sync();*/
              }
            
           $data['user'] =$user; 
       if($request->header()['lang'][0] == true){
          return json_encode(array('msg'=>'User reaction has submitted','status'=>true,'data'=>$data,'likeStatus'=>false));
        }else{
          return json_encode(array('msg'=>'Has agregado una Reacción','status'=>true,'data'=>$data,'likeStatus'=>false));
        }
        
      }
  
}
     
   
}
