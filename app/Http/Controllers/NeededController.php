<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Notifications\forgotPassword;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Model\passwordReset;
use App\User;
use App\Admin;
use App\Model\UserReaction;
use App\Model\Report;
use App\Model\Notification;
use App\Model\JoinLocation;

use Illuminate\Support\Facades\Validator;
use Session;
use App\Model\Category;
use App\Model\Feedback;
use App\Model\Favourite_geolocation;
use App\Model\Post;
use App\Model\ChatJoin;
use App\Model\SupportChat;
use App\Model\Chat;
use DB;
use App\Model\Help;
use App\Channel\GetPublichannel;
use PubNub\PubNub;
use PubNub\Enums\PNStatusCategory;
use PubNub\Callbacks\SubscribeCallback;
use PubNub\PNConfiguration;
use PubNub\Exceptions\PubNubException;
use Monolog\Handler\ErrorLogHandler;

class NeededController extends Controller
{

    protected $pubnub;
    protected $pnconf;


    public function __construct()
    {
      //$this->middleware('api_token');
        $this->pnconf = new PNConfiguration();
        $this->pnconf->setSubscribeKey(env('SubscribeKey'));
        $this->pnconf->setPublishKey(env('PublishKey'));
        $this->pnconf->setSecretKey(env('SecretKey'));
        $this->pnconf->setSecure(false);
        $this->pubnub = new PubNub($this->pnconf);
        //$this->pubnub->ssl => true;
    }
   public function storeNeeded(Request $request){
     
      $validator = Validator::make($request->all(),[
      'post' => 'required',
      'tittle' => 'required',
      'latitude' => 'required',
      'longitude' => 'required',
      'user_latitude' => 'nullable',
      'user_longitude' => 'nullable',
      'description' => 'required',
      //'post_type'  => 'required',
      'needed_price'  => 'required',
      //'image' => 'required|mimes:jpeg,jpg,png|max:2048',
      'image' => 'nullable',
      'user_image' => 'required',
      'user_name' => 'required',
     // 'user_last_name' => 'required',
      'user_id' => 'required',
      'address' => 'nullable',
      'address_second' => 'nullable',
      // 'post_category' => 'nullable',
      'type' => 'nullable',
      'location_id' => 'nullable',
      'location_name' => 'nullable',
       'file_type' => 'nullable',
      
      ]);
      if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{

          if(!empty($request->location_name)){
        $locationData=$this->location_search($request->latitude, $request->longitude, $request->location_name,$request->user_id);
         if(!empty($locationData)){
             $data['location_id']=$locationData['location_id'];
       }
       }
       $data=$request->all();
      //return json_encode(array('msg'=>'Your post has been created successfully.','data'=>$data,'status'=>true)); die;
       if(!empty($request->image)){ 
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
    request()->image->move(public_path('upload/image'), $imageName);
        }else{
          $imageName='';
        }
         $data['image'] = $imageName;
       $getId=Post::create($data);
        $data1=[ 'post_id' => $getId->id,
                'join_id' => $request->user_id,
                'uuid' => $request->user_id,
               'channel' => $getId->id,
         ];
         ChatJoin::create($data1);
         Chat::create(['channel'=>$getId->id,'uuid'=>$request->user_id,'client_id'=>$request->user_id,'post_id'=>$getId->id,'type'=>'public','join_status'=>1,'status'=>0]);
        $getId1=Post::where('id',$request->user_id)->get()->toArray();
         $userData = User::select(['*', DB::RAW('CAST( 6371 * acos( cos( radians(' . $request->latitude . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $request->longitude . ') ) + sin( radians(' . $request->latitude .') ) * sin( radians(latitude) ) )  AS DECIMAL(10,2)) AS distance')])->where('status','!=',2)->having( 'distance', '<=', 10 )->get()->toArray();
 // print_r($userData); die;
         $ids=[];
        foreach($userData as $value){
       // $notifydata=['pn_gcm'=>['data'=>['text'=>$request->tittle,'message'=>$request->description]]];
        $notifydataIos=['pn_apns'=>['data'=>['text'=>$request->tittle,'message'=>$request->description]]];
       
            if($value['id'] != $request->user_id){
            array_push($ids,$value['id']); 
            }
              $notifydata=['title'=>$request->tittle,'description'=>$request->description,'user_name'=>$value['name'],'user_email'=>'','user_id'=>$value['id'],'post_id'=>$getId->id,'type'=>'user'];
            Notification::create($notifydata);  
          }
          $notifydata1=['pn_gcm'=>['data'=>['text'=>$request->tittle,'message'=>$request->description,'uuids'=>$ids,'popular'=>$request->tittle]]];

           $result = $this->pubnub->publish()
             ->channel('notify')
               ->message($notifydata1)
              ->sync();
            
        
       
        // $url= str_replace("/api/auth/storeNeeded","/share_page",\Request::fullUrl());
         
           $needyData=[ 'url'=>  asset('/share_page/').$getId->id ];
         
          return json_encode(array('msg'=>'Your post has been created successfully.','data'=>$needyData,'status'=>true));
          
      }
    }

     public function needed(Request $request)
    {
      // User::where_id()->get()->toArray();
      $validator = Validator::make($request->all(),[
      'latitude' => 'required',
      'longitude' => 'required',
      'user_id' => 'required',
      //'user_id' => 'required',
      ]);
      if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{
        $latitude = $request->latitude;
        $longitude = $request->longitude;
       if($request->page  <= 1){
          $page = 0;
          }else {
          $page = $request->page - 1;
        }
        $limit = 5;
        $offset = ($page * $limit);
      //  $catecory = Post::where('id',$request->category_id)->get()->pluck('name')->toArray();
        $searchTerm = $request->search;
        
            $Business = Post::select(['*', DB::RAW('CAST( 6371 * acos( cos( radians(' . $latitude . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude .') ) * sin( radians(latitude) ) )  AS DECIMAL(10,2)) AS distance')])->where('post','Needed')->where('status',0)->orderBy('created_at','Desc')
           
            ->where(function($query) use ($searchTerm) {
             // $query->where('user_name', 'LIKE', '%' . $searchTerm . '%')
              $query->orWhere('tittle', 'LIKE', '%' . $searchTerm . '%')
              ->orWhere('post', 'LIKE', '%' . $searchTerm . '%');
             
            })->with('userData')->orderBy('created_at','Desc')
            //->orderByRaw('ISNULL(distance), distance ASC')
           // ->groupBy("id")
            ->offset($offset)
            ->take($limit)
            ->get()
            ->toArray();
          
         
        
        if(!empty($Business)){ 
           $img=asset('/upload/image/');
           $url= asset('/share_page/');
          
           $array=[];
          foreach($Business as $value){
            $data = $value;
            //$data['like'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>1])->count('post_id');
        $data['smile'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>1])->count('post_id');
        $data['heart'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>2])->count('post_id');
        $data['Laugh'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>3])->count('post_id');
        $data['shoke'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>4])->count('post_id');
        $data['sad'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>5])->count('post_id');
        $data['angry'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>6])->count('post_id');
        $data['dislike'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>0])->count('post_id');
         
         
         $data['totalLike'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type']])->where('status','!=',0)->count('post_id');
         $data['totalDislike'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type']])->where('status',0)->count('post_id');
          $data['userLike'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'user_id'=>$value['user_id']])->where('status','!=',0)->count('post_id');
          if($data['totalLike'] == 0){
              $data['likeStatus'] = false;

            }else{
              $data['likeStatus'] = false;
            }
              $data['url']= $url.'/'.$value['id'] ;
             if(!empty($data['image'])){ 
            $data['image'] = $value['image'];
          }
               array_push($array,$data);
          }
          // return json_encode(array('msg'=>'data found.','data'=>$array,'status'=>true));
          if($request->header()['lang'][0] == true){
           return json_encode(array('msg'=>'data found.','data'=>$array,'status'=>true));
        }else{
         return json_encode(array('msg'=>'Información válida','data'=>$array,'status'=>true));
        }
        
          }else{
            // return json_encode(array('msg'=>'data found.','data'=>$array,'status'=>true));
            if($request->header()['lang'][0] == true){
            return json_encode(array('msg'=>'data not available','status'=>false));
        }else{
         return json_encode(array('msg'=>'Información no válida','status'=>false));
        }
         
        }
       } 
        //return response()->json($request->user());
    }

     public function updateNeeded(Request $request){
      $validator = Validator::make($request->all(),[
        'id' => 'required',
     // 'post' => 'nullable',
      'tittle' => 'nullable',
      'latitude' => 'nullable',
      'longitude' => 'nullable',
      'description' => 'nullable',
      'needed_price'  => 'nullable',
      //'image' => 'required|mimes:jpeg,jpg,png',
       'image' => 'nullable',
     // 'user_image' => 'required',
     // 'user_name' => 'required',
     // 'user_last_name' => 'required',
     // 'user_id' => 'required',
     // 'post_category' => 'nullable',
     ]);
      if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{
        if(!empty($request->image)){
          $validator = Validator::make($request->all(),[
          'image' => 'mimes:jpeg,jpg,png',
          ]);
          if($validator->fails()){
            return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
            }else{
            $host = $request->getHttpHost();
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('upload/image'), $imageName);
           
          }
        }
        $data = Post::where('id', $request->id)->get()->toArray();
        if(!empty($data)){
          $User = Post::where('id', $request->id)->update([
          //'post' => ($request->post)? $request->post: $data[0]['post'],
          'tittle' => ($request->tittle)? $request->tittle: $data[0]['tittle'],
           'latitude' => ($request->latitude)? $request->latitude: $data[0]['latitude'],
            'longitude' => ($request->longitude)? $request->longitude: $data[0]['longitude'],
          'description' => ($request->description)? $request->description: $data[0]['description'],
            'needed_price' => ($request->needed_price)? $request->needed_price: $data[0]['needed_price'],
         
          'image' => ($request->image)? $imageName: $data[0]['image'],
         
          ]);
          $updatedata = Post::where('id', $request->id)->get()->toArray();

        
          if(!empty($updatedata[0]['image'])) { 
             $image1 = explode("://",$updatedata[0]['image']);
            
             if($image1[0] == 'https'|| $image1[0] == 'http') { 
                 $updatedata[0]['image'] = $updatedata[0]['image'];
             }else{
               $updatedata[0]['image'] = asset('/upload/image/').$updatedata[0]['image'];
             }
              
         
         }else{
           $updatedata[0]['image'] = null;
         }
          return json_encode(array('msg'=>'Your post has been updated successfully.','data'=>$updatedata[0],'status'=>true));
          }else{
          return json_encode(array('msg'=>'Post  not exist .','status'=>false));
        }
      }
    }

        public function deleteNeeded(Request $request)
    {
       $request->validate([
             
             // 'user_id' => 'required',
              'id' => 'required',
             // 'post_type' =>'required',
            
              
      ]);

       $data=Post::where('id',$request->id)->delete();
  
      if($data){
              if($request->header()['lang'][0] == true){
          return json_encode(array('msg'=>'Post has been deleted','status'=>true));
        }else{
        return json_encode(array('msg'=>'La publicación se ha eliminado','status'=>true));
        }
        
      }else{
        if($request->header()['lang'][0] == true){
           return json_encode(array('msg'=>'Something went wrong','status'=>false));
        }else{
         return json_encode(array('msg'=>'Ocurrió un error','status'=>false));
        }
       
      }
       // return json_encode(array('msg'=>'Post has deleted successfully!','status'=>true));
     
    }


     function location_search($lat, $lang, $location_name,$user_id) {
        try{ 
          $location=JoinLocation::Where(['latitude'=>$lat,'longitude'=>$lang,'location_name'=>$location_name,'location_type'=>0,'post_id'=>0])->first();
          $dis=JoinLocation::Where(['latitude'=>$lat,'longitude'=>$lang,'location_name'=>$location_name,'location_type'=>1])->first();

          if(!empty($location)){ 
             return $data1=['location_id'=>$location->id,'location_name'=>$location->location_name,'post_id'=>$location->post_id,'location_type'=>$location->location_type];
          }else{
              if(empty($dis)){
                 $data=JoinLocation::create(['latitude'=>$lat,'longitude'=>$lang,'location_name'=>$location_name,'user_id'=>$user_id]);
              return $data1=['location_id'=>$data->id,'location_name'=>$data->location_name,'post_id'=>$location-> post_id,'location_type'=>$location->location_type];
              }
              // return json_encode(array('msg'=>'Data found','location_id'=>$data->id,'location_name'=>$data->location_name,'status'=>true));
          }

        }catch(\Exception $e){
           return json_encode(array('msg'=>$e->getMessage(),'status'=>false));
        }
    }
   
}
