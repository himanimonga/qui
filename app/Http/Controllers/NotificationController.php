<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Notifications\forgotPassword;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Model\passwordReset;
use App\User;
use App\Admin;
use App\Model\UserReaction;
use App\Model\Report;
use App\Model\Notification;
use App\Model\JoinLocation;

use Illuminate\Support\Facades\Validator;
use Session;
use App\Model\Category;
use App\Model\Feedback;
use App\Model\Favourite_geolocation;
use App\Model\Post;
use App\Model\ChatJoin;
use App\Model\SupportChat;
use App\Model\Chat;
use DB;
use App\Model\Help;
use App\Channel\GetPublichannel;
use PubNub\PubNub;
use PubNub\Enums\PNStatusCategory;
use PubNub\Callbacks\SubscribeCallback;
use PubNub\PNConfiguration;
use PubNub\Exceptions\PubNubException;
use Monolog\Handler\ErrorLogHandler;

class NotificationController extends Controller
{

    protected $pubnub;
    protected $pnconf;


    public function __construct()
    {
      //$this->middleware('api_token');
        $this->pnconf = new PNConfiguration();
        $this->pnconf->setSubscribeKey(env('SubscribeKey'));
        $this->pnconf->setPublishKey(env('PublishKey'));
        $this->pnconf->setSecretKey(env('SecretKey'));
        $this->pnconf->setSecure(false);
        $this->pubnub = new PubNub($this->pnconf);
        //$this->pubnub->ssl => true;
    }

         public function notificationStatus(Request $request)
    {
       $request->validate([
              'user_id' => 'required',
              'type' => 'required',
              'posttype' => 'required',
        ]);

       if($request->posttype == 'public'){
           if($request->type == 0){
         $data1 = Chat::where('uuid',$request->user_id)->where('type','public')->update(['status'=>2]);
        User::where('id',$request->user_id)->update(['status'=>2]);
         if($request->header()['lang'][0] == true){
         return json_encode(array('msg'=>'Notifications Off','status'=>true));
        }else{
          return json_encode(array('msg'=>'Notificaciones Apagadas','status'=>true));
        }
        }else{
         $data1 = Chat::where('uuid',$request->user_id)->where('type','public')->update(['status'=>0]);
        User::where('id',$request->user_id)->update(['status'=>0]);
        if($request->header()['lang'][0] == true){
        return json_encode(array('msg'=>'Notifications On','status'=>true));
        }else{
        return json_encode(array('msg'=>'Notificaciones Encendidas','status'=>true));
        }
      }


       }elseif($request->posttype == 'private'){
            if($request->type == 0){
         $data1 = Chat::where('uuid',$request->user_id)->where('type','private')->update(['status'=>2]);
        User::where('id',$request->user_id)->update(['private_notification'=>2]);
         if($request->header()['lang'][0] == true){
         return json_encode(array('msg'=>'Notifications Off','status'=>true));
        }else{
          return json_encode(array('msg'=>'Notificaciones Apagadas','status'=>true));
        }
        }else{
         $data1 = Chat::where('uuid',$request->user_id)->where('type','private')->update(['status'=>0]);
        User::where('id',$request->user_id)->update(['private_notification'=>0]);
        if($request->header()['lang'][0] == true){
        return json_encode(array('msg'=>'Notifications On','status'=>true));
        }else{
        return json_encode(array('msg'=>'Notificaciones Encendidas','status'=>true));
        }
      }


       }else{
             if($request->type == 0){
         $data1 = Chat::where('uuid',$request->user_id)->update(['status'=>2]);
        User::where('id',$request->user_id)->update(['other_notification'=>2]);
         if($request->header()['lang'][0] == true){
         return json_encode(array('msg'=>'Notifications Off','status'=>true));
        }else{
          return json_encode(array('msg'=>'Notificaciones Apagadas','status'=>true));
        }
        }else{
         $data1 = Chat::where('uuid',$request->user_id)->update(['status'=>0]);
        User::where('id',$request->user_id)->update(['other_notification'=>0]);
        if($request->header()['lang'][0] == true){
        return json_encode(array('msg'=>'Notifications On','status'=>true));
        }else{
        return json_encode(array('msg'=>'Notificaciones Encendidas','status'=>true));
        }
      }


       }

      
    }

  public function get_notificationStatus(Request $request)
    {
       $request->validate([
              'user_id' => 'required',
              
        ]);
       
        $data1 = User::where('id',$request->user_id)->first();
        if(!empty($data1)){ 
         if($data1->status==2){
          $status=true;
         }else{
          $status=false;
         }
          if($data1->private_notification==2){
          $statusprivate=true;
         }else{
          $statusprivate=false;
         }

          if($data1->other_notification==2){
          $other_notification=true;
         }else{
          $other_notification=false;
         }

        return json_encode(array('msg'=>'Notification Off','public'=>$status,'private'=>$statusprivate,'other'=>$other_notification,'status'=>true));
        }else{
         return json_encode(array('msg'=>'Notification On','Data'=>$data1[0],'status'=>false));
        }
    }

     public function notificationCount(Request $request){
      $validator = Validator::make($request->all(),[
      'user_id' => 'required',
      ]);
      if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{
      $noticount= Notification::where(['user_id'=>$request->user_id,'status'=>0])->count();
      return json_encode(array('msg'=>'Data found','data'=>$noticount,'status'=>true));
    }
    }

     public function notification(Request $request)
    {
      $validator = Validator::make($request->all(),[
      'user_id' => 'required',
      ]);
      if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{
          if($request->page  <= 1){
          $page = 0;
          }else {
          $page = $request->page - 1;
        }
        $limit = 10;
        $offset = ($page * $limit);
        $searchTerm = $request->search;
        try{ 
          Notification::where('user_id',$request->user_id)->update(['status'=>1]);
         $data = DB::table('notifications')
          ->selectRaw("notifications.*,posts.latitude as latitude,posts.longitude as longitude")
          ->join('posts', 'notifications.post_id', '=', 'posts.id')
          ->where('notifications.user_id',$request->user_id)
          ->where('notifications.status','!=',2)
           ->where(function($query) use ($searchTerm) {
              $query->Where('posts.user_name', 'LIKE', '%' . $searchTerm . '%')
              ->orWhere('posts.tittle', 'LIKE', '%' . $searchTerm . '%')
              ->orWhere('posts.description', 'LIKE', '%' . $searchTerm . '%');
            })->orderBy('notifications.created_at', 'desc')
           ->offset($offset)
            ->take($limit)
            ->get()
            ->toArray();
            if(!empty($data)){
             
              return json_encode(array('msg'=>'Data found','data'=>$data,'status'=>true));
           
            }else{
              return json_encode(array('msg'=>'Data not found','status'=>false));
            }
           } catch (\Exception $e) {

            return json_encode(array('msg'=>$e->getMessage(),'status'=>false));
            //return $e->getMessage();
        }
      
       }

    
        
    }

     public function notifydelete(Request $request){ 
      $request->validate([
              'channel' => 'required',
             ]);
          try{ 
              $id= Notification::where('id',$request->channel)->delete();
            if($id == 1){ 
                   return json_encode(array('msg'=>'Notification has deleted!','status'=>true));
                 }else{
                   return json_encode(array('msg'=>'Someting went wrong','status'=>false));
                 }
           }catch(\PubNub\Exceptions\PubNubServerException $e) {
        return json_encode(array('msg'=>'Notification not delete','timetoken'=>$e->getMessage(),'status'=>false));
         // echo "Error happened while publishing: " . $e->getMessage();
      }       
    }
  
   
}
