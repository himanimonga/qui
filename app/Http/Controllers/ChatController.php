<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Notifications\forgotPassword;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Model\passwordReset;
use App\User;
use App\Admin;
use App\Model\UserReaction;
use App\Model\Report;
use App\Model\Notification;
use App\Model\JoinLocation;

use Illuminate\Support\Facades\Validator;
use Session;
use App\Model\Category;
use App\Model\Feedback;
use App\Model\Favourite_geolocation;
use App\Model\Post;
use App\Model\ChatJoin;
use App\Model\SupportChat;
use App\Model\Chat;
use DB;
use App\Model\Help;
use App\Channel\GetPublichannel;
use PubNub\PubNub;
use PubNub\Enums\PNStatusCategory;
use PubNub\Callbacks\SubscribeCallback;
use PubNub\PNConfiguration;
use PubNub\Exceptions\PubNubException;
use Monolog\Handler\ErrorLogHandler;

class ChatController extends Controller
{

    protected $pubnub;
    protected $pnconf;


    public function __construct()
    {
      //$this->middleware('api_token');
        $this->pnconf = new PNConfiguration();
        $this->pnconf->setSubscribeKey(env('SubscribeKey'));
        $this->pnconf->setPublishKey(env('PublishKey'));
        $this->pnconf->setSecretKey(env('SecretKey'));
        $this->pnconf->setSecure(false);
        $this->pubnub = new PubNub($this->pnconf);
        //$this->pubnub->ssl => true;
    }

     public function personalChat(Request $request)
    {
     $validator = Validator::make($request->all(),[
      'uuid' => 'required',
      'channel' => 'required',
      'client_id' => 'required',
      'message' => 'required',
      'post_id' => 'required',
      //'name' => 'nullable',
      'timetoken' => 'required',
      'type' => 'required',
      'chat_type' => 'nullable',
      'client_id'=> 'required',
      'usertype' => 'nullable',
      'mute' => 'required',
      'notification_status' => 'required',

      ]);
      if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{
            try{
               
          //return json_encode(array('msg'=>'dfgdf','status'=>true)); die;
          $user = User::find($request->uuid);
          $post = Post::find($request->post_id);
          $PersonalChat = Chat::where(['uuid'=>$request->uuid,'client_id'=>$request->client_id,'type'=>$request->type])->get()->toArray();
     if(!empty($post)){
       
      
         $postname = $post->tittle;
         $location_id = $post->location_id;
       
     }else{
      $postname = $user['name'];

     } 

   if(!empty($PersonalChat)){
      $token= rand(); 
   }else{
    $token= rand(); 

    }
        
          $data=[
              'channel'=> $request->channel,
              'post_name'=> $postname,
              'uuid'=> $request->uuid,
              'message'=> $request->message,
              'client_id'=> $request->client_id,
              'post_id'=>$request->post_id,
              'type'=>$request->type,
              'chat_type'=>$request->chat_type,
              'start_timetoken'=>$request->timetoken,
              'usertype'=>$request->usertype,
              'muteUnmute'=>$request->mute,
              'status'=>$request->notification_status,
            ];

              $msgdata=[
                // 'channel'=> $request->channel,
                   'uuid'=> $request->uuid,
                    'message'=> $request->message,
                    'created_at'=> $request->created_at,
                    'chat_token'=> $token,
                  ];

        //test
      $notifica=Chat::where('channel',$request->channel)->where('muteUnmute',0)->where('status','!=',2)->where('uuid','!=',$request->uuid)->groupBy('uuid')->pluck('uuid')->toArray();
      //print_r($notifica); die;
      if(!empty($notifica)){
      $notifyarray=implode(",",$notifica);
       
      $notifydataIos=['pn_apns'=>['data'=>['popular'=>$request->name,'message'=>$request->message]]];
      $notifydata=['pn_gcm'=>['data'=>['popular'=>$request->name,'message'=>$request->message,'uuids'=>$notifica]]];
       
        $result = $this->pubnub->publish()
              ->channel('notify')
              ->message($notifydata)
              ->sync();
           
      Chat::create($data);

       
          
      }else{
       //for private 
    //$client=Chat::where('channel',$request->channel)->where('muteUnmute',0)->where('status','!=',2)->where('client_id',$request->client_id)->groupBy('client_id')->pluck('client_id')->toArray();
    // print_r($notifica); die;
     if(!empty($notifica)){
        $cid= User::find($request->client_id);
     $notifydataIos=['pn_apns'=>['data'=>['popular'=>$cid->name,'message'=>$request->message]]];
      $notifydata=['pn_gcm'=>['data'=>['popular'=>$cid->name,'message'=>$request->message,'uuids'=>[$request->client_id]]]];
       
        $result = $this->pubnub->publish()
              ->channel('notify')
              ->message($notifydata)
              ->sync();
     }
      Chat::create($data);
          
      }
      if($request->header()['lang'][0] == true){
          return json_encode(array('msg'=>'Message sent!','status'=>true));
        }else{
           return json_encode(array('msg'=>'Mensaje enviado!','status'=>true));
        }
      }catch(\PubNub\Exceptions\PubNubServerException $e) {
         if($request->header()['lang'][0] == true){
          return json_encode(array('msg'=>'Message send successfully!','timetoken'=>$e->getMessage(),'status'=>false));
        }else{
           return json_encode(array('msg'=>'Mensaje enviado!','timetoken'=>$e->getMessage(),'status'=>false));
        }
       
        
      }
           
  }
                  
 }


    public function getChat(Request $request)
    {
     $validator = Validator::make($request->all(),[
     // 'uuid' => 'required',
      'channel' => 'required',
     // 'message' => 'required',
      'user_id' => 'required',
      // 'chat_token' => 'required',
      ]);
      if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{
     // $this->pnconf->setUuid($request->uuid);

          try {

      $result = $this->pubnub->history()
              //->channel($request->channel)
             ->sync();
     //print_r($result); die;
          $user = Chat::where(['uuid'=>$request->user_id])->get()->toArray();
      
           $dataArray=[];
           $i=1;
        foreach($result->getMessages() as   $value){
          $msg=$value->getEntry();
          //print_r($msg['uuid']); die;
          // if($msg['chat_token'] == $request->chat_token){ 
             $data= User::where('id',$msg['uuid'])->get()->toArray();
             $msg1['_id'] = $i; 
            $msg1['chat_token'] = $msg['chat_token'];
             $msg1['text'] = $msg['message'];
             $msg1['createdAt'] = $msg['created_at'];
             $msg1['user'] =[
              '_id' => $data[0]['id'], 
               'name' => $data[0]['name'], 
               'avatar'=> $data[0]['image'],
             ];
              

                array_push($dataArray,$msg1);
                  $i++;
          // }
            
             
       
        
        }
           $dataReverse=  array_reverse($dataArray);
         // echo "Publish worked! Timetoken: " . $result->getTimetoken();
           return json_encode(array('msg'=>'Data found!','data'=>$dataReverse,'status'=>true));
        
      }
      catch(\PubNub\Exceptions\PubNubServerException $e) {
        return json_encode(array('msg'=>'Data not found!','status'=>false));
         // echo "Error happened while publishing: " . $e->getMessage();
      }
  }
                  
  }

    public function support_chat(Request $request)
    {
       $request->validate([
              'user_id' => 'required',
              'user_name' => 'required',
              'user_email' => 'required',
              'channel' => 'required',
              'update' => 'required',
              //'type' => 'required',

                      
      ]);
       $data=[ 'client_id' => $request->user_id,
              //'client_name' => $request->user_name,
              'client_email' => $request->user_email,
              'channel' => $request->channel,
              'msg' => $request->update,
              'type' => 'user',
];
     $data1 = SupportChat::create($data);
      if($data1){
         if($request->header()['lang'][0] == true){
         return json_encode(array('msg'=>'Data inserted!','status'=>true));
        }else{
          return json_encode(array('msg'=>'Información registrada con éxito','status'=>true));
        }
        
      }else{
         if($request->header()['lang'][0] == true){
        return json_encode(array('msg'=>'Data not inserted.','status'=>false));
        }else{
          return json_encode(array('msg'=>'Información sin registrar','status'=>false));
        }
        
      }
 
      
    
  }

      public function unreadmsg(Request $request)
    {
      //return json_encode(array('msg'=>'1','status'=>true)); die;
       $request->validate([
              'user_id' => 'required',
              'channel' => 'nullable',
              'post_id'  => 'required',
              'type'    => 'nullable',
        ]);
       
         $unread = ChatJoin::where(['channel'=>$request->channel,'join_id'=>$request->user_id])->pluck('join_id')->toArray();
         


         $post = Post::find($request->post_id);
         if(!empty($request->type) && $request->type == 'group'){
             JoinLocation::where(['id'=>$request->post_id,'user_id'=>$request->user_id,'status'=>0])->update(['unreadmsg_count'=>0]);
           }
    /* if(!empty($post)){
         $location_id = $post->location_id;
         if(!empty($post->location_id)){
         $joinData=JoinLocation::where('post_id',$location_id)->where('user_id',$request->user_id)->first();
         if(!empty($joinData->unreadmsg_count)){
              $msgCount = $joinData->unreadmsg_count - 1;
                 JoinLocation::where('id',$joinData->id)->where('user_id',$request->user_id)->update(['unreadmsg_count'=>$msgCount]); 
            }else{
              JoinLocation::where('id',$joinData->id)->where('user_id',$request->user_id)->update(['unreadmsg_count'=>0]); 
            }
          }
        }*/
       if(!empty($unread)){
       $unreadmsg = Chat::where(['channel'=>$request->channel,'join_status'=>0])->get()->toArray();
     //print_R($unreadmsg); die;
       $update=[];
      foreach($unreadmsg as $value){
        if(!empty($value['unreadmsg']) ){
         $array= (explode(",",$value['unreadmsg']) );
          //print_R($array); die;
       if (in_array($request->user_id, $array))
        {
        
        }
      else
        {
        $data = Chat::where(['id'=>$value['id'],'join_status'=>0])->pluck('unreadmsg')->first();
       
        if(!empty($data)){
        $ok=Chat::where(['id'=>$value['id']])->update(['unreadmsg'=> $data.$request->user_id.',']);
        array_push($update,$ok);
         
        }else{
          //Chat::where(['id'=>$value['id']])->update(['unreadmsg'=>$request->user_id.',']);
        }
         
        }
        //return json_encode(array('msg'=>'Data inserted!','status'=>true));
      }else{
        Chat::where(['id'=>$value['id']])->update(['unreadmsg'=>$request->user_id.',']);
        
      }
       }

       if(!empty($post)){

               /* $locationjoinData=JoinLocation::where(['latitude'=>$post->latitude,'longitude'=>$post->longitude,'location_type'=>0])->where('user_id',$request->user_id)->first();
         if(!empty($locationjoinData->unreadmsg_count)){
          if($locationjoinData->unreadmsg_count >= count($update)){
              $msgCount = $locationjoinData->unreadmsg_count - count($update);
               JoinLocation::where('id',$locationjoinData->id)->where(['latitude'=>$post->latitude,'longitude'=>$post->longitude,'location_type'=>0])->where('user_id',$request->user_id)->update(['unreadmsg_count'=>$msgCount]); 
            }else{
             JoinLocation::where('id',$locationjoinData->id)->where(['latitude'=>$post->latitude,'longitude'=>$post->longitude,'location_type'=>0])->where('user_id',$request->user_id)->update(['unreadmsg_count'=>0]); 
            }
              //print_r($msgCount); die;
                
            }*/
         $location_id = $post->location_id;
         if(!empty($post->location_id)){
        // $joinData=JoinLocation::where('post_id',$location_id)->where('user_id',$request->user_id)->first();
        //  JoinLocation::where('id',$joinData->id)->where('user_id',$request->user_id)->update(['unreadmsg_count'=>0]); 
         /*if(!empty($joinData->unreadmsg_count)){
          if($joinData->unreadmsg_count >= count($update)){
              $msgCount = $joinData->unreadmsg_count - count($update);
               JoinLocation::where('id',$joinData->id)->where('user_id',$request->user_id)->update(['unreadmsg_count'=>$msgCount]); 
            }else{
              $msgCount = 0;
            }
              //print_r($msgCount); die;
                
            }*/
            }else{
              JoinLocation::where('id',$joinData->id)->where('user_id',$request->user_id)->update(['unreadmsg_count'=>0]); 
            }
          }
        
       
       if($request->header()['lang'][0] == true){
         return json_encode(array('msg'=>'Data inserted!','status'=>true));
        }else{
        return json_encode(array('msg'=>'Información registrada con éxito','status'=>true));
        }
       
      }
       
    else{
       return json_encode(array('msg'=>'User not join!','status'=>false));
     }
    }

     public function onlinecount(Request $request)
  {
    try{ 
         $request->validate([
             // 'uuid' => 'nullable',
              'channel' => 'required',
              'user_id' => 'required',
              'post_id' => 'required',
              'user_status' => 'required',
        ]);

       
       $data = ChatJoin::where(['channel'=>$request->channel,'join_id'=>$request->user_id,'post_id'=>$request->post_id,'status'=>0])->get()->toArray();
      // print_r($data); die;
       if(!empty($data)){
        ChatJoin::where(['channel'=>$request->channel,'join_id'=>$request->user_id,'post_id'=>$request->post_id,'status'=>0])->update([ 'user_status' => $request->user_status]);
         $userStatus = ChatJoin::where(['channel'=>$request->channel,'post_id'=>$request->post_id,'user_status'=>true,'status'=>0])->get()->count();
          $userCount = ChatJoin::where(['channel'=>$request->channel,'post_id'=>$request->post_id,'status'=>0])->get()->count();
     return json_encode(array('msg'=>'Joined!','status'=>true,'userStatus'=>$userStatus,'userCount'=>$userCount));
     }else{
       return json_encode(array('msg'=>'Data not found!','status'=>false,'checkJoin'=>false));
     }
      } catch (\Exception $e) {

       return json_encode(array('msg'=>$e->getMessage(),'status'=>false));
            //return $e->getMessage();
     }
      
  }

    public function chatJoin(Request $request)
    {
       
       $request->validate([
              'uuid' => 'nullable',
              'channel' => 'required',
              'join_id' => 'required',
              'post_id' => 'required',
              'checkJoin' => 'nullable',
        ]);
       if($request->checkJoin == true){
          $data = ChatJoin::where(['channel'=>$request->channel,'join_id'=>$request->join_id,'post_id'=>$request->post_id])->get()->toArray();
      
       if(!empty($data)){
      return json_encode(array('msg'=>'Joined!','status'=>true,'checkJoin'=>true));
     }else{
       return json_encode(array('msg'=>'Data not found!','status'=>false,'checkJoin'=>false));
     }

       }else{
            
        $data = ChatJoin::where(['channel'=>$request->channel,'join_id'=>$request->join_id,'post_id'=>$request->post_id])->get()->toArray();
      
       if(!empty($data)){
      return json_encode(array('msg'=>'Joined!','status'=>true));
     }else{
       $data1=[ 'post_id' => $request->post_id,
                'join_id' => $request->join_id,
                'uuid' => $request->uuid,
               'channel' => $request->channel,
         ];
         ChatJoin::create($data1);
         $postid= Post::find($request->post_id);
         if(!empty($postid)){
           // return json_encode(array('msg'=>'not empty','status'=>true)); die;
            $userData=User::find($request->uuid);
          Chat::create(['channel'=>$request->channel,'uuid'=>$request->join_id,'client_id'=>$request->uuid,'post_id'=>$request->post_id,'post_name'=>$postid->tittle,'type'=>$postid->post_type == 0 ? 'public' : 'private','join_status'=>1,'status'=>0]);
       
        }else{
            //return json_encode(array('msg'=>'empty','status'=>true)); die; 
          $userData=User::find($request->uuid);
        // Chat::create(['channel'=>$request->channel,'uuid'=>$request->join_id,'post_id'=>0,'client_id'=>$request->uuid,'post_name'=>$userData->name,'name'=>$userData->name,'type'=>'private','usertype'=>'usertypeChat','user_username'=>$userData->username,'join_status'=>1,'status'=>0]);
      Chat::create(['channel'=>$request->channel,'uuid'=>$request->join_id,'post_id'=>0,'client_id'=>$request->uuid,'post_name'=>$userData->name,'type'=>'private','usertype'=>'usertypeChat','join_status'=>1,'status'=>0]);
  
     // return json_encode(array('msg'=>'empty','status'=>true)); die;
        }
       return json_encode(array('msg'=>'Joined!','status'=>true));
     }
   }
    }

    public function chatleave(Request $request)
    {
       $request->validate([
              'uuid' => 'nullable',
              'channel' => 'required',
              'join_id' => 'required',
              'post_id' => 'required',
        ]);
        $data = ChatJoin::where(['channel'=>$request->channel,'join_id'=>$request->join_id,'post_id'=>$request->post_id])->get()->toArray();
      
       if(!empty($data)){
         ChatJoin::where('id',$data[0]['id'])->update(['status'=>1]);
      return json_encode(array('msg'=>'User leave!','status'=>true));
     }else{
       
       return json_encode(array('msg'=>'User id not exist!','status'=>false));
     }
    }

    


  
   
}
