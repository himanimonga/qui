<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Notifications\forgotPassword;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Model\passwordReset;
use App\User;
use App\Admin;
use App\Model\UserReaction;
use App\Model\Report;
use App\Model\Notification;
use App\Model\JoinLocation;

use Illuminate\Support\Facades\Validator;
use Session;
use App\Model\Category;
use App\Model\Feedback;
use App\Model\Favourite_geolocation;
use App\Model\Post;
use App\Model\ChatJoin;
use App\Model\SupportChat;
use App\Model\Chat;
use DB;
use App\Model\Help;
use App\Channel\GetPublichannel;
use PubNub\PubNub;
use PubNub\Enums\PNStatusCategory;
use PubNub\Callbacks\SubscribeCallback;
use PubNub\PNConfiguration;
use PubNub\Exceptions\PubNubException;
use Monolog\Handler\ErrorLogHandler;

class ShareController extends Controller
{

    protected $pubnub;
    protected $pnconf;


    public function __construct()
    {
      //$this->middleware('api_token');
        $this->pnconf = new PNConfiguration();
        $this->pnconf->setSubscribeKey(env('SubscribeKey'));
        $this->pnconf->setPublishKey(env('PublishKey'));
        $this->pnconf->setSecretKey(env('SecretKey'));
        $this->pnconf->setSecure(false);
        $this->pubnub = new PubNub($this->pnconf);
        //$this->pubnub->ssl => true;
    }
    
     public function share_page(Request $request)
    {
      $validator = Validator::make($request->all(),[
      'id' => 'required',
      ]);
      if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{
         $url= str_replace("/api/auth/","/",\Request::fullUrl());
         
           $data=  $url.'/'.$request->id; 
          
      return json_encode(array('msg'=>'Data found','data'=>$data,'status'=>true));
       }
        
    }
    
     public function post_share(Request $request)
    {
        try{
      $validator = Validator::make($request->all(),[
      'message'   => 'required', //form post id
      'user_id'   => 'required',
      'channel'   => 'required', //send post name
      'client_id' => 'required', // post creator id
      'post_id'   => 'required', // send post_id
     
      ]);
      if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{
         $data=['message'=>$request->message,'file_type'=>'post','channel'=>$request->channel,'uuid'=>$request->user_id,'post_id'=>$request->post_id,'client_id'=>$request->client_id];
         Chat::create($data);
         if($request->header()['lang'][0] == true){
          return json_encode(array('msg'=>'Message send successfully!','status'=>false));
        }else{
        	 return json_encode(array('msg'=>'Mensaje enviado!','status'=>false));
        }
          
      //return json_encode(array('msg'=>'Data found','data'=>$data,'status'=>true));
       }
        }catch(\PubNub\Exceptions\PubNubServerException $e) {
      	 
       
          return json_encode(array('msg'=>'Mensaje enviado!','timetoken'=>$e->getMessage(),'status'=>false));
      }
        
    }

     
   
}
