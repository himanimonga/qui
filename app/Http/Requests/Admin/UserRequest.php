<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => "required|string",
            'email' => "required|email|unique:users,email,{$this->id}",
            'username' => "required|unique:users,username,{$this->id}",
            'location'=>"required",
            'latitude'=>"nullable",
            'longitude'=>"nullable",
            'email_verified_at'=>"nullable",
        ];
//        dd($this->id);
        if($this->id){
            $rules['password'] = 'nullable|confirmed|min:6';
        }else{
            $rules['password'] = 'required|confirmed|min:6';
        }
        return $rules;
    }
}
