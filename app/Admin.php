<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
     protected $table = 'admins';
	 protected $guard = 'admin';
	 
	  protected $fillable = [
            'name', 'email', 'password','task'
        ];
		
		protected $hidden = [
            'password', 'remember_token',
        ];
}
