<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Help extends Model
{
	 use Sortable;

	public $sortable = ['lat','long','channel'];

    protected $fillable = [

       'lat','long','channel','user_id','post_id','status','created_at','updated_at',
    ];
}
