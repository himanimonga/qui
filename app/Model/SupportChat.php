<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class SupportChat extends Model
{
	 use Sortable;

	public $sortable = ['client_name','client_email'];

    protected $fillable = [

       'client_id','msg','channel','client_name','client_email','status','created_at','updated_at','type'
    ];

    protected $appends = ['client_name'];
	
	public function getclientNameAttribute()
		{
		   $data = $this->belongsTo('App\Model\User','client_id')->first('name');
		   if(!empty($data)){
		   return $data['name'];
		}else{
          return null;
		}
		   
		}
}
