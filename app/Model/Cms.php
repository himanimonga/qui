<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Cms extends Model
{
	 use Sortable;
	 
	public $sortable = ['page_name','title']; 
	
    protected $fillable = [
		'page_name',
		'page_uses',
    	'title',
	    'meta_title',
	    'meta_description',
	    'slug',
	    'content',
	    'featureImage',
		'status'
  	];
	
	public function createSlug($title){
		$title =  strtolower($title);
		return preg_replace('/\s+/', '-', $title);
	}
	
    public $timestamps = false;
}
