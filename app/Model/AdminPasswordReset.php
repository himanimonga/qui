<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\AdminResetPassword as ResetPasswordNotification;
    class AdminPasswordReset extends Authenticatable
    {
        use Notifiable;
        protected $table = 'admin_password_resets';
        protected $guard = 'admin';

        protected $fillable = [
            'email', 'token', 'created_at'
        ];

    }