<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = [

       'title','status','user_id','description','post_id','type'
    ];

    protected $appends = ['user_name','user_email'];

     public function getuserNameAttribute()
		{
		   $data = $this->belongsTo('App\Model\User','user_id')->first('name');
            if(!empty($data)){ 
		   return $data['name'];
            	
		   }else{
		   	return null;
		   }
		   
		}

		 public function getuserEmailAttribute()
		{
		   $data = $this->belongsTo('App\Model\User','user_id')->first('email');
            if(!empty($data)){ 
		   return $data['email'];
            	
		   }else{
		   	return null;
		   }
		   
		}


}
