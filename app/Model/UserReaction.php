<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class UserReaction extends Model
{
	 

    protected $fillable = [

       'status','user_id','post_id','post_type'
    ];
}
