<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Post;

use Carbon\Carbon;
use DateTime;
use App\Model\JoinLocation;
use DB;

class JoinLocation extends Model
{
    protected $fillable = [

       'id','user_id','location_name','latitude','longitude','join','image','location_type','post_id','unreadmsg_count'
    ];
	
		public function Join_Userdata(){
		return $this->belongsTo('App\Model\User','user_id');
	}
	
	protected $appends = ['post_name','channel','url','post_type','type','location','created_at'];
	
       

	

		public function getlocationAttribute()
		{
		   
		   return true;
		   
		}

		


		
	/*public function getusercountAttribute()
		{
		   $data= JoinLocation::where(['status'=>0])->distinct();
		   return $data;
		   
		}*/

		public function getcreatedAtAttribute()
		{
		   
		  //return $this->belongsTo('App\Model\Post','post_id')->where('status',0)->orderBy('id','Desc')->pluck('created_at')->first();
			if($this->location_type==1){ 
			// $quert=DB::table('join_locations')->Select('join_locations.post_id')->where(['latitude'=>$this->latitude,'longitude'=>$this->longitude,'location_name'=>$this->location_name,'status'=>0])->first();
		  
			$data= Post::where('location_id',$this->post_id)->orderBy('id','Desc')->first();
		   //return $data; die;
		   if(!empty($data)){
			$date= Carbon::parse($data['created_at'])->format('Y-m-d H:i:s');
		   }else{
				$data =DB::table('join_locations')->Select('join_locations.created_at')->where('id',$this->id)->first();
				//return $data->created_at;
			  $date= Carbon::parse($data->created_at)->format('Y-m-d H:i:s');
		   }
			
			$datetime = new \DateTime($date);

			$datetimez = clone $datetime;
			$datetime->setTimeZone(new \DateTimeZone('Zulu'));
			$datetime->setTimeZone(new \DateTimeZone('Etc/UTC'));
			$datetime->setTimeZone(new \DateTimeZone('UTC'));
			return $datetime->format('Y-m-d\TH:i:s.u\Z');
			
		 }else{ 
           
		   $quert=DB::table('join_locations')->Select('join_locations.id')->where(['latitude'=>$this->latitude,'longitude'=>$this->longitude,'location_name'=>$this->location_name,'status'=>0])->first();
		   $data= Post::where('location_id',$quert->id)->orderBy('id','Desc')->first();
		   if(!empty($data)){
		    $date= Carbon::parse($data['created_at'])->format('Y-m-d H:i:s');
		  }else{
				$data =DB::table('join_locations')->Select('join_locations.created_at')->where('id',$this->id)->first();
				//return $data->created_at;
			  $date= Carbon::parse($data->created_at)->format('Y-m-d H:i:s');
		   }
			
			$datetime = new \DateTime($date);

			$datetimez = clone $datetime;
			$datetime->setTimeZone(new \DateTimeZone('Zulu'));
			$datetime->setTimeZone(new \DateTimeZone('Etc/UTC'));
			$datetime->setTimeZone(new \DateTimeZone('UTC'));
			return $datetime->format('Y-m-d\TH:i:s.u\Z'); 
			 
		   }
		 
		   
		}
		
		public function gettypeAttribute()
		{
		   
		   return 'public';
		   
		}
	
		public function getpostnameAttribute()
		{
		    if($this->post_id != 0){
		       return $this->location_name ;
		    }
		   
		}
		
		public function getchannelAttribute()
		{
		    
		   return $this->location_name ;
		   
		}
		
		public function geturlAttribute()
		{
		  
		        return asset('/share_page/' .$this->id);
		   
		}
		
		public function getposttypeAttribute()
		{
		  
		        return 0;
		   
		}
		
		public function Postdata(){
		return $this->belongsTo('App\Model\Post','post_id')->where('status',0);
	   }
	   
	  /* public function usercount(){
		return $this->hasMany('App\Model\JoinLocation')->where('status',0);
	   }*/
	
		
		
		
		
		
		
		
		
		
		
		/*public function ChatJoin() {
             $data = $this->hasMany('App\Model\ChatJoin','channel')->where('status',0)->pluck('join_id');
                if(!empty($data)){
              $unreadmsg= $this->hasMany('App\Model\Chat','channel')->where('deleteChannel',0);
             
              }
             return $unreadmsg;
        }*/
    
}
