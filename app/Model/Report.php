<?php

namespace App\Model;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Kyslik\ColumnSortable\Sortable;

class Report extends Authenticatable
{
     use Sortable;
    use Notifiable, HasApiTokens;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'userid','user_name','user_email', 'post_userid','post_username','post_useremail', 'post_id','post_title','title','msg','status','created_at','updated_at','type'
    ];

   public $sortable = ['user_name', 'user_email','post_username','post_useremail','post_title','title','msg'];
   
   
}
