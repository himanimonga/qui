<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class passwordReset extends Model
{
    protected $fillable = [

       'email','token','created_at','updated_at'
    ];
}
