<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Post extends Model
{
	 use Sortable;

	public $sortable = ['latitude','longitude','tittle','description','post_type','user_id','price','price_type','needed_price','post_category','created_at'];

    protected $fillable = [

       'file_type','post','latitude','longitude','tittle','description','image','post_type','user_id','price','needed_price','price_type','user_longitude','post_category','address','address_second','recommend','type','location_id','location_name','updated_at'
    ];

		protected $appends = ['user_name','user_image','user_longitude','user_latitude'];

		 public function getuserLongitudeAttribute()
		{
		   $data = $this->belongsTo('App\Model\User','user_id')->first('longitude');
            if(!empty($data)){ 
		   return $data['longitude'];
            	
		   }else{
		   	return null;
		   }
		   
		}

		public function getuserLatitudeAttribute()
		{
		   $data = $this->belongsTo('App\Model\User','user_id')->first('latitude');
            if(!empty($data)){ 
		   return $data['latitude'];
            	
		   }else{
		   	return null;
		   }
		   
		}
	  
	   public function getuserImageAttribute()
		{
		   $data = $this->belongsTo('App\Model\User','user_id')->first('image');
            if(!empty($data) && $data['image']!=null){ 
		   //return asset('upload/image/' .$data['image']);
            	$url = parse_url($data['image'], PHP_URL_SCHEME);
            	if(!empty($url)){
            		return $data['image'];
            	}else{
            		return asset('upload/image/' .$data['image']);
            	}
            	
		   }else{
		   	return null;
		   }
		   
		}

		 public function getuserNameAttribute()
		{
		   $data = $this->belongsTo('App\Model\User','user_id')->first('name');
            if(!empty($data)){ 
		   return $data['name'];
            	
		   }else{
		   	return null;
		   }
		   
		}

		 public function children() {
		  return $this->hasMany('App\Model\Post', 'location_id')->where('type', 'Sub_discover')->orderBy('id','Desc');
		}

		/* public function Image()
		{
		    if ($this->children()->image) {
		        return asset('upload/image/' . $this->children()->image);
		    } else {
		        return null;
		    }
		}

				public function getImageAttribute($value)
		{
		    if ($value) {
		        return asset('upload/image/' . $value);
		    } else {
		        return null;
		    }
		} */

		
		 public function userData() {
		  return $this->belongsTo('App\Model\User', 'user_id');
		}

		 public function Image()
		{
		    if ($this->image) {
		        return asset('upload/image/' . $this->image);
		    } else {
		        return null;
		    }
		}

				public function getImageAttribute($value)
		{
		    if ($value) {
		        return asset('upload/image/' . $value);
		    } else {
		        return null;
		    }
		}
}
