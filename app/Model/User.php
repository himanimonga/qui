<?php

namespace App\Model;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Kyslik\ColumnSortable\Sortable;

class User extends Authenticatable
{
     use Sortable;
    use Notifiable, HasApiTokens;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','username','location','social_type','social_id','image','status','deleteStatus','latitude','longitude','distance','private_notification','other_notification'
    ];

    public $sortable = ['name', 'email','username'];

    protected $appends = ['client_id','type','user_name','user_image'];
    
       public function getClientIdAttribute()
        {
           return $this->id;
           
        }

        public function getTypeAttribute()
        {
           return 'private';
           
        }

        public function getUserNameAttribute()
        {
           return $this->name;
           
        }

        public function getuserImageAttribute()
        {
           return $this->image;
           
        }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    

     public function NotjoinData(){
        return $this->belongsTo('App\Model\ChatJoin','join_id');
    }
}
