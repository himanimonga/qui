<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Favourite_geolocation extends Model
{
    protected $fillable = [

       'name','image','address','latitude','longitude','user_id'
    ];
}
