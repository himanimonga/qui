<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Str;
use App\Model\passwordReset;
//use Illuminate\Notifications\forgotPassword;

class forgotPassword extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
	
	$reset= passwordReset::where('email',$notifiable->email)->get()->toArray();
		$token=  Str::random(40);
				if(!empty($reset)){
				    passwordReset::where('email',$notifiable->email)->update(['status'=>0,'token'=>$token]);
				}else{
				     passwordReset::create(['email'=>$notifiable->email,'token'=>$token]);
				}
        return (new MailMessage)
		               //->from('himani@braintechnosys.com')
                    //->line('You are receiving this email because we received a password reset request for your account.')

                     ->greeting('Hi '.$notifiable->first_name.',')
					->line("We received a request to reset your password for your QUI account. We're here to help!")
					//->line($notifiable->email.'. We are here to help!')
					->line('Simply click on the button to set a new password:')
                   ->action('Set A New Password',url('/forgotPassword/'.$token.'/'.$notifiable->email))
                      ->line("If you did'nt ask to change your password, don't worry! Your password is still safe and you can delete this email.")
                       ->line("Cheers,");

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
