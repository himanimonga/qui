@extends('admin.layouts.login')

@section('title', 'login here')


@section('content')
	<form class="sign-in-form" method="POST" action="{{ route('admin.auth.loginAdmin') }}">
		{{ csrf_field() }}
		<div class="card">
			<div class="card-body">
				<a href="{{ url('/admin') }}" class="brand text-center d-block m-b-20">
					<img src="{{asset('admin')}}/img/logo/qui.jpg" alt="Logo" />
					
				</a>
				
				<h5 class="sign-in-heading text-center m-b-20">Administrator/Admin Login</h5>
				<h5 class="sign-in-heading text-center m-b-20">Sign in to your account</h5>
				
				<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
					<label for="inputEmail" class="sr-only">Email address</label>
					
					<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>

					@if ($errors->has('email'))
						<span class="help-block">
							<strong>{{ $errors->first('email') }}</strong>
						</span>
					@endif
				</div>

				<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
					<label for="inputPassword" placeholder="Password" class="sr-only">Password</label>

					<input id="password" type="password" class="form-control" name="password" required>

					@if ($errors->has('password'))
						<span class="help-block">
							<strong>{{ $errors->first('password') }}</strong>
						</span>
					@endif
					
				</div>

				<div class="checkbox m-b-10 m-t-20">
					<div class="col-md-offset-4">
						<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
						<a href="{{ route('admin.auth.forgotpassword')}}" class="float-right">Forgot Password?</a>
					</div>
					
				</div>
				
				<button type="submit" class="btn btn-primary btn-floating btn-lg btn-block admin_login">
					Login
				</button>
					
			</div>
		</div>
	</form>           
@endsection