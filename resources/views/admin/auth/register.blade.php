@extends('admin.layouts.app')
	@section('breadcrumbs')
		@include('includes.breadcrumb', ['breadcrumbs' => [
		    					'Listing',
						]])
	@endsection
	
	

	@section('content')
<style>
.error-message{
color: #f30f0f;
}
</style>
 <section class="page-content container">
	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<form method="post" action="{{ route('admin.register.store') }}" enctype="multipart/form-data" autocomplete="off">
					  @csrf
					 
					  <div class="row">
						
						<div class="col-md-6">
						
								<div class="form-group">
									<label>Name*</label>
									<input type="text" name="name" class="form-control" placeholder="Name" value="{{ old('name') }}">
									
									@error('name')
										<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
						</div>
						<div class="col-md-6">
								<div class="form-group">
									<label>Email*</label>
									<input type="text" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}">
									@error('email')
										<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
						
						<div class="col-md-6">
						
								<div class="form-group">
									<label>Password*</label>
									<input type="password" name="password" class="form-control" placeholder="password" value="{{ old('password') }}">
									
									@error('password')
										<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
						</div>
						<div class="col-md-6">
								<div class="form-group">
									<label>Re Enter Password*</label>
									<input type="password" name="password_confirmation" class="form-control" placeholder="password" value="{{ old('password_confirmation') }}">
									@error('password_confirmation')
										<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							
							
							<div class="col-md-6">
								<div class="form-group">
								<label>Module*<span class="required"></span></label>
									
									<div class="form-check">
								<label><input type="checkbox" id="subscribeNews" class="form-check-input" name="module[]" value='admin.user|admin.disabled|admin.user.create|admin.active|admin.inactive|admin.user.store|admin.user.update|admin.user.edit|admin.userDestroy|admin.userDisable|admin.user_status'>Manage Users</label></div>
								<div class="form-check">
												<label><input type="checkbox" id="subscribeNews" class="form-check-input" name="module[]" value='admin.category|admin.edit.category|admin.categoryAdd|admin.category.update|admin.categoryDelete' >Category</label>
												</div>
												<div class="form-check">
												<label><input class="form-check-input" type="checkbox" id="subscribeNews"  name="module[]" value='admin.post_userDetails_needed|admin.post_userDetails_offered|admin.post_userDetails_news|admin.needed|admin.news|admin.offered|admin.editPost_news|admin.Postview_news|admin.editPost_offered|admin.Postview_offered|admin.Postview_needed|admin.editPost_needed|admin.warning|admin.help|admin.Postview_warning|admin.Postview_help|admin.editPost_help|admin.editPost_warning|admin.track|admin.newest|admin.popular|admin.recommend_more|admin.recommend|admin.postChange_status|admin.recommendChange|admin.postDelete|admin.recommend_more'   >Manage Post</label>
												</div>
												<div class="form-check">
												<label><input class="form-check-input" type="checkbox" id="subscribeNews"  name="module[]" value='admin.cms|admin.editCms' >Cms</label></div>
												<div class="form-check">
												<label><input class="form-check-input" type="checkbox" id="subscribeNews"  name="module[]" value="admin.feedback','admin.feedback_edit|admin.feedback.view|admin.feedbackDelete|admin.feedbackUpdate" >Feedback</label></div>
												<div class="form-check">
												<label><input class="form-check-input" type="checkbox" id="subscribeNews"  name="module[]" value="admin.chat|admin.chat_edit|admin.chatList|admin.chat_delete" >Chat</label></div>
												<div class="form-check">
												<label><input class="form-check-input" type="checkbox" id="subscribeNews"  name="module[]" value="admin.pushnotification|admin.pushnotificationAdd|admin.notificationStatus|admin.Post_pushnotificationAdd|admin.pushnotificationDelete" >Notification</label>
											</div>
												
									
									@error('module')
										<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
							</div>
						
						
						<div class="card-footer row">
							<div class="input submit">
								<div class="submit">
									<input class="btn btn-primary" type="submit" value="Save">
								</div>
							</div>
						</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		</div>
</section>
@endsection



