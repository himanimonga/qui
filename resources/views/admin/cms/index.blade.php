@extends('admin.layouts.app')
	@section('breadcrumbs')
		@include('includes.breadcrumb', ['breadcrumbs' => [
		                    
		    					'Listing'
						]])
	@endsection
@section('content')



<section class="page-content container-fluid">

	<div class="row">

		<div class="col-lg-12">

			<div class="card">

				<div class="card-body">

					<!--<div class="card-header">

						<a href="{{ route('admin.addCms')}}" class="btn btn-danger text-white">Add More</a>

					

					<div class="col-lg-6">	

										<form action="{{ route('admin.cms') }}" method="GET">

												<div class="form-group">

													<div class="input-group mb-3">

															<input type="text" class="form-control" placeholder="Search by name" name="search" value="{{ Request::get('search') }}">

															<div class="input-group-append">

																<input type="submit" class="form-control" value="Search">

															</div>

													</div>

												</div>

											</form>

									</div>

									</div>-->

					<div class="table-responsive">

						<table class="table table-striped">

							<thead>

								<tr>

									<th><strong><b>Purpose</b></strong></th>

									<!-- <th><strong>@sortablelink('title','Page Title')</strong></th> -->

									<th><strong><b>Actions</b></strong></th>

								</tr>

							</thead>

							<tbody>

								@if(!$users->isEmpty())

								@foreach($users as $key => $value)

								<tr>

									<td>{{$value['page_uses']}}</td>

									<!-- <td>{{$value['title']}}</td> -->

									<td>

										<a href="{{ route('admin.editCms',$value['id']) }}">

											<i class="zmdi zmdi-edit zmdi-hc-fw font-size-16"></i>	

										</a>

									</td>

								</tr>

								@endforeach

								@else

									<tr><td colspan="4" class="text-center">No record found</td></tr>

								@endif

							</tbody>

						</table>

							@if(!empty($users->links()))

								<div class="pull-right">

										<div class="dataTables_paginate paging_simple_numbers" id="bs4-table_paginate">

											<ul class="pagination">

												<li class="paginate_button page-item">{{ $users->links() }}</li>

											</ul>

										</div>

								</div>

								@endif

					</div>

				</div>

			</div>

		</div>

	</div>

</section>

<script>

	$(document).ready(function(){

		$('body').on('click','#deleteBlog',function(){   

			Swal.fire({

			  title: 'Are you sure?',

			  text: "You won't be able to revert this!",

			  type: 'warning',

			  showCancelButton: true,

			  confirmButtonColor: '#3085d6',

			  cancelButtonColor: '#d33',

			  confirmButtonText: 'Yes, delete it!'

			}).then((result) => {

			  if (result.value) {

					$("#deleteblogForm").submit();

			  }

			})



		});

	});

	</script>

@endsection