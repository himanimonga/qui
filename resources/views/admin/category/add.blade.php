@extends('admin.layouts.app')
@section('breadcrumbs')
@include('includes.breadcrumb', ['breadcrumbs' => [
'Listing' => route('admin.category'),
'Add New'
]])
@endsection
@section('content')
<section class="page-content container">
	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<form method="post" action="{{ route('admin.Post_categoryAdd') }}" enctype="multipart/form-data" autocomplete="off">
						@csrf
						<div class="row">
							
							
							<div class="col-md-6">
								<div class="form-group">
									<label>Name*</label>
									<input type="text" name="name" class="form-control" placeholder="Name" value="{{ old('name') }}">
									
									@error('name')
									<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							
							
							<!--<div class="col-md-6">
								<div class="form-group">
									<label>Blog image*</label>
									<input type="file" name="image" class="form-control"><b>Upload minimum width 874px and hight 512px image.</b>
									
									@error('image')
									<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
							</div>-->
							
						</div>
						
						<div class="card-footer row">
							<div class="input submit">
								<div class="submit">
									<input class="btn btn-primary" type="submit" value="Save">
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>


@endsection