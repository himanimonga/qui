@extends('admin.layouts.app')
	@section('breadcrumbs')
		@include('includes.breadcrumb', ['breadcrumbs' => [
		    					'Listing' => route('admin.category'),
		    					 $post->title
						]])
	@endsection
@section('content')
<section class="page-content container">
	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<form method="post" action="{{ route('admin.category.update', $post->id) }}" autocomplete="off" enctype="multipart/form-data">
					  @csrf
					  @method('PUT')
						<div class="row">
						<input type="hidden" name="page_name" class="form-control"value="needed">
							<div class="col-md-6">
								<div class="form-group">
									<label>Name*</label>
									<input type="text" name="name" class="form-control" placeholder="Name" value="{{ old('name', $post->name) }}">
									
									@error('name')
										<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
							</div>
							
							
							<div class="col-md-6">
								<div class="form-group">
									<label>Image*</label>
									<input type="file" name="image" class="form-control">
									
									@error('image')
										<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
							</div>
						</div>
							<div class="row">
							<div class="col-md-6">
							<?php if(!empty($post->image)){ ?>
								<img src="{{ url('admin/images/add-more-icons/img/'.$post->image) }}">
							<?php } ?>
							</div>
						</div>
						<div class="card-footer row">
							<div class="input submit">
								<div class="submit">
									<input class="btn btn-primary" type="submit" value="Update">
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection