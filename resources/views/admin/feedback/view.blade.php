@extends('admin.layouts.app')
@section('breadcrumbs')
		@include('includes.breadcrumb', ['breadcrumbs' => [
		    					 'Listing' => route('admin.warning'),
		    					
						]])
	@endsection
@section('content')
<section class="page-content container-fluid">
	<div class="card">
		
		<div class="card-body">
		<!--	<ul class="nav nav-pills nav-pills-primary mb-3" id="pills-demo-1" role="tablist">
										<li class="nav-item">
											<a class="nav-link active font-size-12" id="pills-1-tab" data-toggle="pill" href="#pills-1" role="tab" aria-controls="pills-1" aria-selected="true">Contact Info</a>
										</li>
										<li class="nav-item">
											<a class="nav-link font-size-12" id="pills-2-tab" data-toggle="pill" href="#pills-2" role="tab" aria-controls="pills-2" aria-selected="false">Demographic Info</a>
										</li>
										<li class="nav-item">
											<a class="nav-link font-size-12" id="pills-3-tab" data-toggle="pill" href="#pills-3" role="tab" aria-controls="pills-3" aria-selected="false">Diagnosis Info</a>
										</li>
										<li class="nav-item">
											<a class="nav-link font-size-12" id="pills-4-tab" data-toggle="pill" href="#pills-4" role="tab" aria-controls="pills-3" aria-selected="false">Treatment Info</a>
										</li>
										<li class="nav-item">
											<a class="nav-link font-size-12" id="pills-5-tab" data-toggle="pill" href="#pills-5" role="tab" aria-controls="pills-3" aria-selected="false">Support Questions</a>
										</li>
										<li class="nav-item">
											<a class="nav-link font-size-12" id="pills-6-tab" data-toggle="pill" href="#pills-6" role="tab" aria-controls="pills-3" aria-selected="false">Support Survey</a>
										</li>
										<li class="nav-item">
											<a class="nav-link font-size-12" id="pills-7-tab" data-toggle="pill" href="#pills-7" role="tab" aria-controls="pills-3" aria-selected="false">Terms Use</a>
										</li>
				</ul>-->
			
			<div class="tab-content" id="pills-tabContent-1">
				
				<div class="tab-pane fade show active" id="pills-1" role="tabpanel" aria-labelledby="pills-1">
					<div class="card card-bg-color">
						<div class="card-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label><b>Date:</b></label>
										
										<label >@php $date=strtotime($post['created_at']); @endphp {{ date('d-M-Y',$date)}}</label>
										
									</div>
								</div>
							</div>
							<div class="row">
								
								<div class="col-md-6">
									<div class="form-group">
										<label><b>Sender Name:</b></label>
										<label>{{ old('sender_name', $post->sender_name) }}</label>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label><b>Sender Email:</b></label>
										<label>{{ old('sender_email', $post->sender_email) }}</label>
									</div>
								</div>

								<!--div class="col-md-6">
									<div class="form-group">
										<label><b>Price:</b></label>
										<label >{{ old('price', $post->price) }}</label>
									</div>
								</div-->
								
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label><b>Receiver Name:</b></label>
										<label>{{ old('	receiver_name', $post->receiver_name) }}</label>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label><b>Receiver Email:</b></label>
										<label>{{ old('receiver_email', $post->receiver_email) }}</label>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label><b>Rating:</b></label>
										<label>{{ old('	rating', $post->rating) }}</label>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label><b>Comment:</b></label>
										<label>{{ old('comment', $post->comment) }}</label>
									</div>
								</div>
								
								
								<!--<div class="col-md-6">
									<div class="form-group">
										<label><b>Street Address:</b></label>
										<label >dgfgf</label>
									</div>
								</div>-->
							</div>
						
							</div>
					</div>
				</div>
                </div>
                  <div><a href="{{url('feedback')}}" class="btn btn-primary text-white">Back</a>
							</div>
			</div>
		</div>
	</section>

@endsection