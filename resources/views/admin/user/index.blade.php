@extends('admin.layouts.app')
@section('breadcrumbs')
    @include('includes.breadcrumb', ['breadcrumbs' => ['Listing']])
@endsection
@section('content')
    <section class="page-content container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form action="{{ route('admin.user') }}" method="GET">
                                        <div class="form-group">
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" placeholder="Search by name"
                                                       name="search" value="{{ Request::get('search') }}">
                                                <div class="input-group-append">
                                                    <input type="submit" class="form-control" value="Search">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-lg-6 text-right" style="line-height:0;">
                                    <a href="{{route("admin.user.create")}}" class="btn btn-primary add_btn_top"
                                       style="">Add User </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th><strong>@sortablelink('name','Name')</strong></th>
                                <th><strong>@sortablelink('username','Username')</strong></th>
                                <th><strong>@sortablelink('email','Email')</strong></th>
                                <th><strong>@sortablelink('location','Location')</strong></th>
                                <th><strong>@sortablelink('created_at','Date')</strong></th>
                                <th><strong>Status</strong></th>
                                <th><strong>Actions</strong></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!$users->isEmpty())
                                @foreach($users as $key => $value)
                                    @if($value['deleteStatus'] == 0)
                                        <tr>
                                            <td>{{$value['name']}}</td>
                                            <td>{{$value['username']}}</td>
                                            <td>{{$value['email']}}</td>
                                            <td>{{$value['location']}}</td>

                                            <td>@php $date=strtotime($value['created_at']); @endphp {{ date('d-M-Y',$date)}}</td>
                                            <td>
                                                @if($value['status'] == 0)
                                                    <a data-status='0' data-toggle='tooltip' data-placement='top'
                                                       data-original-title='Active' href="javascript:void(0)"
                                                       data-id="{{$value['id']}}" class="updateStatus">
                                                        <i class="zmdi zmdi-shield-check zmdi-hc-fw font-size-20"></i>
                                                    </a>
                                                @else
                                                    <a data-status='1' data-toggle='tooltip' data-placement='top'
                                                       data-original-title='Dective' href="javascript:void(0)"
                                                       data-id="{{$value['id']}}" class="updateStatus_active">
                                                        <i class="zmdi zmdi-close-circle zmdi-hc-fw font-size-20"></i>
                                                    </a>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('admin.user.edit',$value['id']) }}"
                                                   data-id="{{$value['id']}}" >
                                                    <i class="zmdi zmdi-edit zmdi-hc-fw font-size-20"></i>
                                                </a>
                                                <a data-status='1' data-toggle='tooltip' data-placement='top'
                                                   data-original-title='temporary Delete' href="javascript:void(0)"
                                                   data-id="{{$value['id']}}" class="disableStatus">
                                                    <i class="zmdi zmdi-delete zmdi-hc-fw font-size-20"></i>
                                                </a>
                                            </td>
                                            @endif
                                        </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="4" class="text-center">No record found</td>
                                        </tr>
                                    @endif
                            </tbody>
                        </table>
                        @if(!empty($users->links()))
                            <div class="pull-right">
                                <div class="dataTables_paginate paging_simple_numbers" id="bs4-table_paginate">
                                    <ul class="pagination">
                                        <li class="paginate_button page-item">{{ $users->links() }}</li>
                                    </ul>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $("body").on("click", ".updateStatus", (function () {
            var t = this;

            Swal.fire({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Yes, deactive it!"
            }).then((function (e) {
                if (e.value) {
                    var r = $(t).attr("data-id");
                    $(t).parents("form:first").serialize();
                    var urlw = '{{route("admin.userDestroy")}}';
                    $.ajax({
                        url: urlw, type: "post", data: {id: r}, success: function (t) {
                            t.status ? (toastr.success(t.msg, "Success"), setTimeout((function () {
                                location.reload()
                            }), 3e3)) : toastr.error(t.msg, "Success")
                        }
                    })
                }
            }))
        }));

        $("body").on("click", ".updateStatus_active", (function () {
            var t = this;

            Swal.fire({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Yes, active it!"
            }).then((function (e) {
                if (e.value) {
                    var r = $(t).attr("data-id");
                    $(t).parents("form:first").serialize();
                    var urlw = '{{route("admin.userDestroy")}}';
                    $.ajax({
                        url: urlw, type: "post", data: {id: r}, success: function (t) {
                            t.status ? (toastr.success(t.msg, "Success"), setTimeout((function () {
                                location.reload()
                            }), 3e3)) : toastr.error(t.msg, "Success")
                        }
                    })
                }
            }))
        }));


        $("body").on("click", ".disableStatus", (function () {
            var t = this;

            Swal.fire({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Yes, delete it!"
            }).then((function (e) {
                if (e.value) {
                    var r = $(t).attr("data-id");
                    $(t).parents("form:first").serialize();
                    var urlw = '{{route("admin.user_status")}}';
                    $.ajax({
                        url: urlw, type: "post", data: {id: r}, success: function (t) {
                            t.status ? (toastr.success(t.msg, "Success"), setTimeout((function () {
                                location.reload()
                            }), 3e3)) : toastr.error(t.msg, "Success")
                        }
                    })
                }
            }))
        }));
    </script>

@endsection
