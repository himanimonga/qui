@extends('admin.layouts.app')
@section('breadcrumbs')
@include('includes.breadcrumb', ['breadcrumbs' => [
'Listing' => route('admin.subadmin_list'),
$blog->name
]])
@endsection
@section('content')
<section class="page-content container">
	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<form method="post" action="{{ route('admin.subadmin_updatemodule') }}" enctype="multipart/form-data" autocomplete="off">
						@csrf
						
						<div class="row">
							
							<div class="col-md-6">
								<input type="hidden" name="id" value="{{$blog['id']}}">
								<div class="form-group">
									<label>Name*</label>
									<input type="text" name="name" class="form-control" placeholder="Name" value="{{ old('name', $blog['name']) }}">
									
									@error('name')
									<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Email*</label>
									<input type="text" name="email" class="form-control" placeholder="Email" value="{{ old('email', $blog['email']) }}">
									@error('email')
									<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							
							<?php  $allowed_routes=[];  
								$task= explode(',',$blog['task']);
								if(!empty($task)){
									foreach($task as $route){
										$allowed_routes = array_merge($allowed_routes,@explode('|',$route));
									}
								}
								//print_r($allowed_routes);			
							?>
							<div class="col-md-6">
								<div class="form-group">
									<label>Module*<span class="required"></span></label>
									
									<div class="form-check">
									<label><input type="checkbox" id="subscribeNews" class="form-check-input" name="task[]" value='admin.user|admin.disabled|admin.user.create|admin.active|admin.inactive|admin.user.store|admin.user.update|admin.user.edit|admin.userDestroy|admin.userDisable|admin.user_status' @if (in_array('admin.user',$allowed_routes)) {{"checked='checked'"}} @endif >Manage Users</label></div>
									<div class="form-check">
							<label><input type="checkbox" id="subscribeNews" class="form-check-input" name="task[]" value='admin.category|admin.edit.category|admin.categoryAdd|admin.category.update|admin.categoryDelete' @if (in_array('admin.category',$allowed_routes)) {{"checked='checked'"}} @endif >Category</label>
									</div>
									<div class="form-check">
						<label><input class="form-check-input" type="checkbox" id="subscribeNews"  name="task[]" value='admin.post_userDetails_needed|admin.post_userDetails_offered|admin.post_userDetails_news|admin.needed|admin.news|admin.offered|admin.editPost_news|admin.Postview_news|admin.editPost_offered|admin.Postview_offered|admin.Postview_needed|admin.editPost_needed|admin.warning|admin.help|admin.Postview_warning|admin.Postview_help|admin.editPost_help|admin.editPost_warning|admin.track|admin.newest|admin.popular|admin.recommend_more|admin.recommend|admin.postChange_status|admin.recommendChange|admin.postDelete|admin.recommend_more' @if (in_array('admin.post_userDetails_needed',$allowed_routes)) {{"checked='checked'"}} @endif >Manage Post</label>
									</div>
									<div class="form-check">
					<label><input class="form-check-input" type="checkbox" id="subscribeNews"  name="task[]" value='admin.cms|admin.editCms' @if (in_array('admin.cms',$allowed_routes)) {{"checked='checked'"}} @endif>Cms</label></div>
									
									
									<div class="form-check">
					<label><input class="form-check-input" type="checkbox" id="subscribeNews"  name="task[]" value="admin.feedback','admin.feedback_edit|admin.feedback.view|admin.feedbackDelete|admin.feedbackUpdate" @if (in_array('admin.feedback',$allowed_routes)) {{"checked='checked'"}} @endif >Feedback</label></div>
									<div class="form-check">
						<label><input class="form-check-input" type="checkbox" id="subscribeNews"  name="task[]" value="admin.chat|admin.chat_edit|admin.chatList|admin.chat_delete" @if (in_array('admin.chat',$allowed_routes)) {{"checked='checked'"}} @endif >Chat</label></div>
									<div class="form-check">
					<label><input class="form-check-input" type="checkbox" id="subscribeNews"  name="task[]" value="admin.pushnotification|admin.pushnotificationAdd|admin.notificationStatus|admin.Post_pushnotificationAdd|admin.pushnotificationDelete" @if (in_array('admin.pushnotification',$allowed_routes)) {{"checked='checked'"}} @endif >Notification</label></div>
									
									
								</div>
								


								@error('task')
								<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
								@enderror
							</div>
						</div>
						
						
						<div class="card-footer row">
							<div class="input submit">
								<div class="submit">
									<input class="btn btn-primary" type="submit" value="Save">
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
</section>


@endsection