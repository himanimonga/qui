@extends('admin.layouts.app')
	@section('breadcrumbs')
		@include('includes.breadcrumb', ['breadcrumbs' => [
		    				'Home'
						]])
	@endsection
@section('content')
<section class="page-content container-fluid">
	<div class="row">
		<div class="col-12">
							<div class="card">
								<div class="row m-0 col-border-xl">
									<div class="col-md-12 col-lg-6 col-xl-4">
										<div class="card-body">
											<div class="icon-rounded icon-rounded-primary float-left m-r-20">
												<i class="icon dripicons-graph-bar"></i>
											</div>
											<h5 class="card-title m-b-5 counter" data-count="{{$data['total']}}">0</h5>
											<a href="{{route('admin.user')}}"><h6 class="text-muted m-t-10">
												Total Users
											</h6></a>
											<div class="progress progress-active-sessions mt-4" style="height:7px;">
												<div class="progress-bar bg-primary" role="progressbar" style="" aria-valuenow="72" aria-valuemin="0" aria-valuemax="100"></div>
											</div>
											<!--<small class="text-muted float-left m-t-5 mb-3">
												Change
											</small>
											<small class="text-muted float-right m-t-5 mb-3 counter append-percent" data-count="72">
												0
											</small>-->
										</div>
									</div>
									<div class="col-md-12 col-lg-6 col-xl-4">
										<div class="card-body">
											<div class="icon-rounded icon-rounded-accent float-left m-r-20">
												<i class="la la-users"></i>
											</div>
											<h5 class="card-title m-b-5 counter" data-count="{{$data['active']}}">0</h5>
											<a href="{{route('admin.active')}}"><h6 class="text-muted m-t-10">
												Active Users
											</h6></a>
											<div class="progress progress-add-to-cart mt-4" style="height:7px;">
												<div class="progress-bar bg-accent" role="progressbar" style="" aria-valuenow="67" aria-valuemin="0" aria-valuemax="100"></div>
											</div>
											<!--<small class="text-muted float-left m-t-5 mb-3">
												Change
											</small>
											<small class="text-muted float-right m-t-5 mb-3 counter append-percent" data-count="67">
												0
											</small>-->
										</div>
									</div>
									<div class="col-md-12 col-lg-6 col-xl-4">
										<div class="card-body">
											<div class="icon-rounded icon-rounded-info float-left m-r-20">
												<i class="la la-users"></i>
											</div>
											<h5 class="card-title m-b-5 counter" data-count="{{$data['deactivate']}}">0</h5>
											<a href="{{route('admin.inactive')}}"><h6 class="text-muted m-t-10">
												Deactive Users
											</h6></a>
											<div class="progress progress-new-account mt-4" style="height:7px;">
												<div class="progress-bar bg-info" role="progressbar" style="" aria-valuenow="83" aria-valuemin="0" aria-valuemax="100"></div>
											</div>
											<!--<small class="text-muted float-left m-t-5 mb-3">
												Change
											</small>
											<small class="text-muted float-right m-t-5 mb-3 counter append-percent" data-count="83">
												0
											</small>-->
										</div>
									</div>
									<div class="col-md-12 col-lg-6 col-xl-4">
										<div class="card-body">
											<!--div class="icon-rounded icon-rounded-info float-left m-r-20">
										<button onclick="notifyMe()">Notify me!</button>
											</div-->
											
											
											
										</div>
									</div>
									
								</div>
							</div>
						</div>
	</div>
	
@endsection