<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
	<link rel="shortcut icon" href="{{asset('admin')}}/img/favicon.ico" />
	<link rel="stylesheet" href="{{asset('admin')}}/css/vendor/bootstrap.css">
	<link rel="stylesheet" href="{{asset('')}}css/toastr.min.css">
	<link rel="stylesheet" href="{{asset('admin')}}/vendor/metismenu/dist/metisMenu.css">
	<link rel="stylesheet" href="{{asset('admin')}}/vendor/switchery-npm/index.css">
	<link rel="stylesheet" href="{{asset('admin')}}/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
	<!-- ======================= LINE AWESOME ICONS ===========================-->
	<link rel="stylesheet" href="{{asset('admin')}}/css/icons/line-awesome.min.css">
	<link rel="stylesheet" href="{{asset('admin')}}/css/icons/simple-line-icons.css">
	<!-- ======================= DRIP ICONS ===================================-->
	<link rel="stylesheet" href="{{asset('admin')}}/css/icons/dripicons.min.css">
	<link rel="stylesheet" href="{{asset('admin')}}/css/jquery.multiselect.css">
	<!-- ======================= MATERIAL DESIGN ICONIC FONTS =================-->
	<link rel="stylesheet" href="{{asset('admin')}}/css/icons/material-design-iconic-font.min.css">
	<!-- ======================= PAGE VENDOR STYLES ===========================-->
	<link rel="stylesheet" href="{{asset('admin')}}/vendor/datatables.net-bs4/css/dataTables.bootstrap4.css">

    <link rel="stylesheet" href="{{asset('admin')}}/vendor/fullcalendar/dist/fullcalendar.css">
	
	<link rel="stylesheet" href="{{asset('admin')}}/vendor/bootstrap-datepicker/bootstrap-datepicker.min.css">
	<!-- ======================= GLOBAL COMMON STYLES ============================-->
	<link rel="stylesheet" href="{{asset('admin')}}/css/common/main.bundle.css?v=<?php echo time();?>">

	<!-- ======================= LAYOUT TYPE ===========================-->
	<link rel="stylesheet" href="{{asset('admin')}}/css/layouts/vertical/core/main.css">
	<!-- ======================= MENU TYPE ===========================-->
	<link rel="stylesheet" href="{{asset('admin')}}/css/layouts/vertical/menu-type/default.css">
	<!-- ======================= THEME COLOR STYLES ===========================-->
	<link rel="stylesheet" href="{{asset('admin')}}/css/layouts/vertical/themes/theme-a.css">
	<link rel="stylesheet" href="{{asset('')}}css/sweetalert2.min.css">
	
	<link rel="stylesheet" href="{{asset('admin')}}/vendor/bootstrap-daterangepicker/daterangepicker.css">
	<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css" />-->
    <link rel="stylesheet" href="{{asset('admin')}}/css/developer.css">
	
	
	
	
	
	<script src="https://cdn.pubnub.com/pubnub-3.7.13.min.js"></script>
	<script src="{{asset('admin')}}/vendor/jquery/dist/jquery.min.js"></script>
	<script src="{{asset('admin')}}/js/cards/counter-group.js"></script>
	<!--<script src="{{asset('admin')}}/js/jquery.multiselect.js"></script>-->
	<script src="{{asset('admin')}}/js/components/countUp-init.js"></script>
	<script src="{{asset('')}}/js/toastr.min.js"></script>
	
	<script src="{{asset('admin')}}/vendor/moment/min/moment.min.js"></script>
		<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/pikaday/1.6.1/pikaday.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.js"></script>-->
		<script src="{{asset('admin')}}/vendor/fullcalendar/dist/fullcalendar.min.js"></script>
		<script src="{{asset('admin')}}/js/components/fullcalendar-init.js"></script>
		
		<script src="{{asset('admin')}}/vendor/bootstrap-daterangepicker/daterangepicker.js"></script>
		<script src="{{asset('admin')}}/js/components/bootstrap-datepicker-init.js"></script>
		<script src="{{asset('admin')}}/js/components/bootstrap-date-range-picker-init.js"></script>
		<!--<script src="https://cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>-->
		<script src="{{asset('public/vendor')}}/unisharp/laravel-ckeditor/ckeditor.js"></script>
		<script src="{{asset('public/vendor')}}/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
		<script src="{{asset('admin')}}/js/wickedpicker.min.js"></script>
		<script src="https://cdn.pubnub.com/sdk/javascript/pubnub.4.19.0.min.js"></script>
		
		
        <!--script src="https://code.jquery.com/jquery-1.12.0.min.js"></script-->
		<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>-->

	<!-- Scripts -->
    
</head>
<body>
	
    <div id="app">
		@include('admin.layouts.leftmenu')
		<div class="content-wrapper">
			@include('admin.layouts.topsection')
	        <main class="py-4">
				@if(Session::has('message'))
					<script>
					var type = "{{ Session::get('alert-type', 'info') }}";
					switch(type){
						case 'info':
							toastr.info("{{ Session::get('message') }}");
							break;

						case 'warning':
							toastr.warning("{{ Session::get('message') }}");
							break;

						case 'success':
							toastr.success("{{ Session::get('message') }}");
							break;

						case 'error':
							toastr.error("{{ Session::get('message') }}");
							break;
					}

					</script>
				 @endif
	            @yield('content')
	        </main>
    	</div>
	</div>
	<script src="{{asset('admin')}}/vendor/modernizr/modernizr.custom.js"></script>
	<script src="{{asset('admin')}}/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
	<!--<script src="{{asset('admin')}}/vendor/jquery/dist/jquery.min.js"></script>-->
	
	<script src="{{asset('admin')}}/vendor/js-storage/js.storage.js"></script>
	<script src="{{asset('admin')}}/vendor/js-cookie/src/js.cookie.js"></script>
	<script src="{{asset('admin')}}/vendor/pace/pace.js"></script>
	<script src="{{asset('admin')}}/vendor/metismenu/dist/metisMenu.js"></script>
	<script src="{{asset('admin')}}/vendor/switchery-npm/index.js"></script>
	<script src="{{asset('admin')}}/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
	<!-- ================== PAGE LEVEL VENDOR SCRIPTS ==================-->
	<script src="{{asset('admin')}}/vendor/countup.js/dist/countUp.min.js"></script>
	<script src="{{asset('admin')}}/vendor/chart.js/dist/Chart.bundle.min.js"></script>
	<script src="{{asset('admin')}}/js/cards/users-chart.js"></script>
	<script src="{{asset('admin')}}/js/cards/bounce-rate-chart.js"></script>
	<script src="{{asset('admin')}}/js/cards/session-duration-chart.js"></script>
	
	<!-- datepicker  -->
	<script src="{{asset('admin')}}/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
	<script src="{{asset('admin')}}/js/components/bootstrap-datepicker-init.js"></script>
	<script src="{{asset('admin')}}/vendor/datatables.net/js/jquery.dataTables.js"></script>
	<script src="{{asset('admin')}}/vendor/datatables.net-bs4/js/dataTables.bootstrap4.js"></script>
	<!-- ================== GLOBAL APP SCRIPTS ==================-->
	<script src="{{asset('admin')}}/js/global/app.js"></script>
	<script src="{{asset('admin')}}/js/common.js"></script>
	<script src="{{asset('')}}js/sweetalert2.min.js"></script>
	
	
	
	
	<script>
	$.ajaxSetup({headers:{"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr("content")}})
	</script>

		

</body>
</html>
