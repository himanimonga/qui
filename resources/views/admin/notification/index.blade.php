@extends('admin.layouts.app')
@section('breadcrumbs')
    @include('includes.breadcrumb', ['breadcrumbs' => ['Listing']])
@endsection
@section('content')
    <section class="page-content container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form action="{{ route('admin.pushnotification') }}" method="GET">
                                        <div class="form-group">
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" placeholder="Search by name"
                                                       name="search" value="{{ Request::get('search') }}">
                                                <div class="input-group-append">
                                                    <input type="submit" class="form-control" value="Search">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-lg-6 text-right" style="line-height:0;">
                                    <a href="{{route("admin.pushnotificationAdd")}}" class="btn btn-primary add_btn_top"
                                       style="">Add Notification </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th><strong>@sortablelink('title','Title')</strong></th>
                                <th><strong>@sortablelink('description','Description')</strong></th>
                                <!--th><strong>@sortablelink('name','Name')</strong></th>
                                <th><strong>@sortablelink('email','Email')</strong></th-->
                                <th><strong>@sortablelink('created_at','Date')</strong></th>
                                <th><strong>Actions</strong></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!$users->isEmpty())
                                @foreach($users as $key => $value)
                                    @if($value['status'] == 0)
                                        <tr>
                                            <td>{{$value['title']}}</td>
                                            <td style="width: 474px;">{{$value['description']}}</td>
                                            <!--td>{{$value['user_name']}}</td>
                                            <td>{{$value['user_email']}}</td-->
                                            <td>@php $date=strtotime($value['created_at']); @endphp {{ date('d-M-Y',$date)}}</td>
                                           
                                            <td>
                                               
                                                <a data-status='1' data-toggle='tooltip' data-placement='top'
                                                   data-original-title='Delete' href="javascript:void(0)"
                                                   data-id="{{$value['id']}}" class="updateNotify">
                                                    <i class="zmdi zmdi-delete zmdi-hc-fw font-size-20"></i>
                                                </a>
                                            </td>
                                            @endif
                                        </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="4" class="text-center">No record found</td>
                                        </tr>
                                    @endif
                            </tbody>
                        </table>
                        @if(!empty($users->links()))
                            <div class="pull-right">
                                <div class="dataTables_paginate paging_simple_numbers" id="bs4-table_paginate">
                                    <ul class="pagination">
                                        <li class="paginate_button page-item">{{ $users->links() }}</li>
                                    </ul>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $("body").on("click", ".updateNotify", (function () {
            var t = this;

            Swal.fire({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Yes, deactive it!"
            }).then((function (e) {
                if (e.value) {
                    var r = $(t).attr("data-id");
                    $(t).parents("form:first").serialize();
                    var urlw = '{{route("admin.pushnotificationDelete")}}';
                    $.ajax({
                        url: urlw, type: "post", data: {id: r}, success: function (t) {
                            t.status ? (toastr.success(t.msg, "Success"), setTimeout((function () {
                                location.reload()
                            }), 3e3)) : toastr.error(t.msg, "Success")
                        }
                    })
                }
            }))
        }));

        
    </script>

@endsection
