@extends('admin.layouts.app')
@section('breadcrumbs')
@include('includes.breadcrumb', ['breadcrumbs' => [
'Listing' => route('admin.category'),
'Add New'
]])
@endsection
@section('content')
<section class="page-content container">
	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<form method="post" action="{{ route('admin.Post_pushnotificationAdd') }}" enctype="multipart/form-data" autocomplete="off">
						@csrf
						<div class="row">
							
							<!--div class="col-lg-6">
								<div class="form-group use-type">
								 <label>By User Type*</label>
									<select  class="form-control" name="user_type" style="color: #a0a2a5;" id="user_type">
										<option value="">--Select user--</option>	
										<option value="user" > All User</option> 
										<option value="custom_" >Custom Communication</option>  

									</select>

								</div>
							</div-->
							<div class="col-md-6">
								<div class="form-group">
									<label>Title*</label>
									<input type="text" name="title" class="form-control" placeholder="Title" value="{{ old('title') }}">
									
									@error('title')
									<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Description*</label>
									<textarea type="text" name="description" class="form-control" placeholder="description" rows="10" cols="50">{{ old('description') }}</textarea>
									
									@error('description')
										<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
							</div>
						</div>
						
						<div class="card-footer row">
							<div class="input submit">
								<div class="submit">
									<input class="btn btn-primary" type="submit" value="Save">
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>



@endsection