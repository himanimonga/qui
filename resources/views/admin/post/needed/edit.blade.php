@extends('admin.layouts.app')
	@section('breadcrumbs')
		@include('includes.breadcrumb', ['breadcrumbs' => [
		    					'Listing' => route('admin.needed'),
		    					 $post->title
						]])
	@endsection
@section('content')
<section class="page-content container">
	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<form method="post" action="{{ route('admin.postUpdate', $post->id) }}" autocomplete="off" enctype="multipart/form-data">
					  @csrf
					  @method('PUT')
						<div class="row">
						<input type="hidden" name="page_name" class="form-control"value="needed">
							<div class="col-md-6">
								<div class="form-group">
									<label>Title*</label>
									<input type="text" name="tittle" class="form-control" placeholder="Title" value="{{ old('tittle', $post->tittle) }}">
									
									@error('tittle')
										<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Description*</label>
									<textarea type="text" name="description" class="form-control" placeholder="Description" rows="5" cols="10">{{ old('description', $post->description) }}</textarea>
									
									@error('post')
										<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>How much would you like to play?</label>
									<select class="form-control" name="needed_price">
										<option value= 10 <?php if($post->needed_price == 10) { echo "selected"; }?>>$10</option>
										<option value=5/hr <?php if($post->needed_price == '5/hr') { echo "selected"; }?>>$5/hr</option>
										<option value=Free <?php if($post->needed_price == 'Free') { echo "selected"; }?>>Free</option>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Image*</label>
									<input type="file" name="image" class="form-control">
									
									@error('image')
										<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
							</div>
						</div>
							<div class="row">
							<div class="col-md-6">
							<?php if(!empty($post->image)){ ?>
								<img src="{{ url('/upload/image/'.$post->image) }}" height="150" width="150">
							<?php } ?>
							</div>
						</div>
						<div class="card-footer row">
							<div class="input submit">
								<div class="submit">
									<input class="btn btn-primary" type="submit" value="Update">
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection