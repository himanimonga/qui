@extends('admin.layouts.app')
@section('breadcrumbs')
@include('includes.breadcrumb', ['breadcrumbs' => [

'Listing'
]])
@endsection
@section('content')

<section class="page-content container-fluid">
	<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-body">
				<div class="card-header">
				<div class="row">
				<div class="col-lg-6">	
					@php  $seg=Request::segment(2); @endphp
					@if($seg == 'help')
				<form action="{{ route('admin.help') }}" method="GET">
					@else
					<form action="{{ route('admin.newest','Help') }}" method="GET">
					@endif
					<div class="form-group">
						<div class="input-group mb-3">
								<input type="text" class="form-control" placeholder="Search by name" name="search" value="{{ Request::get('search') }}">
								<div class="input-group-append">
									<input type="submit" class="form-control" value="Search">
								</div>
						</div>
					</div>
				</form>
				</div>
                <div class="col-lg-6">	
				<div class="dropdown pull-right">
					  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					    Sorting
					  </button>
					  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
					  	<a class="dropdown-item" href="https://onlineprojectprogress.com/laravel/qui/help">All</a>
					    <a class="dropdown-item" href="https://onlineprojectprogress.com/laravel/qui/newest/Help">Newest</a>
					    <a class="dropdown-item" href="https://onlineprojectprogress.com/laravel/qui/popular/Help">Popular</a>
					    <a class="dropdown-item" href="https://onlineprojectprogress.com/laravel/qui/recommend_more/News/Help">recommend</a>
					  </div>
                </div>
				</div>

				</div>
				</div>
				</div>


				<div class="table-responsive">
					<table class="table table-striped">
					<thead>
						<tr>
							<th><strong>#</strong></th>
							<!--<th><strong>@sortablelink('post','Post')</strong></th>-->
							<th><strong>@sortablelink('tittle','Title')</strong></th>
							<!--th><strong>@sortablelink('description','Description')</strong></th-->
							<th><strong>@sortablelink('user_name','User Name')</strong></th>

							<th><strong>Image</strong></th>
							<th><strong>@sortablelink('created_at','Date')</strong></th>
							<th style="width: 119px;"><strong>Geofencing Configurations</strong></th>
							<th><strong>Track</strong></th>
							<th><strong>Status</strong></th>
							<th><strong>Actions</strong></th>
						</tr>
					</thead>

						<tbody>
							@if(!$users->isEmpty())
							
							@php $i=1;@endphp
							@foreach($users as $key => $value)
							@if($value['post'] == 'News' && $value['post_category'] == 'help')
							<tr>
								<td>{{$i}}</td>
								<!--<td>{{$value['post']}}</td>-->
								<td>{{$value['tittle']}}</td>
						<!--td style="
								height: 120px; width:150px;
								">{{ substr($value['description'],-100)}}</td-->
								<td><a href="{{ route('admin.post_userDetails_news',$value['user_id']) }}">{{$value['user_name']}}</a></td>
								<td>@if(!empty($value['image']))<img src="{{asset('/upload/image/'.$value['image'])}}" style="
								height: 100px; width:100px;
								">@endif</td>
								<td>@php $date=strtotime($value['updated_at']); @endphp {{ date('d-M-Y',$date)}}</td>
								<td><a href="{{ route('admin.GeofencingConfigurations',$value['id']) }}"><i  class="zmdi zmdi-my-location zmdi-hc-fw font-size-20"></i></a></td>
								
								<td>
									
									@if(in_array($value['id'],$main))
									<a href="{{ route('admin.track',$value['id']) }}">
											<i class="zmdi zmdi-8tracks zmdi-hc-fw font-size-20"></i>	
										</a>
										@else
										<a href="#">
											<i class="zmdi zmdi-block zmdi-hc-fw font-size-20"></i>	
										</a>
										@endif
								</td>
								<td>

								@if($value['status'] == 0)
											<a data-status='0' data-toggle='tooltip' data-placement='top' data-original-title='Active' href="javascript:void(0)" data-id="{{$value['id']}}" class="updateStatus">
												<i  class="zmdi zmdi-shield-check zmdi-hc-fw font-size-20"></i>
											</a>
										@else
											<a data-status='1' data-toggle='tooltip' data-placement='top' data-original-title='Dective' href="javascript:void(0)" data-id="{{$value['id']}}" class="updateStatus_active">
												<i  class="zmdi zmdi-close-circle zmdi-hc-fw font-size-20"></i>
											</a>
										@endif

										@if($value['recommend'] == 0)
											<a data-status='0' data-toggle='tooltip' data-placement='top' data-original-title='Recommended' href="javascript:void(0)" data-id="{{$value['id']}}" class="recommendStatus">
												<i  class="zmdi zmdi-flower-alt  zmdi-hc-fw font-size-20"></i>
											</a>
										@else
											<a data-status='1' data-toggle='tooltip' data-placement='top' data-original-title='Recommended reject' href="javascript:void(0)" data-id="{{$value['id']}}" class="recommendStatus_active">
												<i  class="zmdi zmdi-close-circle zmdi-hc-fw font-size-20"></i>
											</a>
										@endif
								</td><td>
									<a href="{{ route('admin.Postview_help',$value['id']) }}">
											<i class="zmdi zmdi-eye zmdi-hc-fw font-size-20"></i>	
										</a>
									<a href="{{ route('admin.editPost_help',$value['id']) }}">
												<i class="zmdi zmdi-edit zmdi-hc-fw font-size-16"></i>
											</a>
										<a data-status='1' data-toggle='tooltip' data-placement='top' data-original-title='Delete' href="javascript:void(0)" data-id="{{$value['id']}}" class="disableStatus">
												<i class="zmdi zmdi-delete zmdi-hc-fw font-size-20"></i>
											</a>
								</td>

							</tr>
							
							@endif
							@php $i++; @endphp
							@endforeach
                             
							
							
							@else
							<tr>
								<td colspan="7" class="text-center">No record found</td>
							</tr>
							@endif
						</tbody>
					</table>
				@if(!empty($users->links()))
				<div class="pull-right">
				<div class="dataTables_paginate paging_simple_numbers" id="bs4-table_paginate">
				<ul class="pagination">
					<li class="paginate_button page-item">{{ $users->links() }}</li>
				</ul>
				</div>
				</div>
				@endif
				</div>
			</div>
		</div>
	</div>
	</div>
</section>



<script>
	$("body").on("click",".recommendStatus",(function(){
	var t=this;

	Swal.fire({title:"Are you sure?",text:"You won't be able to revert this!",type:"warning",showCancelButton:!0,confirmButtonColor:"#3085d6",cancelButtonColor:"#d33",confirmButtonText:"Yes, recommend reject it!"}).then((function(e){if(e.value){var r=$(t).attr("data-id");
	$(t).parents("form:first").serialize();
	var urlw = '{{route("admin.recommendChange")}}';
	$.ajax({url:urlw,type:"post",data:{id:r},success:function(t){
	t.status?(toastr.success(t.msg,"Success"),setTimeout((function(){location.reload()}),3e3)):toastr.error(t.msg,"Success")}
	})}}))}));

$("body").on("click",".recommendStatus_active",(function(){
	var t=this;

	Swal.fire({title:"Are you sure?",text:"You won't be able to revert this!",type:"warning",showCancelButton:!0,confirmButtonColor:"#3085d6",cancelButtonColor:"#d33",confirmButtonText:"Yes, recommend it!"}).then((function(e){if(e.value){var r=$(t).attr("data-id");
	$(t).parents("form:first").serialize();
	var urlw = '{{route("admin.recommendChange")}}';
	$.ajax({url:urlw,type:"post",data:{id:r},success:function(t){
	t.status?(toastr.success(t.msg,"Success"),setTimeout((function(){location.reload()}),3e3)):toastr.error(t.msg,"Success")}
	})}}))}));
$("body").on("click",".updateStatus",(function(){
	var t=this;

	Swal.fire({title:"Are you sure?",text:"You won't be able to revert this!",type:"warning",showCancelButton:!0,confirmButtonColor:"#3085d6",cancelButtonColor:"#d33",confirmButtonText:"Yes, deactive it!"}).then((function(e){if(e.value){var r=$(t).attr("data-id");
	$(t).parents("form:first").serialize();
	var urlw = '{{route("admin.postChange_status")}}';
	$.ajax({url:urlw,type:"post",data:{id:r},success:function(t){
	t.status?(toastr.success(t.msg,"Success"),setTimeout((function(){location.reload()}),3e3)):toastr.error(t.msg,"Success")}
	})}}))}));

$("body").on("click",".updateStatus_active",(function(){
	var t=this;

	Swal.fire({title:"Are you sure?",text:"You won't be able to revert this!",type:"warning",showCancelButton:!0,confirmButtonColor:"#3085d6",cancelButtonColor:"#d33",confirmButtonText:"Yes, active it!"}).then((function(e){if(e.value){var r=$(t).attr("data-id");
	$(t).parents("form:first").serialize();
	var urlw = '{{route("admin.postChange_status")}}';
	$.ajax({url:urlw,type:"post",data:{id:r},success:function(t){
	t.status?(toastr.success(t.msg,"Success"),setTimeout((function(){location.reload()}),3e3)):toastr.error(t.msg,"Success")}
	})}}))}));


$("body").on("click",".disableStatus",(function(){
	var t=this;

	Swal.fire({title:"Are you sure?",text:"You won't be able to revert this!",type:"warning",showCancelButton:!0,confirmButtonColor:"#3085d6",cancelButtonColor:"#d33",confirmButtonText:"Yes, delete it!"}).then((function(e){if(e.value){var r=$(t).attr("data-id");
	$(t).parents("form:first").serialize();
	var urlw = '{{route("admin.postDelete")}}';
	$.ajax({url:urlw,type:"post",data:{id:r},success:function(t){
	t.status?(toastr.success(t.msg,"Success"),setTimeout((function(){location.reload()}),3e3)):toastr.error(t.msg,"Success")}
	})}}))}));
</script>

@endsection