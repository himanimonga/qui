@extends('admin.layouts.app')
	@section('breadcrumbs')
		@include('includes.breadcrumb', ['breadcrumbs' => [
		    					'Listing' => route('admin.offered'),
		    					 $post->title
						]])
	@endsection
@section('content')
<section class="page-content container">
	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<form method="post" action="{{ route('admin.postUpdate', $post->id) }}" autocomplete="off" enctype="multipart/form-data">
					  @csrf
					  @method('PUT')
						<div class="row">
						<input type="hidden" name="page_name" class="form-control"value="offered">
							<div class="col-md-6">
								<div class="form-group">
									<label>Title*</label>
									<input type="text" name="tittle" class="form-control" placeholder="Title" value="{{ old('tittle', $post->tittle) }}">
									
									@error('tittle')
										<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Type</label>
									<select class="form-control price_type" name="price_type">
										<option value= 0 {{ old('price_type', $post->price_type) == 0 ? 'selected' : '' }} data-id=0>Paid</option>
										<option value=1 {{ old('price_type', $post->price_type) == 1 ? 'selected' : '' }}   data-id=1>Free</option>
									</select>
								</div>
							</div>
							<div class="col-md-6 slides"  >
								<div class="form-group">
									<label>Price</label>
									<input type="text" name="price" class="form-control" placeholder="Price" value="{{ old('price', $post->price) }}">
									
									@error('price')
										<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="form-group">
									<label>Description*</label>
									<textarea type="text" name="description" class="form-control" placeholder="Description" rows="5" cols="10">{{ old('description', $post->description) }}</textarea>
									
									@error('description')
										<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Image*</label>
									<input type="file" name="image" class="form-control">
									
									@error('image')
										<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
							</div>
						</div>
							<div class="row">
							<div class="col-md-6">
							<?php if(!empty($post->image)){ ?>
								<img src="{{ url('/upload/image/'.$post->image) }}" height="150" width="150">
							<?php } ?>
							</div>
						</div>
						<div class="card-footer row">
							<div class="input submit">
								<div class="submit">
									<input class="btn btn-primary" type="submit" value="Update">
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<script>

$(document).ready(function(){
	var a = $('.price_type').val();
	if(a == 1){
		$('.slides').hide();
	}
	if(a == 0){
		$('.slides').show();
	}
		$('body').on('change', '.price_type', function(){
			if($(this).find(':selected').attr('data-id') != 0){
				$('.slides').hide();
				var div = $(this).find(':selected').attr('data-id');
				$('#'+div).slideToggle();
			}
			if($(this).find(':selected').attr('data-id') != 1){
				$('.slides').show();
				var div = $(this).find(':selected').attr('data-id');
				$('#'+div).slideToggle();
			}
		});
		
		
	});
</script>
<script>
@endsection