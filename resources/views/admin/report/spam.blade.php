@extends('admin.layouts.app')
	@section('breadcrumbs')
		@include('includes.breadcrumb', ['breadcrumbs' => [

		    					'Listing'
						]])
	@endsection
@section('content')

<section class="page-content container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<div class="card-header">
					<div class="row">
									<div class="col-lg-6">
											<form action="{{ route('admin.spam') }}" method="GET">
												<div class="form-group">
													<div class="input-group mb-3">
															<input type="text" class="form-control" placeholder="Search by name" name="search" value="{{ Request::get('search') }}">
															<div class="input-group-append">
																<input type="submit" class="form-control" value="Search">
															</div>
													</div>
												</div>
											</form>
									</div>


					</div>
					</div>
					</div>


					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
								    <th><strong>Post</strong></th>
								    <!--th><strong>Message</strong></th-->
								    <th><strong>User Name</strong></th>
								    <th><strong>User Email</strong></th>
								     <th><strong>Date</strong></th>
								    <th><strong>Action</strong></th>

								</tr>
							</thead>
							<tbody>
								@if(!$users->isEmpty())
								@foreach($users as $key => $value)
								<tr>

									<td>{{$value->post_title}}</td>
									<!--td>{{$value->msg}}</td-->
									<td>{{$value->user_name}}</td>
									<td>{{$value->user_email}}</td>
									<td>@php $date=strtotime($value['created_at']); @endphp {{ date('d-M-Y',$date)}}</td>
                                    
                                    <td>
										@if($value['status'] == 0)
											<a data-status='0' data-toggle='tooltip' data-placement='top' data-original-title='Active' href="javascript:void(0)" data-id="{{$value['id']}}" class="updateStatus">
												<i  class="zmdi zmdi-shield-check zmdi-hc-fw font-size-20"></i>
											</a>
										@else
											<a data-status='1' data-toggle='tooltip' data-placement='top' data-original-title='Dective' href="javascript:void(0)" data-id="{{$value['id']}}" class="updateStatus_active">
												<i  class="zmdi zmdi-close-circle zmdi-hc-fw font-size-20"></i>
											</a>
										@endif
										<a href="{{ route('admin.Postview',$value['post_id']) }}">
											<i class="zmdi zmdi-eye zmdi-hc-fw font-size-20"></i>	
										</a>
										<a data-status='1' data-toggle='tooltip' data-placement='top' data-original-title='Delete' href="javascript:void(0)" data-id="{{$value['id']}}" class="disableStatus">
												<i class="zmdi zmdi-delete zmdi-hc-fw font-size-20"></i>
											</a>

									</td>

								</tr>

								@endforeach
								@else
									<tr><td colspan="6" class="text-center">No record found</td></tr>
								@endif
							</tbody>
						</table>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<script>




$("body").on("click",".disableStatus",(function(){
	var t=this;

	Swal.fire({title:"Are you sure?",text:"You won't be able to revert this!",type:"warning",showCancelButton:!0,confirmButtonColor:"#3085d6",cancelButtonColor:"#d33",confirmButtonText:"Yes, delete it!"}).then((function(e){if(e.value){var r=$(t).attr("data-id");
	$(t).parents("form:first").serialize();
	var urlw = '{{route("admin.reportDelete")}}';
	$.ajax({url:urlw,type:"post",data:{id:r},success:function(t){
	t.status?(toastr.success(t.msg,"Success"),setTimeout((function(){location.reload()}),3e3)):toastr.error(t.msg,"Success")}
	})}}))}));



$("body").on("click",".updateStatus",(function(){
	var t=this;

	Swal.fire({title:"Are you sure?",text:"You won't be able to revert this!",type:"warning",showCancelButton:!0,confirmButtonColor:"#3085d6",cancelButtonColor:"#d33",confirmButtonText:"Yes, deactive it!"}).then((function(e){if(e.value){var r=$(t).attr("data-id");
	$(t).parents("form:first").serialize();
	var urlw = '{{route("admin.reportChange_status")}}';
	$.ajax({url:urlw,type:"post",data:{id:r},success:function(t){
	t.status?(toastr.success(t.msg,"Success"),setTimeout((function(){location.reload()}),3e3)):toastr.error(t.msg,"Success")}
	})}}))}));

$("body").on("click",".updateStatus_active",(function(){
	var t=this;

	Swal.fire({title:"Are you sure?",text:"You won't be able to revert this!",type:"warning",showCancelButton:!0,confirmButtonColor:"#3085d6",cancelButtonColor:"#d33",confirmButtonText:"Yes, active it!"}).then((function(e){if(e.value){var r=$(t).attr("data-id");
	$(t).parents("form:first").serialize();
	var urlw = '{{route("admin.reportChange_status")}}';
	$.ajax({url:urlw,type:"post",data:{id:r},success:function(t){
	t.status?(toastr.success(t.msg,"Success"),setTimeout((function(){location.reload()}),3e3)):toastr.error(t.msg,"Success")}
	})}}))}));
</script>


@endsection
