@extends('admin.layouts.app')
	@section('breadcrumbs')
		@include('includes.breadcrumb', ['breadcrumbs' => [
		                    
		    					'Listing'
						]])
	@endsection
@section('content')



<section class="page-content container-fluid">

	<div class="row">

		<div class="col-lg-12">

			<div class="card">
                     <div class="card-header">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form action="{{ route('admin.support') }}" method="GET">
                                        <div class="form-group">
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" placeholder="Search by name"
                                                       name="search" value="{{ Request::get('search') }}">
                                                <div class="input-group-append">
                                                    <input type="submit" class="form-control" value="Search">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                               
                            </div>
                        </div>
                   <div class="table-responsive">

            <table class="table table-striped">

              <thead>

                <tr>

                  <th><strong><b>Name</b></strong></th>
                  <th><strong><b>Email</b></strong></th>
                  <th><strong><b>Date</b></strong></th>
                 
                  <th><strong><b>Action</b></strong></th>

                </tr>

              </thead>

              <tbody>
                             @if(!$users->isEmpty())
                

                @foreach($users as $key => $value)
                               
                <tr>

                  <td>{{$value->client_name}}</td>
                  <td>{{$value->client_email}}</td>
                 
                <td>@php $date=strtotime($value['created_at']); @endphp {{ date('d-M-Y',$date)}}</td>
                 
                  <td>
                    <a href="{{ route('admin.support_chat',$value->id) }}">

                      <i class="zmdi zmdi-twitch zmdi-hc-fw font-size-16"></i>  

                    </a>

                    

                  </td>
                  
                                     

                </tr>

                @endforeach

                @else

                  <tr><td colspan="5" class="text-center">No record found</td></tr>

                @endif

              </tbody>

            </table>
                     @if(!empty($users->links()))

                <div class="pull-right">

                    <div class="dataTables_paginate paging_simple_numbers" id="bs4-table_paginate">

                      <ul class="pagination">

                        <li class="paginate_button page-item">{{ $users->links() }}</li>

                      </ul>

                    </div>

                </div>

                @endif
            
          </div>
				
			</div>

		</div>

	</div>

</section>



@endsection