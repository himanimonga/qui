@extends('admin.layouts.app')
	@section('breadcrumbs')
		@include('includes.breadcrumb', ['breadcrumbs' => [
		                    
		    					'Listing'
						]])
	@endsection
@section('content')



<section class="page-content container-fluid">

	<div class="row">

		<div class="col-lg-12">

			<div class="card">

				 <div class="card-body">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form action="{{ route('admin.chat') }}" method="GET">
                                        <div class="form-group">
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" placeholder="Search by name"
                                                       name="search" value="{{ Request::get('search') }}">
                                                <div class="input-group-append">
                                                    <input type="submit" class="form-control" value="Search">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                   <div class="table-responsive">

						<table class="table table-striped">

							<thead>

								<tr>

									<th><strong><b>Post Name</b></strong></th>
									<th><strong><b>Chat type</strong></th>
									<!--th><strong><b>Post description</b></strong></th-->
                                    <!--th><strong><b>Join Mambers</b></strong></th-->
									<!-- <th><strong>@sortablelink('title','Page Title')</strong></th> -->

									<th><strong><b>Chat</b></strong></th>

								</tr>

							</thead>

							<tbody>
                              @if(!$users->isEmpty())
							
								@foreach($users as $key => $value)

								<tr>
                                    
									<td>{{$value->tittle}}</td>
									<td>{{$value->type}}</td>
									
                                      <td>

										<a href="{{ route('admin.chatList',$value->id) }}">

											<i class="zmdi zmdi-twitch zmdi-hc-fw font-size-16"></i>	

										</a>

									</td>

								</tr>

								@endforeach

								@else

									<tr><td colspan="4" class="text-center">No record found</td></tr>

								@endif

							</tbody>

						</table>

							@if(!empty($users->links()))

								<div class="pull-right">

										<div class="dataTables_paginate paging_simple_numbers" id="bs4-table_paginate">

											<ul class="pagination">

												<li class="paginate_button page-item">{{ $users->links() }}</li>

											</ul>

										</div>

								</div>

								@endif

					</div>

				
			</div>

		</div>

	</div>

</section>
<script src="https://cdn.pubnub.com/sdk/javascript/pubnub.4.27.4.min.js"></script>
<script>
	const pubnub = new PubNub({
    // replace the key placeholders with your own PubNub publish and subscribe keys
    publishKey: 'pub-c-bff41e09-ba9b-44da-9ed7-3effd3946ac1',
    subscribeKey: 'sub-c-351e97d4-f351-11ea-8db0-569464a6854f',
    uuid: "theClientUUID"
  });

	const file = await pubnub.downloadFile({
  channel: 'Channel-jhy9753y6',
  id: 'e201438d-e347-43d9-a5b0-901846bcfce6',
  name: 'Screenshot_2.png',
});

const myImageTag = document.createElement('img');
myImageTag.src = URL.createObjectURL(await file.toFile());
alert(myImageTag.src);
document.body.appendChild(myImageTag);
		
</script>


@endsection