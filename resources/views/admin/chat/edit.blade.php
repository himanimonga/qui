@extends('admin.layouts.app')
	@section('breadcrumbs')
		@include('includes.breadcrumb', ['breadcrumbs' => [
		                    
		    					'Cms Pages' => route('admin.cms'),
		    				'Edit New'
						]])
	@endsection
@section('content')


<section class="page-content container">

	<div class="row">

		<div class="col-lg-12">

			<div class="card">

				@if($page->page_uses == 'About Us')

				<div class="card-header">

						

	



<!--<a href="{{ route('admin.eventview')}}" class="button pllis">Event List</a>-->

</div>

@endif

				<div class="card-body">

					<form method="post" action="{{ route('admin.updateCms', $page->id) }}" enctype="multipart/form-data" autocomplete="off">

					  @csrf

					  @method('PUT')

						<div class="row">

							<div class="col-md-6">

								<div class="form-group">

									<label>Usage</label>

									<input type="text" name="page_uses" class="form-control" placeholder="Page Name" value="{{ old('page_uses', $page->page_uses) }}" readonly="readonly">

									

									@error('page_uses')

										<span class="text-danger font-size-14" role="alert">{{ $message }}</span>

									@enderror

								</div>

							</div>

							<div class="col-md-6">

								<div class="form-group">

									<label>Page Title*</label>

									<input type="text" name="title" class="form-control" placeholder="Page Title" value="{{ old('title', $page->title) }}">

									

									@error('title')

									<span class="text-danger font-size-14" role="alert">{{ $message }}</span>@enderror

								</div>

							</div>

						</div>

						<!--<div class="row">

							<div class="col-md-6">

								<div class="form-group">

									<label>Meta Title*</label>

									<input type="text" name="meta_title" class="form-control" placeholder="Meta Title" value="{{ old('meta_title', $page->meta_title) }}">

									

									@error('meta_title')

									<span class="text-danger font-size-14" role="alert">{{ $message }}</span>@enderror

								</div>

							</div>

							<div class="col-md-6">

								<div class="form-group">

									<label>Meta Description*</label>

									<textarea type="text" name="meta_description" class="form-control" placeholder="Meta Description" rows="5" cols="10">{{ old('meta_description', $page->meta_description) }}</textarea>

									

									@error('meta_description')

										<span class="text-danger font-size-14" role="alert">{{ $message }}</span>

									@enderror

								</div>

							</div>

							

						</div>-->

					



						<div class="row">

						@if(!empty($page->featureImage))

						

						<!--	<div class="col-md-6">

								<div class="form-group">

									<label>Banner Image</label>

									<input type="file" name="image" class="form-control" placeholder="Image" value="">

									<p>(Upload Min 1349 x 275 px and Max 2mb image)<p>

									@error('image')

										<span class="text-danger font-size-14" role="alert">{{ $message }}</span>

									@enderror

								</div>

							</div>

							

							<div class="col-md-6">

								<img src="{{ url('/uploads/cms/'.$page->featureImage) }}" height="150" width="150">

			.

							</div>-->

							@else

								

							<!--<div class="col-md-6">

								<div class="form-group">

									<label>Image Required</label>

									<input type="checkbox">

								</div>

							</div>-->

							

						<!--	<div class="col-md-6" id ="image">

								<div class="form-group">

									<label>Banner Image</label>

									<input type="file" name="image" class="form-control" placeholder="Image" value="">

									@error('image')

										<span class="text-danger font-size-14" role="alert">{{ $message }}</span>

									@enderror

								</div>

							</div>-->

							

							@endif

							@php $banner = explode(" ",$page->page_name);

							$last_word = array_pop($banner);

							@endphp

							@if($last_word == "Banner")

								@else

							<div class="col-md-12">
<div class="form-group">
									<label>Page Content*</label>
									<textarea type="text" name="content" class="form-control" placeholder="Page Content" rows="10" cols="50" id="troll">{{ old('content', $page->content) }}</textarea>
									
									@error('content')
										<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
								

							</div>

						</div>

						<!--<div class="row">

							<div class="col-md-4">

								<div class="form-group">

									<label>Status</label>

									<select class="form-control" name="status">

										<option value="1" <?php if($page->status == 1) { echo "selected"; }?>>Enable</option>

										<option value="0" <?php if($page->status == 0) { echo "selected"; }?>>Disable</option>

									</select>

								</div>

							</div>

							</div>-->

						@endif

						<div class="row">

							<!--<div class="col-md-6">

								<div class="form-group">

									<label>Featured Image*</label>

									<input type="file" name="featureImage" class="form-control">

									

									@error('featureImage')

										<span class="text-danger font-size-14" role="alert">{{ $message }}</span>

									@enderror

								</div>

							</div>-->

							

						</div>

						

						<div class="card-footer row">

							<div class="input submit">

								<div class="submit">

									<input class="btn btn-primary" type="submit" value="Save">

								</div>

							</div>

						</div>

					</form>

				</div>

			</div>

		</div>

	</div>

</section>
  <script>
   CKEDITOR.replace('content', {
	    allowedContent: true,
		filebrowserUploadUrl: "{{route('admin.ckeditor.upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'content',
		fillEmptyBlocks: false,
		protectedSource:[/<i[a-z0-9\=\'\"_\- ]*><\/i>/g],
	extraPlugins:'colorbutton',
		
    });
	
	 
   </script>
   <script>

   /*CKEDITOR.replace('content', {

	    allowedContent: true,

		filebrowserUploadUrl: "{{route('admin.ckeditor.upload', ['_token' => csrf_token() ])}}",

        filebrowserUploadMethod: 'content',

		fillEmptyBlocks: false,

		protectedSource:[/<i[a-z0-9\=\'\"_\- ]*><\/i>/g],

	extraPlugins:'colorbutton',

		

    });*/

	

	 

   $( document ).ready(function() {

    $("#image").hide();

	  $('input[type="checkbox"]').click(function(){

            if($(this).prop("checked") == true){

              $("#image").show();

			}

            else if($(this).prop("checked") == false){

                 $("#image").hide();

            }

        });

    });	

   </script>

@endsection