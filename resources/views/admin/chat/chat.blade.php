@extends('admin.layouts.app')
	@section('breadcrumbs')
		@include('includes.breadcrumb', ['breadcrumbs' => [
		                    
		    					'Listing'
						]])
	@endsection
@section('content')
<style>
  s {
      display: none;
}

body {font-family:Arial;}
#chatBox {width:1000px;height:400px;border: 0px solid;overflow:scroll}
#loader {display:none;}
.messages {min-height:40px;border-bottom:1px solid #1f1f1f;}
.date {font-size:9px;color:#1f1f1f;}

.chat-container {
  height: 100px;
  overflow: auto;
  display: flex;
  flex-direction: column-reverse;
}

</style>



<section class="page-content container-fluid">

	<div class="row">

		<div class="col-lg-12">

			<div class="card">

				 <div class="card-body">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-lg-6">
                                    <h3 class="crd-head">Chat</h3>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                  
             
          <!--div class="scroll-date"><p></p></div-->
					<div class="chat-container" id="chatBox" >
            <div class="type-box" id="scroll">
              <input type="text" placeholder="Type here" id="update-text" class="new-emoji" name="emoji" autofocus style="width: 801px;">
              <div class="attachments">
                <button class="attach-item">
                  <input type="file" id="file">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="currentColor" d="M1.816 15.556v.002c0 1.502.584 2.912 1.646 3.972s2.472 1.647 3.974 1.647a5.58 5.58 0 0 0 3.972-1.645l9.547-9.548c.769-.768 1.147-1.767 1.058-2.817-.079-.968-.548-1.927-1.319-2.698-1.594-1.592-4.068-1.711-5.517-.262l-7.916 7.915c-.881.881-.792 2.25.214 3.261.959.958 2.423 1.053 3.263.215l5.511-5.512c.28-.28.267-.722.053-.936l-.244-.244c-.191-.191-.567-.349-.957.04l-5.506 5.506c-.18.18-.635.127-.976-.214-.098-.097-.576-.613-.213-.973l7.915-7.917c.818-.817 2.267-.699 3.23.262.5.501.802 1.1.849 1.685.051.573-.156 1.111-.589 1.543l-9.547 9.549a3.97 3.97 0 0 1-2.829 1.171 3.975 3.975 0 0 1-2.83-1.173 3.973 3.973 0 0 1-1.172-2.828c0-1.071.415-2.076 1.172-2.83l7.209-7.211c.157-.157.264-.579.028-.814L11.5 4.36a.572.572 0 0 0-.834.018l-7.205 7.207a5.577 5.577 0 0 0-1.645 3.971z"></path></svg>
                </button>
                <!--button class="attach-item">
                <input type="file">
                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                width="36.174px" height="36.174px" viewBox="0 0 36.174 36.174" style="enable-background:new 0 0 36.174 36.174;"
                xml:space="preserve">
                  <g>
                    <path d="M23.921,20.528c0,3.217-2.617,5.834-5.834,5.834s-5.833-2.617-5.833-5.834s2.616-5.834,5.833-5.834
                      S23.921,17.312,23.921,20.528z M36.174,12.244v16.57c0,2.209-1.791,4-4,4H4c-2.209,0-4-1.791-4-4v-16.57c0-2.209,1.791-4,4-4h4.92
                      V6.86c0-1.933,1.566-3.5,3.5-3.5h11.334c1.934,0,3.5,1.567,3.5,3.5v1.383h4.92C34.383,8.244,36.174,10.035,36.174,12.244z
                      M26.921,20.528c0-4.871-3.963-8.834-8.834-8.834c-4.87,0-8.833,3.963-8.833,8.834s3.963,8.834,8.833,8.834
                      C22.958,29.362,26.921,25.399,26.921,20.528z"/>
                  </g>
                </svg>
                </button-->
              </div>
            <button id="publish-button" type="submit" class="send-btn">Send</button>
            </div>
            <div id="dom">
			
             <div class="chat-item cht-left" id="messages-topA">
                </div>
             <div class="chat-item cht-right" id="messages-top">
              </div>
			 
              </div>  
					
					 <div class='inner'> </div>
                    <!-- WHERE YOU WILL LOAD CONTENT-->
                    
        


             
             <!-- chat item end -->

						   <!-- type box -->
						
						<!-- type box end -->
					
          </div>
				
			</div>

		</div>

	</div>



	

</section>
<script>
      function notifyMe(message) {
		   var cha= '<?php echo $data[0]; ?>_notify'; 
      if (message == undefined) {
        message = "Hi there! You clicked a button.";
      };
      if (!("Notification" in window)) {
          alert("This browser does not support desktop notification");
        }
      else if (Notification.permission === "granted") {
          //var notification = new Notification(message);
          var notification = new Notification(message.text, {
    //body: 'From: ' + message.text,
    body:  message.description,
    tag: cha,
    icon: 'https://onlineprojectprogress.com/laravel/qui/admin/img/logo/qui.jpg',
    //url: 'https://onlineprojectprogress.com/laravel/qui/dashboard'
  });
  notification.onshow = function() {
    setTimeout(notification.close, 30000);
  };
        }
      else if (Notification.permission !== 'denied') {
          Notification.requestPermission(function (permission) {
            if (permission === "granted") {
              var notification = new Notification("Hi there!");
            }
          });
        }
      }
    

      $(document).ready(function() {
         //alert(document.location.protocol);   
          var cha= '<?php echo $data[0]; ?>_notify'; 
		  console.log(cha);
    var pubnub = PUBNUB({
            subscribe_key: 'sub-c-bd11da7c-c6ef-11ea-8c8c-4a3a4f8d7ece',
      publish_key: 'pub-c-142aecf3-b02c-4aef-963e-5aa45f2c8814',
      ssl : (('https:' == document.location.protocol) ? true : false)
        });
    pubnub.subscribe({
            channel :cha, // Subscribing to PubNub's channel
            message : function(message){
             // console.log(message);
        var image = "https://onlineprojectprogress.com/laravel/qui/admin/img/logo/qui.jpg";
              var data = message.update 
              var data1 = message.url 
              notifyMe(message);
               //notifyMe(message.url);
            }
        })
    
    });
  </script>
  <script src="https://cdn.pubnub.com/sdk/javascript/pubnub.4.29.6.js"></script>
    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
      </script>
<script>


function formatAMPM(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return strTime;
}

  
  var cha= '<?php echo $data[0]; ?>';
   const updateText123 = document.getElementById('update-text');
  $( document ).ready(function() {
      $("body").on("click",".send-btn",(function(){
    
      var urlw = '{{route("admin.fileSave")}}';
    var fileData  = $('#file').prop('files')[0];
    //console.log(fileData);
    
      
     if(fileData == 'undefined' ){
 var formData = new FormData();
formData.append('file', fileData);
      $.ajax({url:urlw,dataType: 'JSON', cache: false,contentType: false,processData: false,type:"post",data:formData,
        
success:function(response){
   
     pubnub.publish({
      channel : theChannel,
      message : {'entry' : 'Admin','update':updateText123.vlaue,'type':'support','createdAt':d,'image' : response.message,}
     // file: file,
    });

 },
    error:function(e){
        console.log("error");
        console.log(e);
    },
  })
  document.getElementById("file").value = ''
  document.getElementById("update-text").value = ''
  }}));
     

  pubnub.history(
    {
        channel: cha,
        reverse: true,
       stringifiedTimeToken: true,

       //count: 10, // false is the default
    },
    function (status, response) {
        if (status.error) {
        console.log(status)
      }
      else {
         
        window.onload = function() {

  var input = document.getElementById("update-text").focus();
}
         var text = "";
         var i;
          for (i = response.messages.length - 1; i >= 0;  i--) { 
            var current = new Date();
           var previous = response.messages[i].entry.createdAt;
         // $(".scroll-date").html(previous);
            //for (i = 0; i < 20;  i++) {
           // text += response.messages[i].entry.entry + "<br>";
            var date = new Date(response.messages[i].entry.createdAt);
           var d=(((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '-' + ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())));
          //var date1 = new Date(response.messages[i].entry.message.createdAt);
          // var d1=(((date1.getMonth() > 8) ? (date1.getMonth() + 1) : ('0' + (date1.getMonth() + 1))) + '-' + ((date1.getDate() > 9) ? date1.getDate() : ('0' + date1.getDate())));
        
         
           if(response.messages[i].entry.type == 'user'){

           // alert(event.message.update);
             if(response.messages[i].entry.image != undefined){
              
                displayMessageA('',$('.inner').prepend('<div class="chat-item cht-left" id="messages-topA"><img src="'+response.messages[i].entry.user_image+'" alt=""><div class="chat-txt"><div class="cht-head"><h4>'+response.messages[i].entry.user_name+'</h4><p class="cht-time">'+d+' '+formatAMPM(new Date(previous))+'</p></div><p><a href="'+response.messages[i].entry.image+'" target="_blank"><img src="'+response.messages[i].entry.image+'"></a></p></div></div>'));
             }else{
            
           if(response.messages[i].entry.file != undefined){
         if(response.messages[i].entry.file_type == 'video/mp4'){
          //console.log(response.messages[i].entry.file_type);
           displayMessageA('',$('.inner').prepend('<div class="chat-item cht-left" id="messages-topA"><img src="'+response.messages[i].entry.user_image+'" alt=""><div class="chat-txt"><div class="cht-head"><h4>'+response.messages[i].entry.user_name+'</h4><p class="cht-time">'+d+' '+formatAMPM(new Date(previous))+'</p></div><p><video width="320" height="240" controls><source src="'+response.messages[i].entry.file+'" type="video/mp4"></video></p></div></div>'));
         }else if(response.messages[i].entry.file_type == 'video/mkv'){
           displayMessageA('',$('.inner').prepend('<div class="chat-item cht-left" id="messages-topA"><img src="'+response.messages[i].entry.user_image+'" alt=""><div class="chat-txt"><div class="cht-head"><h4>'+response.messages[i].entry.user_name+'</h4><p class="cht-time">'+d+' '+formatAMPM(new Date(previous))+'</p></div><p><video width="320" height="240" controls><source src="'+response.messages[i].entry.file+'" type="video/mkv"></video></p></div></div>'));
         }else if(response.messages[i].entry.file_type == 'video/mov'){
           displayMessageA('',$('.inner').prepend('<div class="chat-item cht-left" id="messages-topA"><img src="'+response.messages[i].entry.user_image+'" alt=""><div class="chat-txt"><div class="cht-head"><h4>'+response.messages[i].entry.user_name+'</h4><p class="cht-time">'+d+' '+formatAMPM(new Date(previous))+'</p></div><p><video width="320" height="240" controls><source src="'+response.messages[i].entry.file+'" type="video/mov"></video></p></div></div>'));
         }else if(response.messages[i].entry.file_type == 'video/x-flv'){
           displayMessageA('',$('.inner').prepend('<div class="chat-item cht-left" id="messages-topA"><img src="'+response.messages[i].entry.user_image+'" alt=""><div class="chat-txt"><div class="cht-head"><h4>'+response.messages[i].entry.user_name+'</h4><p class="cht-time">'+d+' '+formatAMPM(new Date(previous))+'</p></div><p><video width="320" height="240" controls><source src="'+response.messages[i].entry.file+'" type="video/flv"></video></p></div></div>'));
         }else if(response.messages[i].entry.file_type == 'application/doc'){
           displayMessageA('',$('.inner').prepend('<div class="chat-item cht-left" id="messages-topA"><img src="'+response.messages[i].entry.user_image+'" alt=""><div class="chat-txt"><div class="cht-head"><h4>'+response.messages[i].entry.user_name+'</h4><p class="cht-time">'+d+' '+formatAMPM(new Date(previous))+'</p></div><p><a href="'+response.messages[i].entry+'" target="_blank" download>'+response.messages[i].entry.file_name+'</a></p></div></div>'));
         }else if(response.messages[i].entry.file_type == 'Docx'){
           displayMessageA('',$('.inner').prepend('<div class="chat-item cht-left" id="messages-topA"><img src="'+response.messages[i].entry.user_image+'" alt=""><div class="chat-txt"><div class="cht-head"><h4>'+response.messages[i].entry.user_name+'</h4><p class="cht-time">'+d+' '+formatAMPM(new Date(previous))+'</p></div><p><a href="'+response.messages[i].entry.file+'" target="_blank" download>'+response.messages[i].entry.file_name+'</a></p></div></div>'));
         }else if(response.messages[i].entry.file_type == 'application/xls'){
           displayMessageA('',$('.inner').prepend('<div class="chat-item cht-left" id="messages-topA"><img src="'+response.messages[i].entry.user_image+'" alt=""><div class="chat-txt"><div class="cht-head"><h4>'+response.messages[i].entry.user_name+'</h4><p class="cht-time">'+d+' '+formatAMPM(new Date(previous))+'</p></div><p><a href="'+response.messages[i].entry.file+'" target="_blank" download>'+response.messages[i].entry.file_name+'</a></p></div></div>'));
         }else if(response.messages[i].entry.file_type == 'application/xlsx'){
           displayMessageA('',$('.inner').prepend('<div class="chat-item cht-left" id="messages-topA"><img src="'+response.messages[i].entry.user_image+'" alt=""><div class="chat-txt"><div class="cht-head"><h4>'+response.messages[i].entry.user_name+'</h4><p class="cht-time">'+d+' '+formatAMPM(new Date(previous))+'</p></div><p><a href="'+response.messages[i].entry.file+'" target="_blank" download>'+response.messages[i].entry.file_name+'</a></p></div></div>'));
         }else if(response.messages[i].entry.file_type == 'application/html'){
           displayMessageA('',$('.inner').prepend('<div class="chat-item cht-left" id="messages-topA"><img src="'+response.messages[i].entry.user_image+'" alt=""><div class="chat-txt"><div class="cht-head"><h4>'+response.messages[i].entry.user_name+'</h4><p class="cht-time">'+d+' '+formatAMPM(new Date(previous))+'</p></div><p><a href="'+response.messages[i].entry.file+'" target="_blank" download>'+response.messages[i].entry.file_name+'</a></p></div></div>'));
         }else if(response.messages[i].entry.file_type == 'application/pdf'){
          
           displayMessageA('',$('.inner').prepend('<div class="chat-item cht-left" id="messages-topA"><img src="'+response.messages[i].entry.user_image+'" alt=""><div class="chat-txt"><div class="cht-head"><h4>'+response.messages[i].entry.user_name+'</h4><p class="cht-time">'+d+' '+formatAMPM(new Date(previous))+'</p></div><p><a href="'+response.messages[i].entry.file+'" target="_blank" download>'+response.messages[i].entry.file_name+'</a></p></div></div>'));
         }else if(response.messages[i].entry.file_type == 'application/msword'){
           displayMessageA('',$('.inner').prepend('<div class="chat-item cht-left" id="messages-topA"><img src="'+response.messages[i].entry.user_image+'" alt=""><div class="chat-txt"><div class="cht-head"><h4>'+response.messages[i].entry.user_name+'</h4><p class="cht-time">'+d+' '+formatAMPM(new Date(previous))+'</p></div><p><a href="'+response.messages[i].entry.file+'" target="_blank" download>'+response.messages[i].entry.file_name+'</a></p></div></div>'));
         }else if(response.messages[i].entry.file_type == 'audio'){
           displayMessageA('',$('.inner').prepend('<div class="chat-item cht-left" id="messages-topA"><img src="'+response.messages[i].entry.user_image+'" alt=""><div class="chat-txt"><div class="cht-head"><h4>'+response.messages[i].entry.user_name+'</h4><p class="cht-time">'+d+' '+formatAMPM(new Date(previous))+'</p></div><p><audio controls autoplay><source src="'+response.messages[i].entry.file+'" type="audio/aac"></audio></p></div></div>'));
         }
       }else{
        displayMessageA('',$('.inner').prepend('<div class="chat-item cht-left" id="messages-topA"><img src="'+response.messages[i].entry.user_image+'" alt=""><div class="chat-txt"><div class="cht-head"><h4>'+response.messages[i].entry.user_name+'</h4><p class="cht-time">'+d+' '+formatAMPM(new Date(previous))+'</p></div><p>'+response.messages[i].entry.update+'</p></div></div>'));
         
       }
          }
         }else{
          if (response.messages[i].entry.file != undefined) {
          	const getUrl = pubnub.getFileUrl({ channel: cha, id: response.messages[i].entry.file.id, name: response.messages[i].entry.file.name });
            var previous = response.messages[i].entry.message.createdAt;
            
            var fileName = response.messages[i].entry.file.name;
           var ext = fileName.split('.').pop();
          
           if(ext == 'jpg' || ext == 'jpeg' || ext == 'png'){ 
            displayMessage('',$('.inner').prepend('<div class="chat-item cht-right" id="messages-top"><img src="https://onlineprojectprogress.com/laravel/qui/admin/img/logo/qui.jpg" alt=""><div class="chat-txt"><div class="cht-head"><h4>Support</h4><p class="cht-time">'+formatAMPM(new Date(previous))+'</p></div><p><a href="'+getUrl+'" target="_blank"><img src="'+getUrl+'"></a>'+response.messages[i].entry.message.update+'</p></div></div>'));
        }else if(ext == 'mp4' || ext == 'mpv' || ext == 'MPEG'){
        	displayMessage('',$('.inner').prepend('<div class="chat-item cht-right" id="messages-top"><img src="https://onlineprojectprogress.com/laravel/qui/admin/img/logo/qui.jpg" alt=""><div class="chat-txt"><div class="cht-head"><h4>Support</h4><p class="cht-time">'+formatAMPM(new Date(previous))+'</p></div><p><video width="320" height="240" controls><source src="'+getUrl+'" type="video/'+ext+'"></video>'+response.messages[i].entry.message.update+'</p></div></div>'));
  
        }else if(ext == 'mp3' ){
			displayMessage('',$('.inner').prepend('<div class="chat-item cht-right" id="messages-top"><img src="https://onlineprojectprogress.com/laravel/qui/admin/img/logo/qui.jpg" alt=""><div class="chat-txt"><div class="cht-head"><h4>Support</h4><p class="cht-time">'+formatAMPM(new Date(previous))+'</p></div><p><audio controls><source src="'+getUrl+'" type="audio/'+ext+'"></audio>'+response.messages[i].entry.message.update+'</p></div></div>'));
      
		}else if(ext == 'pdf' || ext == 'docx' || ext == 'xls' || ext == 'xlsx'){
       
      displayMessage('',$('.inner').prepend('<div class="chat-item cht-right" id="messages-top"><img src="https://onlineprojectprogress.com/laravel/qui/admin/img/logo/qui.jpg" alt=""><div class="chat-txt"><div class="cht-head"><h4>Support</h4><p class="cht-time">'+formatAMPM(new Date(previous))+'</p></div><p><a href="'+getUrl+'" target="_blank" download>'+response.messages[i].entry.file.name+'<i class="la la-download"></i></a></p></div></div>'));
      
    }else{
        	displayMessage('',$('.inner').prepend('<div class="chat-item cht-right" id="messages-top"><img src="https://onlineprojectprogress.com/laravel/qui/admin/img/logo/qui.jpg" alt=""><div class="chat-txt"><div class="cht-head"><h4>Support</h4><p class="cht-time">'+formatAMPM(new Date(previous))+'</p></div><a href="'+getUrl+'" target="_blank" download>'+response.messages[i].entry.file.name+'<i class="la la-download"></i></a><p>'+response.messages[i].entry.message.update+'</p></div></div>'));
        }
          }else{
            displayMessage('',$('.inner').prepend('<div class="chat-item cht-right" id="messages-top"><img src="https://onlineprojectprogress.com/laravel/qui/admin/img/logo/qui.jpg" alt=""><div class="chat-txt"><div class="cht-head"><h4>Support</h4><p class="cht-time">'+d+' '+formatAMPM(new Date(previous))+'</p></div><p>'+response.messages[i].entry.update+'</p></div></div>'));
          }
 
     
       }
          
          }
         
       
      }
    }
);
  }); 

 const messagesTop = document.getElementById('messages-top');
  const messagesTopA = document.getElementById('messages-topA');
  const updateText = document.getElementById('update-text');
  const sendButton = document.getElementById('publish-button');
//console.log(updateText.value);
  
	  const theChannel = cha;
      
  
  const pubnub = new PubNub({
    // replace the key placeholders with your own PubNub publish and subscribe keys
    publishKey: 'pub-c-142aecf3-b02c-4aef-963e-5aa45f2c8814',
    subscribeKey: 'sub-c-bd11da7c-c6ef-11ea-8c8c-4a3a4f8d7ece',
    secretKey:'sec-c-NGMxYmFjNTMtMjFkNS00YzY0LTkyYzctZWNjZDUyMTY4ZmZk',
    ssl: true,
    uuid: "0"
  });

 
  pubnub.addListener({

	file: function(event) {
   
   var previous = event.message.createdAt;
		const getUrl = pubnub.getFileUrl({ channel: cha, id: event.file.id, name: event.file.name });
		var fileName = event.file.name;
           var ext = fileName.split('.').pop();
          
          
           if(ext == 'jpg' || ext == 'jpeg' || ext == 'png'){
		 displayMessage('',$('#dom').append('<div class="chat-item cht-right" id="messages-top"><img src="https://onlineprojectprogress.com/laravel/qui/admin/img/logo/qui.jpg" alt=""><div class="chat-txt"><div class="cht-head"><h4>Support</h4><p class="cht-time">'+formatAMPM(new Date(previous))+'</p></div><p><a href="'+event.message.update+'" target="_blank"><img src="'+getUrl+'">'+event.message.update+'</a></p></div></div>'));
		  }else if(ext == 'mp4' || ext == 'mpv' || ext == 'MPEG' || ext == 'webm'){
           displayMessage('',$('#dom').append('<div class="chat-item cht-right" id="messages-top"><img src="https://onlineprojectprogress.com/laravel/qui/admin/img/logo/qui.jpg" alt=""><div class="chat-txt"><div class="cht-head"><h4>Support</h4><p class="cht-time">'+formatAMPM(new Date(previous))+'</p></div><p><video width="320" height="240" controls><source src="'+getUrl+'" type="video/'+ext+'"></video>'+event.message.update+'</p></div></div>'));
  
		  }else if(ext == 'mp3' || ext == 'mpeg' ){
			displayMessage('',$('#dom').append('<div class="chat-item cht-right" id="messages-top"><img src="https://onlineprojectprogress.com/laravel/qui/admin/img/logo/qui.jpg" alt=""><div class="chat-txt"><div class="cht-head"><h4>Support</h4><p class="cht-time">'+formatAMPM(new Date(previous))+'</p></div><p><audio controls><source src="'+getUrl+'" type="audio/'+ext+'"></audio>'+event.message.updat+'</p></div></div>'));
      
		}else if(ext == 'pdf' || ext == 'doc' || ext == 'xls' || ext == 'docx' || ext == 'xlsx'){
      displayMessage('',$('#dom').append('<div class="chat-item cht-right" id="messages-top"><img src="https://onlineprojectprogress.com/laravel/qui/admin/img/logo/qui.jpg" alt=""><div class="chat-txt"><div class="cht-head"><h4>Support</h4><p class="cht-time">'+formatAMPM(new Date(previous))+'</p></div><p><a href="'+getUrl+'" target="_blank" download>'+event.file.name+'</a></p></div></div>'));
      
    }else{
        displayMessage('',$('#dom').append('<div class="chat-item cht-right" id="messages-top"><img src="https://onlineprojectprogress.com/laravel/qui/admin/img/logo/qui.jpg" alt=""><div class="chat-txt"><div class="cht-head"><h4>Support</h4><p class="cht-time">'+formatAMPM(new Date(previous))+'</p></div><p>'+event.message.update+'</p></div></div>'));
          
		  }
		
	},
    message: function(event) {
      console.log(event.message);
        
         var current = new Date();
           var previous = event.message.createdAt;
          console.log(event.message);
          if(event.message.type == 'user'){
             if(event.message.image != undefined){
			 console.log(event.message.image);
                displayMessageA('',$('#dom').append('<div class="chat-item cht-left" id="messages-topA"><img src="'+event.message.user_image+'" alt=""><div class="chat-txt"><div class="cht-head"><h4>'+event.message.user_name+'</h4><p class="cht-time">'+formatAMPM(new Date(previous))+'</p></div><p><a href="'+event.message.image+'"><img src="'+event.message.image+'" target="_blank"></a></p></div></div>'));
             }else{
			 if(event.message.file != undefined){
        console.log(event.message.file_type);
        if(event.message.file_type == 'video/mp4'){
           displayMessageA('',$('#dom').append('<div class="chat-item cht-left" id="messages-topA"><img src="'+event.message.user_image+'" alt=""><div class="chat-txt"><div class="cht-head"><h4>'+event.message.user_name+'</h4><p class="cht-time">'+formatAMPM(new Date(previous))+'</p></div><p><video width="320" height="240" controls><source src="'+event.message.file+'" type="video/mp4"></video></p></div></div>'));
         }else if(event.message.file_type == 'video/mkv'){
           displayMessageA('',$('#dom').append('<div class="chat-item cht-left" id="messages-topA"><img src="'+event.message.user_image+'" alt=""><div class="chat-txt"><div class="cht-head"><h4>'+event.message.user_name+'</h4><p class="cht-time">'+formatAMPM(new Date(previous))+'</p></div><p><video width="320" height="240" controls><source src="'+event.message.file+'" type="video/mkv"></video></p></div></div>'));
         }else if(event.message.file_type == 'video/mov'){
           displayMessageA('',$('#dom').append('<div class="chat-item cht-left" id="messages-topA"><img src="'+event.message.user_image+'" alt=""><div class="chat-txt"><div class="cht-head"><h4>'+event.message.user_name+'</h4><p class="cht-time">'+formatAMPM(new Date(previous))+'</p></div><p><video width="320" height="240" controls><source src="'+event.message.file+'" type="video/mov"></video></p></div></div>'));
         }else if(event.message.file_type == 'video/x-flv'){
           displayMessageA('',$('#dom').append('<div class="chat-item cht-left" id="messages-topA"><img src="'+event.message.user_image+'" alt=""><div class="chat-txt"><div class="cht-head"><h4>'+event.message.user_name+'</h4><p class="cht-time">'+formatAMPM(new Date(previous))+'</p></div><p><video width="320" height="240" controls><source src="'+event.message.file+'" type="video/flv"></video></p></div></div>'));
         }else if(event.message.file_type == 'application/doc'){
           displayMessageA('',$('#dom').append('<div class="chat-item cht-left" id="messages-topA"><img src="'+event.message.user_image+'" alt=""><div class="chat-txt"><div class="cht-head"><h4>'+event.message.user_name+'</h4><p class="cht-time">'+formatAMPM(new Date(previous))+'</p></div><p><a href="'+event.message.file+'" target="_blank" download>'+event.message.file_name+'</a></p></div></div>'));
         }else if(event.message.file_type == 'Docx'){
           displayMessageA('',$('#dom').append('<div class="chat-item cht-left" id="messages-topA"><img src="'+event.message.user_image+'" alt=""><div class="chat-txt"><div class="cht-head"><h4>'+event.message.user_name+'</h4><p class="cht-time">'+formatAMPM(new Date(previous))+'</p></div><p><a href="'+event.message.file+'" target="_blank" download>'+event.message.file_name+'</a></p></div></div>'));
         }else if(event.message.file_type == 'application/xls'){
           displayMessageA('',$('#dom').append('<div class="chat-item cht-left" id="messages-topA"><img src="'+event.message.user_image+'" alt=""><div class="chat-txt"><div class="cht-head"><h4>'+event.message.user_name+'</h4><p class="cht-time">'+formatAMPM(new Date(previous))+'</p></div><p><a href="'+event.message.file+'" target="_blank" download>'+event.message.file_name+'</a></p></div></div>'));
         }else if(event.message.file_type == 'application/xlsx'){
           displayMessageA('',$('#dom').append('<div class="chat-item cht-left" id="messages-topA"><img src="'+event.message.user_image+'" alt=""><div class="chat-txt"><div class="cht-head"><h4>'+event.message.user_name+'</h4><p class="cht-time">'+formatAMPM(new Date(previous))+'</p></div><p><a href="'+event.message.file+'" target="_blank" download>'+event.message.file_name+'</a></p></div></div>'));
         }else if(event.message.file_type == 'application/html'){
           displayMessageA('',$('#dom').append('<div class="chat-item cht-left" id="messages-topA"><img src="'+event.message.user_image+'" alt=""><div class="chat-txt"><div class="cht-head"><h4>'+event.message.user_name+'</h4><p class="cht-time">'+formatAMPM(new Date(previous))+'</p></div><p><a href="'+event.message.file+'" target="_blank" download>'+event.message.file_name+'</a></p></div></div>'));
         }else if(event.message.file_type == 'application/pdf'){
           displayMessageA('',$('#dom').append('<div class="chat-item cht-left" id="messages-topA"><img src="'+event.message.user_image+'" alt=""><div class="chat-txt"><div class="cht-head"><h4>'+event.message.user_name+'</h4><p class="cht-time">'+formatAMPM(new Date(previous))+'</p></div><p><a href="'+event.message.file+'" target="_blank" download>'+event.message.file_name+'</a></p></div></div>'));
         }else if(event.message.file_type == 'application/msword'){
           displayMessageA('',$('#dom').append('<div class="chat-item cht-left" id="messages-topA"><img src="'+event.message.user_image+'" alt=""><div class="chat-txt"><div class="cht-head"><h4>'+event.message.user_name+'</h4><p class="cht-time">'+formatAMPM(new Date(previous))+'</p></div><p><a href="'+event.message.file+'" target="_blank" download>'+event.message.file_name+'</a></p></div></div>'));
         }else if(event.message.file_type == 'audio'){
           displayMessageA('',$('#dom').append('<div class="chat-item cht-left" id="messages-topA"><img src="'+event.message.user_image+'" alt=""><div class="chat-txt"><div class="cht-head"><h4>'+event.message.user_name+'</h4><p class="cht-time">'+formatAMPM(new Date(previous))+'</p></div><p><audio controls><source src="'+event.message.file+'" type="audio/aac"></audio></p></div></div>'));
         }
       }else{ 
          displayMessageA('',$('#dom').append('<div class="chat-item cht-left" id="messages-topA"><img src="'+event.message.user_image+'" alt=""><div class="chat-txt"><div class="cht-head"><h4>'+event.message.user_name+'</h4><p class="cht-time">'+formatAMPM(new Date(previous))+'</p></div><p>'+event.message.update+'</p></div></div>'));
        }
      }
         }else{
		
             if(event.message.update != ''){
                displayMessage('',$('#dom').append('<div class="chat-item cht-right" id="messages-top"><img src="https://onlineprojectprogress.com/laravel/qui/admin/img/logo/qui.jpg" alt=""><div class="chat-txt"><div class="cht-head"><h4>Support</h4><p class="cht-time">'+formatAMPM(new Date(previous))+'</p></div><p>'+event.message.update+'</p></div></div>'));
             }else{ 
           
           
             }     

           	
           

     
       }
   
     
    }, 
   
    presence: function(event) {
    /* displayMessage('[PRESENCE: ' + event.action + ']',
        'uuid: ' + event.uuid + ', channel: ' + event.channel);*/
       // );
   
    },
   
    status: function(event) {
     /* displayMessage('[STATUS: ' + event.category + ']',
        'connected to channels: ' + event.affectedChannels);*/

      if (event.category == 'PNConnectedCategory') {
        //submitUpdate(theEntry, 'Harmless.');
      }
    }
  });

  pubnub.subscribe({
    channels: [cha],
    withPresence: true
  });
  var d=new Date();
 const f=document.getElementById('file');
 sendButton.addEventListener('click', () => { 

  	 if(f.value == '' && updateText.value != ''){
 	const theEntry = 'Admin';
	 submitUpdate(theEntry, updateText.value)
	
	 
	}else{
   
		const theEntry = document.getElementById('file');
    submitUpdate(theEntry, updateText.value)
   // console.log(theEntry.files[0]);
   
  
  
  

	}
  });
  
 submitUpdate = function(anEntry, anUpdate) {
 
  if(anEntry == 'Admin'){
     $.ajax({
               type:'POST',
               url:'{{route("admin.supportPost")}}',
               data:{_token: "{{ csrf_token() }}",id:"{{Request::segment(3)}}",msg:anUpdate},
               success:function(data) {
                  //console.log(data);
                  //alert(data);
               }
            });
  	pubnub.publish({
      channel : theChannel,
      message : {'entry' : anEntry, 'update' : anUpdate,'type':'support','createdAt':d}
     // file: file,
    },
    function(status, response) {
      if (status.error) {
        console.log(status.error);
      }
      else {
		  
       /* displayMessage('[PUBLISH: sent]',
          'timetoken: ' + response.timetoken);*/
      }
    });
	 document.getElementById("update-text").value = ''
	 
  	}else{
  		const file = anEntry.files[0];
         
   console.log(file);

       
    pubnub.sendFile({
      channel : theChannel,
      message : {'entry' : anEntry, 'update' : anUpdate,'type':'support','createdAt':d},
      file: file,
    },
    function(status, response) {
      if (status.error) {
         console.log(status.error)
      }
      else {
		 // console.log(status);
       /* displayMessage('[PUBLISH: sent]',
          'timetoken: ' + response.timetoken);*/
      }
    });
	document.getElementById("file").value = ''
	 document.getElementById("update-text").value = ''
	//console.log(ok);
  	}
    
  };


  displayMessage = function(messageType, aMessage) {
    let pmessage = document.createElement('s');
    //let br = document.createElement('br');
     
    messagesTop.after(pmessage);
    //pmessage.appendChild(document.createTextNode(messageType));
  // pmessage.appendChild(br);
    pmessage.appendChild(document.createTextNode(aMessage));
  }

  displayMessageA = function(messageType, aMessage) {
    let pmessage = document.createElement('s');
   // let br = document.createElement('br');
 
    messagesTopA.after(pmessage);
  // pmessage.appendChild(document.createTextNode(messageType));
  // pmessage.appendChild(br);
    pmessage.appendChild(document.createTextNode(aMessage));
  }

  
 $('#content').scrollPagination({
    'url': 'data.json', 
    'data': {
      'page': 1, // which entry to load on init
      'size': 10, // number of pages
    },
    'heightOffset': 0
  });




</script>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
<script src="{{asset('js/emoji-picker-input-textarea/inputEmoji.js')}}"></script>
<script>



$(function () {
  $('.new-emoji').emoji({
   
    listCSS: {
      position: 'relative', 
      border: '1px solid gray', 
       width: '333.5px',
      display: 'none'
    }
  });
})
</script>



@endsection