<!-- START LOGO WRAPPER -->
<nav class="top-toolbar navbar navbar-mobile navbar-tablet">
	<ul class="navbar-nav nav-left">
		<li class="nav-item">
			<a href="javascript:void(0)" data-toggle-state="aside-left-open">
				<i class="icon dripicons-align-left"></i>
			</a>
		</li>
	</ul>
	<ul class="navbar-nav nav-center site-logo">
		<li>
			<a href="{{ url('/admin') }}">
				<div class="logo_mobile">
					<span>{{ config('app.name', 'Laravel') }}</span>
				</div>
			</a>
		</li>
	</ul>
	<ul class="navbar-nav nav-right">
		<li class="nav-item">
			<a href="javascript:void(0)" data-toggle-state="mobile-topbar-toggle">
				<i class="icon dripicons-dots-3 rotate-90"></i>
			</a>
		</li>
	</ul>
</nav>
<!-- END LOGO WRAPPER -->

<!-- START TOP TOOLBAR WRAPPER -->
<nav class="top-toolbar navbar navbar-desktop flex-nowrap">

	
	
	<!-- START LEFT DROPDOWN MENUS -->
	<header class="page-header">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h1 class="separator">@if(!empty($pageTitle)) {{$pageTitle}} @endif</h1>
					@yield('breadcrumbs')			
			</div>
		</div>
	</header>
	
	<!-- END LEFT DROPDOWN MENUS -->
	
	
	<!-- START RIGHT TOOLBAR ICON MENUS -->
	<ul class="navbar-nav nav-right">
		
		<li class="nav-item dropdown">
			<a data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
				<i class="icon dripicons-user"></i>
			</a>
			<div class="dropdown-menu dropdown-menu-right dropdown-menu-accout">
				<div class="dropdown-header pb-3">
					<a class="dropdown-item" href="{{ route('admin.auth.changepassword') }}"><i class="zmdi zmdi-account-calendar zmdi-hc-fw"></i> Change Password</a>
					<a class="dropdown-item" href="{{ route('admin.auth.logout') }}"  onclick="event.preventDefault();
					document.getElementById('logout-form').submit();">
					<i class="icon dripicons-lock-open"></i> {{ __('Logout') }}</a>
					<form id="logout-form" action="{{ route('admin.auth.logout') }}" method="POST" style="display: none;">
						@csrf
					</form>
				</div>
			</div>	
		</li>
	</ul>
	<!-- END RIGHT TOOLBAR ICON MENUS -->
	
</nav>
<!-- END TOP TOOLBAR WRAPPER -->