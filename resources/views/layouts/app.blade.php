<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

   <title>QUI</title>
	
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/custom.css') }}">
	<link rel="stylesheet" href="{{ asset('css/media.css') }}">
   
	
	<script src="{{ asset('js/jquery-3.3.1.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
    
</head>
<body>
	
    <div id="app">
		
		<div class="content-wrapper">
			
	        <main class="py-4">
				
					
	            @yield('content')
	        </main>
    	</div>
	</div>
	<script src="{{asset('admin')}}/vendor/modernizr/modernizr.custom.js"></script>
	<script src="{{asset('admin')}}/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
	<!--<script src="{{asset('admin')}}/vendor/jquery/dist/jquery.min.js"></script>-->
	
	<script src="{{asset('admin')}}/vendor/js-storage/js.storage.js"></script>
	<script src="{{asset('admin')}}/vendor/js-cookie/src/js.cookie.js"></script>
	<script src="{{asset('admin')}}/vendor/pace/pace.js"></script>
	<script src="{{asset('admin')}}/vendor/metismenu/dist/metisMenu.js"></script>
	<script src="{{asset('admin')}}/vendor/switchery-npm/index.js"></script>
	<script src="{{asset('admin')}}/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
	<!-- ================== PAGE LEVEL VENDOR SCRIPTS ==================-->
	<script src="{{asset('admin')}}/vendor/countup.js/dist/countUp.min.js"></script>
	<script src="{{asset('admin')}}/vendor/chart.js/dist/Chart.bundle.min.js"></script>
	<script src="{{asset('admin')}}/js/cards/users-chart.js"></script>
	<script src="{{asset('admin')}}/js/cards/bounce-rate-chart.js"></script>
	<script src="{{asset('admin')}}/js/cards/session-duration-chart.js"></script>
	
	<!-- datepicker  -->
	<script src="{{asset('admin')}}/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
	<script src="{{asset('admin')}}/js/components/bootstrap-datepicker-init.js"></script>
	<script src="{{asset('admin')}}/vendor/datatables.net/js/jquery.dataTables.js"></script>
	<script src="{{asset('admin')}}/vendor/datatables.net-bs4/js/dataTables.bootstrap4.js"></script>
	<!-- ================== GLOBAL APP SCRIPTS ==================-->
	<script src="{{asset('admin')}}/js/global/app.js"></script>
	<script src="{{asset('admin')}}/js/common.js"></script>
	<script src="{{asset('')}}/js/sweetalert2.min.js"></script>
	
	
	<script>
		var userTarget = "";
var exit = false;
$('.input-daterange').datepicker({
  format: "dd/mm/yyyy",
  weekStart: 1,
  language: "en",
  daysOfWeekHighlighted: "0,6",
  startDate: "01/01/2017",
  orientation: "bottom auto",
  autoclose: true,
  showOnFocus: true,
  maxViewMode: 'days',
  keepEmptyValues: true,
  templates: {
    leftArrow: '&lt;',
    rightArrow: '&gt;'
  }
});
$('.input-daterange').focusin(function(e) {
  userTarget = e.target.name;
});
$('.input-daterange').on('changeDate', function(e) {
  if (exit) return;
  if (e.target.name != userTarget) {
    exit = true;
    $(e.target).datepicker('clearDates');
  }
  exit = false;
});
		</script>
	
	
	<script>
	$.ajaxSetup({headers:{"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr("content")}})
	</script>
	
	

</body>
</html>
