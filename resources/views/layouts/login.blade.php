<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
	
	<link rel="stylesheet" href="{{asset('admin')}}/css/vendor/bootstrap.css">
	<link rel="stylesheet" href="{{asset('')}}css/toastr.min.css">
	<link rel="stylesheet" href="{{asset('admin')}}/vendor/metismenu/dist/metisMenu.css">
	<link rel="stylesheet" href=".{{asset('admin')}}/vendor/switchery-npm/index.css">
	<link rel="stylesheet" href="{{asset('admin')}}/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
	<!-- ======================= LINE AWESOME ICONS ===========================-->
	<link rel="stylesheet" href="{{asset('admin')}}/css/icons/line-awesome.min.css">
	<!-- ======================= DRIP ICONS ===================================-->
	<link rel="stylesheet" href="{{asset('admin')}}/css/icons/dripicons.min.css">
	<!-- ======================= MATERIAL DESIGN ICONIC FONTS =================-->
	<link rel="stylesheet" href=".{{asset('admin')}}/css/icons/material-design-iconic-font.min.css">
	<!-- ======================= GLOBAL COMMON STYLES ============================-->
	<link rel="stylesheet" href="{{asset('admin')}}/css/common/main.bundle.css">
	<!-- ======================= LAYOUT TYPE ===========================-->
	<link rel="stylesheet" href="{{asset('admin')}}/css/layouts/vertical/core/main.css">
	<!-- ======================= MENU TYPE ===========================================-->
	<link rel="stylesheet" href="{{asset('admin')}}/css/layouts/vertical/menu-type/default.css">
	<!-- ======================= THEME COLOR STYLES ===========================-->
	<link rel="stylesheet" href="{{asset('admin')}}/css/layouts/vertical/themes/theme-a.css">
	<script src="{{asset('admin')}}/vendor/jquery/dist/jquery.min.js"></script>
	<script src="{{asset('')}}/js/toastr.min.js"></script>
</head>
<body>
    <div class="container">
		@if(Session::has('message'))
			<script>
			var type = "{{ Session::get('alert-type', 'info') }}";
			switch(type){
				case 'info':
					toastr.info("{{ Session::get('message') }}");
					break;

				case 'warning':
					toastr.warning("{{ Session::get('message') }}");
					break;

				case 'success':
					toastr.success("{{ Session::get('message') }}");
					break;

				case 'error':
					toastr.error("{{ Session::get('message') }}");
					break;
			}

			</script>
		 @endif
        @yield('content')
    </div>
	
	<script src="{{asset('admin')}}/vendor/modernizr/modernizr.custom.js"></script>
	<script src="{{asset('admin')}}/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
	<script src="{{asset('admin')}}/vendor/js-storage/js.storage.js"></script>
	<script src="{{asset('admin')}}/vendor/js-cookie/src/js.cookie.js"></script>
	<script src="{{asset('admin')}}/vendor/pace/pace.js"></script>
	<script src="{{asset('admin')}}/vendor/metismenu/dist/metisMenu.js"></script>
	<script src="{{asset('admin')}}/vendor/switchery-npm/index.js"></script>
	<script src="{{asset('admin')}}/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
	<!-- ================== GLOBAL APP SCRIPTS ==================-->
	<script src="{{asset('admin')}}/js/global/app.js"></script>
</body>
</html>
