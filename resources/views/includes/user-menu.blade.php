<div class="col-md-3">
					<div class="profile-sidebar border shadow-sm">
						<!-- SIDEBAR USER TITLE -->
						<div class="profile-usertitle">
							<div class="profile-usertitle-name">
								{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}
							</div>
							<div class="profile-usertitle-job">
								{{ Auth::user()->user_type }}
							</div>
						</div>
						<!-- END SIDEBAR USER TITLE -->
						<!-- SIDEBAR MENU -->
						<div class="profile-usermenu">
							<ul class="nav">
								<li class="{{($pageTitle=='Profile')?'active':''}}">
									<a href="{{route('profile')}}">
									<i class="icofont-user-alt-3"></i>
									My Account </a>
								</li>
								<!--<li>
									<a href="{{route('auth.changepasword')}}">
									<i class="icofont-settings-alt"></i>
									Account Settings </a>
								</li>-->
								@if(Auth::user()->user_type == "Mentor" || Auth::user()->user_type == "Mentor-Caregiver")
								<!--<li class="{{($pageTitle=='Create Video')?'active':''}}">
									<a href="{{route('createVideo')}}" >
									<i class="icofont-video"></i>
								
									Create Video 
									</a>
								</li>-->
								<li class="{{($pageTitle=='Video Scheduling')?'active':''}}">
									<a href="{{route('videoScheduling')}}" >
									<i class="icofont-video"></i>
								
									 Add Video Scheduling
									</a>
								</li>
								@elseif(Auth::user()->user_type == "Fighter" || Auth::user()->user_type == "Fighter-Caregiver") 
										<li class="{{($pageTitle=='Create Video')?'active':''}}">
											<a href="{{route('createVideo')}}" >
									<i class="icofont-video"></i>
								
									join video
									</a>
								</li>
								@elseif(Auth::user()->user_type == "volunteer") 
										<li class="{{($pageTitle=='Add Volunteer Testimonial' ||  $pageTitle== 'Listing' || $pageTitle=='Edit Testimonial')?'active':''}}">
											<a href="{{route('list_volunteerTestimonial')}}" >
									<i class="icofont-quote-right"></i>
								     
									Testimonials
									</a>
								</li>
									
									
									@else
									@endif
								<li class="{{($pageTitle=='Change Password')?'active':''}}">
									<a href="{{route('auth.changepasword')}}" >
									<i class="icofont-ui-password"></i>
									Change Password </a>
								</li>
								<li>
									<a href="{{route('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
									<i class="icofont-login"></i>
									Logout </a>
								</li>
							</ul>
						</div>
						<!-- END MENU -->
					</div>
				</div>