@if (! empty($breadcrumbs))
	<nav class="breadcrumb-wrapper" aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="{{ route('admin.dashboard') }}"><i class="icon dripicons-home"></i></a>
			</li>
			@foreach ($breadcrumbs as $label => $link)
				@if (is_int($label) && ! is_int($link))
					<li class="breadcrumb-item">{{ $link }}</li>
				@else
					<li class="breadcrumb-item"><a href="{{ $link }}">{{ $label }}</a></li>
				@endif	
			@endforeach
		</ol>
	</nav>
@endif	