@extends('layouts.app')

@section('content')

<header>
	<div class="qui-head">
		@if(!empty(Session::get('messagecon')))
					<div class="alert alert-success" style="margin-bottom: 50px;">
					{{ Session::get('messagecon') }}</div>
		@endif
		<div class="container sml-container">
			<div class="row">
				<div class="col-lg-12">
					<div class="head-box text-center">
						<a class="navbar-brand" href="#"><img src="{{ asset('image/logo.png') }}" alt="logo"></a>
						<h2>Contact Us</h2>
						<!--<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>-->
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
<!-- header end -->

<!-- page content -->

<section class="p-0">
	<div class="page-content bg-white">
		<div class="col-md-12">
		<div class="contact_form">
						<form action="{{route('admin.contactPost')}}" method="POST">
						@csrf
						<div class="form-row cont-form">
					
							<div class="input-group col-md-4 mb-3">
								<input type="text" class="form-control" placeholder="Name" name="name" value="{{old('name')}}">
								@error('name')
										<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
							</div>

							<div class="input-group col-md-4 mb-3">
								<input type="text" class="form-control" placeholder="Email" name="email" value="{{old('email')}}">
								@error('email')
										<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
							</div>

							<div class="input-group col-md-4 mb-3">
								<input type="text" class="form-control" placeholder="Subject" name="subject" value="{{old('subject')}}">
								@error('subject')
										<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
							</div>
                          
							<div class="input-group text_area col-md-12 mb-3">
								<textarea class="form-control" placeholder="Message" name="message">{{old('message')}}</textarea>
								<div class="input-group text_area col-md-12">
								@error('message')
								 <span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
									</div>
									</div>
                              
							  
							  
							<div class="form-group col-md-12 mb-0">
							<button type="submit" class="blue bb-radius">Submit</button>
							</div>
							
							</div>
					</form>
				</div>
				</div>
	</div>
</section>
<!-- page content end -->

<!-- Optional JavaScript -->


@endsection