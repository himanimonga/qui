@extends('layouts.app')

@section('content')
<!-- header -->
<header>
	
	<div class="qui-head">
		@if(!empty(Session::get('message')))
					<div class="alert alert-success" style="margin-bottom: 50px;">
					{{ Session::get('message') }}</div>
		@endif
		@if(!empty(Session::get('message1')))
					<div class="alert alert-danger" style="margin-bottom: 50px;">{{ Session::get('message1') }}</div>
		@endif
		<div class="container sml-container">

			<div class="row">
				<div class="col-lg-12">
					<div class="head-box text-center">
						<a class="navbar-brand" href="#"><img src="{{ asset('image/logo.png') }}" alt="logo"></a>
						<h2>@if(!empty($receiverdata)) {{$receiverdata[0]['email']}} @endif</h2>
						<!--<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>-->
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
<!-- header end -->

<!-- page content -->
<section class="p-0">
	<div class="qui-card usr-dtl">
		<div class="container">
			<form method="post" action="{{ route('userFeedback')}}" autocomplete="off">
		@csrf
			<div class="row">
		 
				<div class="col-lg-3">
					<!-- profile picture -->
					<input type="hidden" name="receiver_id" value="@if(!empty($receiverdata)) {{$receiverdata[0]['id']}} @endif">
					<input type="hidden" name="sender_id" value="@if(!empty($senderdata)) {{$senderdata[0]['id']}} @endif" >

					<div class="user-img">
						@if(!empty($data[0]['image']))
						
						<img class="img-fluid" src="{{ asset('upload/profile/'.$data[0]['image']) }}" alt="profile picture">
							@else
							<img class="img-fluid" src="{{ asset('image/profile-pic.png')}}" alt="profile picture">
							@endif
						
					</div>
					<!-- profile picture end -->
				</div>
				<div class="col-lg-9">
					<!-- user detail -->
					<div class="user-detail">
					<input type="hidden" name="sender_name" value="@if(!empty($senderdata)) {{$senderdata[0]['name']}} @endif">
					<input type="hidden" name="sender_email" value="@if(!empty($senderdata)) {{$senderdata[0]['email']}} @endif">
					<input type="hidden" name="receiver_name" value="@if(!empty($receiverdata)) {{$receiverdata[0]['name']}} @endif">
					<input type="hidden" name="receiver_email" value="@if(!empty($receiverdata)) {{$receiverdata[0]['email']}} @endif">
					<input type="hidden" name="receiver_address" value="@if(!empty($receiverdata)) {{$receiverdata[0]['location']}} @endif">
						<h4>@if(!empty($receiverdata)) {{$receiverdata[0]['name']}} @endif</h4>
						<p>@if(!empty($receiverdata)) {{$receiverdata[0]['email']}} @endif</p>

						<p>
							<img src="{{ asset('image/address.png') }}" alt="address icon">@if(!empty($receiverdata))
							{{$receiverdata[0]['location']}} @endif</p>
							<input type="hidden" name="sender_address" value="@if(!empty($senderdata)) {{$senderdata[0]['location']}} @endif">
						<p class="rating">
							<input type="checkbox" id="star1" value = "1" name="starOne" style="opacity:0; position:absolute;"  @if(!empty($feedback)) {{$feedback[0]['rating'] == 1 || $feedback[0]['rating'] == 2 || $feedback[0]['rating'] == 3 || $feedback[0]['rating'] == 4 || $feedback[0]['rating'] == 5 ? "Checked":"" }} @else @endif>
							<label for="star1" class="@if(!empty($feedback)) {{ $feedback[0]['rating'] == 1 || $feedback[0]['rating'] == 2 || $feedback[0]['rating'] == 3 || $feedback[0]['rating'] == 4 || $feedback[0]['rating'] == 5 ? 'active':'' }}@else @endif"  id="show1">&#9733</label>
							<input type="checkbox" id="star2" value = "2" name="starTwo" style="opacity:0; position:absolute;" @if(!empty($feedback)) {{  $feedback[0]['rating'] == 2 || $feedback[0]['rating'] == 3 || $feedback[0]['rating'] == 4 || $feedback[0]['rating'] == 5 ? 'Checked':'' }} @endif> 
							<label for="star2" class=" @if(!empty($feedback)) {{ $feedback[0]['rating'] == 2 || $feedback[0]['rating'] == 3 || $feedback[0]['rating'] == 4 || $feedback[0]['rating'] == 5 ? "active":"" }}@else @endif" id="show2">&#9733</label>
							<input type="checkbox" id="star3" value = "3" name="starThree" style="opacity:0; position:absolute;" @if(!empty($feedback)) {{ $feedback[0]['rating'] == 3 || $feedback[0]['rating'] == 4 || $feedback[0]['rating'] == 5 ? "Checked":"" }} @endif> 
							<label for="star3" class="@if(!empty($feedback)) {{$feedback[0]['rating'] == 3 || $feedback[0]['rating'] == 4 || $feedback[0]['rating'] == 5 ? 'active':'' }} @else @endif"  id="show3">&#9733</label>
							<input type="checkbox" id="star4" value = "4" name="starFour" style="opacity:0; position:absolute;" @if(!empty($feedback)) {{ !empty($feedback) &&  $feedback[0]['rating'] == 4 || $feedback[0]['rating'] == 5 ? "Checked":"" }} @endif> 
							<label for="star4" class="@if(!empty($feedback)) {{$feedback[0]['rating'] == 4 || $feedback[0]['rating'] == 5 ? 'active':'' }}@else @endif"  id="show4">&#9733</label>
							
							<input type="checkbox" id="star5" value = "5" name="starFive" style="opacity:0; position:absolute;" @if(!empty($feedback)) {{ $feedback[0]['rating'] == 5 ? "Checked": ""}} @endif>
							<label for="star5" class=" @if(!empty($feedback)) {{$feedback[0]['rating'] == 5 ? 'active':'' }} @else @endif" id="show5">&#9733</label>
							
						</p>
						@if(!empty($feedback))
						<p class="rating-txt">@if(!empty($feedback)) {{$feedback[0]['rating']}}@else 0; @endif/5</p>
					@endif
					</div>
					<!-- user detail end -->
				</div>
				<div class="col-lg-12">
					<div class="feedback mt-3">
						<textarea class="w-100" rows="6" placeholder="Enter your Feedback" name="comment">@if(!empty($feedback)){{ old('comment',$feedback[0]['comment'])}}@else {{ old('comment')}} @endif</textarea>
						<input type="submit" class="btn btn-primary" name="submit" value="submit">
						
					</div>
				</div>
			
			</div>
			</form>
		</div>
	</div>
</section>
<script>
$( document ).ready(function() {
	 
   $(document).on("click","#star1",function() {
   
	var x = document.getElementById("star1").checked;
	 if(x == true){

        var element = document.getElementById("show1");
		   element.classList.add("active");
		   }else{
		    	var element = document.getElementById("show");
				var element1 = document.getElementById("show2");
				var element2 = document.getElementById("show3");
				var element3 = document.getElementById("show4");
				var element4 = document.getElementById("show5");
				   element.classList.remove("active");
				   element1.classList.remove("active");
				   element2.classList.remove("active");
				   element3.classList.remove("active");
				   element4.classList.remove("active");
				    var y = document.getElementById("star2").checked = false; 
				    var z = document.getElementById("star3").checked = false;
					var a = document.getElementById("star4").checked = false;
					var b = document.getElementById("star5").checked = false;
				   //element.classList.remove("active");
		   }
    });

	 $(document).on("click","#star2",function() {
		 var x = document.getElementById("star1").checked = true;
	     var y = document.getElementById("star2").checked;
	
	 if(y == true){
		  var element1 = document.getElementById("show1");
		   element1.classList.add("active");
        var element = document.getElementById("show2");
		   element.classList.add("active");
		   }else{
               var element1 = document.getElementById("show2");
				var element2 = document.getElementById("show3");
				var element3 = document.getElementById("show4");
				var element4 = document.getElementById("show5");
				 element1.classList.remove("active");
				   element2.classList.remove("active");
				   element3.classList.remove("active");
				   element4.classList.remove("active");
				    var z = document.getElementById("star3").checked = false;
					var a = document.getElementById("star4").checked = false;
					var b = document.getElementById("star5").checked = false;
		   }
    });
	
	 $(document).on("click","#star3",function() {
		  var x = document.getElementById("star1").checked = true;
	     var y = document.getElementById("star2").checked  = true;
	var z = document.getElementById("star3").checked;
	 if(z == true){
		  var element1 = document.getElementById("show1");
		   element1.classList.add("active");
		    var element2 = document.getElementById("show2");
		   element2.classList.add("active");
        var element = document.getElementById("show3");
		   element.classList.add("active");
		   }else{
				var element1 = document.getElementById("show3");
				var element3 = document.getElementById("show4");
				var element4 = document.getElementById("show5");
				 element1.classList.remove("active");
				   element3.classList.remove("active");
				   element4.classList.remove("active");
				  
				   var a = document.getElementById("star4").checked = false;
					var b = document.getElementById("star5").checked = false;
		   }
    });
	
	 $(document).on("click","#star4",function() {
		  var x = document.getElementById("star1").checked = true;
	     var y = document.getElementById("star2").checked  = true;
	var z = document.getElementById("star3").checked  = true;
	var a = document.getElementById("star4").checked;
	 if(a == true){
		  var element1 = document.getElementById("show1");
		   element1.classList.add("active");
		    var element2 = document.getElementById("show2");
		   element2.classList.add("active");
        var element3 = document.getElementById("show3");
		   element3.classList.add("active");
        var element = document.getElementById("show4");
		   element.classList.add("active");
		   }else{
				
				 var element3 = document.getElementById("show4");
				var element4 = document.getElementById("show5");
				var b = document.getElementById("star5").checked = false;
				 element3.classList.remove("active");
				  element4.classList.remove("active");
		   }
    });
	
	 $(document).on("click","#star5",function() {
		  var x = document.getElementById("star1").checked = true;
	     var y = document.getElementById("star2").checked  = true;
	var z = document.getElementById("star3").checked  = true;
	var a = document.getElementById("star4").checked = true;
	var b = document.getElementById("star5").checked;
	 if(b == true){
		  var element1 = document.getElementById("show1");
		   element1.classList.add("active");
		    var element2 = document.getElementById("show2");
		   element2.classList.add("active");
        var element3 = document.getElementById("show3");
		   element3.classList.add("active");
        var element4 = document.getElementById("show4");
		   element4.classList.add("active");
        var element = document.getElementById("show5");
		   element.classList.add("active");
		   }else{
				var element = document.getElementById("show5");
		   element.classList.remove("active");
		   var b = document.getElementById("star5").checked = false;
		   }
    });
});
</script>
@endsection